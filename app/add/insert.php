<?php
session_start();
if (!isset($_POST['ADD'])) {
    header('Localhost: add.php');
}
$topic = trim(htmlspecialchars($_SESSION['topic'], ENT_QUOTES));
$table = $_SESSION['table'];
?>
<html>
    <head>
        <title>ADD QUESTION</title>
        <link href="css/new_css.css" type="text/css" rel="stylesheet"/>
        <script language="Javascript">
            function redirection() {
                setTimeout("location.href='add.php'", 2000);
            }
        </script>
    </head>
    <body onLoad="redirection()">
        <div id="container">
            <?php //include("header/head.php"); ?>
            <?php //include("bars/menubar_teacher.html"); ?>
            <div id="content-container">
                <div id="content">
                    <?php
                    require("config.php");
                    mysql_select_db('roughsheet_database_1423552512');
                    date_default_timezone_set('Asia/Kolkata');

                    $type = trim(htmlspecialchars($_POST['type'], ENT_QUOTES));
                    $diff_level = trim(htmlspecialchars($_POST['diff'], ENT_QUOTES));
                    $intro = trim(htmlspecialchars($_POST['intro'], ENT_QUOTES));
                    $que = trim(htmlspecialchars($_POST['que'], ENT_QUOTES));
                    //$encodedque=json_encode($que);
                    $op1 = trim(htmlspecialchars($_POST['op1'], ENT_QUOTES));
                    $op2 = trim(htmlspecialchars($_POST['op2'], ENT_QUOTES));
                    $op3 = trim(htmlspecialchars($_POST['op3'], ENT_QUOTES));
                    $op4 = trim(htmlspecialchars($_POST['op4'], ENT_QUOTES));
                    $ans = trim(htmlspecialchars($_POST['ans'], ENT_QUOTES));
                    $sol = trim(htmlspecialchars($_POST['solution'], ENT_QUOTES));

                    $alt = mysql_query("INSERT INTO `$table` (`topic_id`, `q_type`, `Introduction`, `Question`, `Op1`, `Op2`, `Op3`, `Op4`, `Answer`, `Solution`, `Difficulty_level`) VALUES('$topic','$type','$intro','$que','$op1','$op2','$op3','$op4','$ans','$sol','$diff_level')") or die(mysql_error());
                    echo "<center>Question Added Successfully!! <br><br> <a href='add.php'>ADD ANOTHER QUESTION</a></center>";
                    ?>
                </div>
            </div>
            <hr>
        </div>
        <div id=foot >Designed & Developed By &copy; CHRONIKTECH 2015</div>
    </body>
</html>
