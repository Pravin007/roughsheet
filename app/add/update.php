<?php
//include "session_check.php";
session_start();
if (!isset($_SESSION['table'])) {
    header('Location: index.php');
} elseif (isset($_GET['logout']) && $_GET['logout'] == "yes") {
    session_destroy();
    header('Location: index.php');
} else {
    //stay
}
?>

<html>
    <head>
        <title>UPDATE Que</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="css/new_css.css" type="text/css" media="screen">
        <link href="css/btncss.css" type="text/css" rel="stylesheet"/>
        <link href="css/table_css.css" type="text/css" rel="stylesheet"/>
        <!-- OF COURSE YOU NEED TO ADAPT NEXT LINE TO YOUR tiny_mce.js PATH -->
        <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/general.js"></script>
        <script type="text/javascript" src="MathJax.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                mode: "textareas",
                elements: "elm1",
                theme: "advanced",
                plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,fmath_formula,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
                theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,fmath_formula,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
                file_browser_callback: "ajaxfilemanager",
                paste_use_dialog: false,
                theme_advanced_resizing: true,
                theme_advanced_resize_horizontal: true,
                apply_source_formatting: true,
                force_br_newlines: true,
                force_p_newlines: false,
                relative_urls: true

            });
            function ajaxfilemanager(field_name, url, type, win) {
                var ajaxfilemanagerurl = "tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
                switch (type) {
                    case "image":
                        break;
                    case "media":
                        break;
                    case "flash":
                        break;
                    case "file":
                        break;
                    default:
                        return false;
                }
                tinyMCE.activeEditor.windowManager.open({
                    url: "tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                    width: 782,
                    height: 440,
                    inline: "yes",
                    close_previous: "no"
                }, {
                    window: win,
                    input: field_name
                });
            }
        </script>
        <script language="JavaScript" src="gen_validatorv4.js"
        type="text/javascript" xml:space="preserve"></script>
    </head>
    <body onUnload="exitPopUp()">
        <div id=wrap>
            <div id="container">


<?php //include("header/head.php");  ?>


                <div id="content-container1">
                    <span style="margin-left: 40px;" ><?php echo"TABLE:  " . strtoupper($_SESSION['table']) . " "; ?><a href="upload.php" style="margin-left: 50px;">UPLOAD EXCEL</a><a href='edit_q.php' style="margin-left: 50px;">EDIT QUESTIONS</a><a href="add.php?logout=yes" style="margin-left: 50px;">LOGOUT</a></span>
                    <div id="content-container2">
                        <div id="content">
                            </br></br></br><center>

                                <table class=table style="margin:0 0 0 40px;">
<?php
$table = $_SESSION['table'];
require("config.php");
mysql_select_db('roughsheet_database_1423552512');
date_default_timezone_set('Asia/Kolkata');
if (isset($_GET['func']) && $_GET['func'] == 'del') {
    mysql_query("delete from $table where id='$_GET[id]' ") or die(mysql_error());
    header('Location: edit_q.php');
    echo"<script>window.location.assign('edit_q.php');</script>";
} elseif (isset($_REQUEST['UPDATE'])) {

    $type_of_q = trim(htmlspecialchars($_POST['type'], ENT_QUOTES));
    $diff_l = trim(htmlspecialchars($_POST['diff'], ENT_QUOTES));
    $intro = trim(htmlspecialchars($_POST['intro'], ENT_QUOTES));
    $question = trim(htmlspecialchars($_POST['que'], ENT_QUOTES));
    //$encodedque=json_encode($que);
    $op1 = trim(htmlspecialchars($_POST['op1'], ENT_QUOTES));
    $op2 = trim(htmlspecialchars($_POST['op2'], ENT_QUOTES));
    $op3 = trim(htmlspecialchars($_POST['op3'], ENT_QUOTES));
    $op4 = trim(htmlspecialchars($_POST['op4'], ENT_QUOTES));
    $ans = trim(htmlspecialchars($_POST['ans'], ENT_QUOTES));
    $solution = trim(htmlspecialchars($_POST['solution'], ENT_QUOTES));

    mysql_query("UPDATE $table SET `q_type`='$type_of_q',`Introduction`='$intro',`Question`='$question',`Op1`='$op1',`Op2`='$op2',`Op3`='$op3',`Op4`='$op4',`Answer`='$ans',`Solution`='$solution',`Difficulty_level`='$diff_l' WHERE `id`='$_REQUEST[id]'") or die(mysql_error());

    echo"<script>window.location='edit_q.php';</script>";
} else {
    //stay to edit the question
    $q_query = mysql_query("select * from $table where id='$_GET[id]'") or die(mysql_error());
    $q_data = mysql_fetch_assoc($q_query);
}
?>
                                    <form method="POST" action="update.php" name="myform" id="myform">
                                        <input type="hidden" name=id value="<?php echo $_GET['id'] ?>">
                                        <tr><td>Question Type</td><td><select name="type" >
                                    <?php
                                    $qtype_q = mysql_query("select * from `rs_qtype_31052015` where `id`='$q_data[q_type]'") or die(mysql_error());
                                    $qtype_d = mysql_fetch_assoc($qtype_q);
                                    for ($t = 1; $t <= 4; $t++) {
                                        if ($q_data['q_type'] == $t) {
                                            echo"<option value='$qtype_d[id]' selected >$qtype_d[type]</option>";
                                        } else {
                                            echo"<option value='$qtype_d[id]'>$qtype_d[type]</option>";
                                        }
                                    }
                                    ?>
                                                </select></td></tr> 	
                                        <tr><td>Difficulty Level</td><td><select name="diff" >
                                                    <?php
                                                    if ($q_data['Difficulty_level'] == "Easy") {
                                                        echo"<option value='Easy' selected >Easy</option>";
                                                    } else {
                                                        echo"<option value='Easy' >Easy</option>";
                                                    }
                                                    if ($q_data['Difficulty_level'] == "Medium") {
                                                        echo"<option value='Medium' selected >Medium</option>";
                                                    } else {
                                                        echo"<option value='Medium' >Medium</option>";
                                                    }
                                                    if ($q_data['Difficulty_level'] == "Hard") {
                                                        echo"<option value='Hard' selected >Hard</option>";
                                                    } else {
                                                        echo"<option value='Hard' >Hard</option>";
                                                    }
                                                    ?>
                                                </select></td></tr> 	
                                        <tr><td>INTRODUCTION</td><td><textarea name="intro" ><?php echo $q_data['Introduction'] ?></textarea></td></tr>	
                                        <tr><td>QUESTION</td><td><textarea name="que"><?php echo $q_data['Question'] ?></textarea></td></tr>

                                        <tr><td>OPTION 1 </td><td><textarea name="op1"><?php echo $q_data['Op1'] ?></textarea></td></tr>        
                                        <tr><td>OPTION 2 </td><td><textarea name="op2"><?php echo $q_data['Op2'] ?></textarea></td></tr>

                                        <tr><td>OPTION 3 </td><td><textarea name="op3"><?php echo $q_data['Op3'] ?></textarea></td></tr>

                                        <tr><td>OPTION 4 </td><td><textarea name="op4"><?php echo $q_data['Op4'] ?></textarea></td></tr>

                                        <tr><td>ANSWER(option&nbspnumber)</td><td>
                                                <select name="ans">
                                                    <?php
                                                    $ans = $q_data['Answer'];
                                                    if ($ans == 1) {
                                                        echo"<option value='1' SELECTED >1</option>";
                                                    } else {
                                                        echo"<option value='1'>1</option>";
                                                    }
                                                    if ($ans == 2) {
                                                        echo"<option value='2' SELECTED >2</option>";
                                                    } else {
                                                        echo"<option value='2'>2</option>";
                                                    }
                                                    if ($ans == 3) {
                                                        echo"<option value='3' SELECTED >3</option>";
                                                    } else {
                                                        echo"<option value='3'>3</option>";
                                                    }
                                                    if ($ans == 4) {
                                                        echo"<option value='4' SELECTED >4</option>";
                                                    } else {
                                                        echo"<option value='4'>4</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td></tr>
                                        <tr><td>SOLUTION </td><td><textarea name="solution"><?php echo $q_data['Solution'] ?></textarea></td></tr>
                                        <tr><td colspan=2><center><input type=submit class=btn name=UPDATE value=Submit></center></td></tr>

                                    </form>

                                </table>
                            </center>
                        </div>

                    </div>
                </div>
                                                    <?php include('footer/footer.html'); ?>
            </div>
            <script language="JavaScript" type="text/javascript"
                    xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
                        var frmvalidator = new Validator("myform");
                        frmvalidator.addValidation("unit", "req", "PLEASE ENTER UNIT NUMBER (1 to 6) ONLY NUMERICs");
                        frmvalidator.addValidation("unit", "numeric", "PLEASE ENTER UNIT NO. IN NUMERICS");
                        frmvalidator.addValidation("ans", "req", "PLEASE ENTER ANSWER NO. IN NUMERIC");
                        frmvalidator.addValidation("ans", "numeric", "PLEASE ENTER ANSWER NO. IN NUMERIC");
//]]></script>
        </div>
<?php include("footer.html"); ?>
    </body>
</html>