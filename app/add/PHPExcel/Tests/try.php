<?php
//if(!isset($_SESSION['admin_code'])){
//   header('Location: ../../../admin/index.php');
//}
// else{
// $sec_code=$_SESSION['admin_code'];
// include "../../../admin/config.php";
// $code_query=mysql_query("select admin_code from admin_info where admin_id=2 ");
// $code=mysql_fetch_assoc($code_query);
// 
// if($code['admin_code']!=$sec_code)
// {
//    unset($_SESSION);
//    header('Location: ../../../admin/index.php');
// }
// }
// ?>
<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
error_reporting(E_ALL);
include("../../../../../../feedback/php/config.php");
/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
//echo date('H:i:s') , " Creating a new PHPExcel object......" , PHP_EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Setting document properties........" , PHP_EOL;
$objPHPExcel->getProperties()->setCreator("TEAM WEBGYOR")
							 ->setLastModifiedBy("WEBGYOR")
							 ->setTitle("Office 2007 XLSX Test Document for Attendance Upload")
							 ->setSubject("Office 2007 XLSX Test Document for Attendance Upload")
							 ->setDescription("Excel Sheet for Students Attendance upload")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Attendance Uploads");


// Create a first sheet, representing sales data
//echo date('H:i:s') , " Add some data" , PHP_EOL;
$objPHPExcel->setActiveSheetIndex(0);
//FORMATTING THE HEADER PART
$objPHPExcel->getActiveSheet()->setCellValue('A1', '                                                                                                              JSPM\'s');
//$objPHPExcel->getActiveSheet()->mergeCells('E1:H1');
$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');

$objPHPExcel->getActiveSheet()->setCellValue('A2', '                                                               IMPERIAL COLLEGE OF ENGINEERING AND RESEARCH');
//$objPHPExcel->getActiveSheet()->mergeCells('C2:H2');
$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');

$objPHPExcel->getActiveSheet()->setCellValue('A3', '                                                                                         WAGHOLI, PUNE-412 207');
//$objPHPExcel->getActiveSheet()->mergeCells('D3:H3');
$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');

$objPHPExcel->getActiveSheet()->setCellValue('A4','                                                        DEPARTMENT OF ELECTRONICS AND TELECOMMUNICATION');
//$objPHPExcel->getActiveSheet()->mergeCells('C4:H4');
$objPHPExcel->getActiveSheet()->mergeCells('A4:H4');


$objPHPExcel->getActiveSheet()->setCellValue('A5', "                                                                   $year YEAR (E&TC) Div $uppercase ATTENDANCE REPORT");
//$objPHPExcel->getActiveSheet()->mergeCells('D5:H5');
$objPHPExcel->getActiveSheet()->mergeCells('A5:H5');
//-->
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setCellValue('A7','Sr. No.');
$objPHPExcel->getActiveSheet()->mergeCells('A7:A8');
$objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$objPHPExcel->getActiveSheet()->setCellValue('B7','Roll No.');
$objPHPExcel->getActiveSheet()->setCellValue('C7','Name of the student');

$sub_query="select * from " . $type . "_subject_info where semester='$sem_data[semester]' and year='$yr' " ;
	$sub_data=mysql_query($sub_query) or die(mysql_error());
        //$sub_count=mysql_num_rows($sub_data);
        while($sub_nickname=mysql_fetch_assoc($sub_data)){
	$sub_nickname_array[]=$sub_nickname['sub_nickname'];
        }
	$sub_nickname_count=count($sub_nickname_array);

if($sub_nickname_count=='6'){
$literal='J';
$preliteral='I';
$litcol='9';
$objPHPExcel->getActiveSheet()->setCellValue('D7',"$sub_nickname_array[0]");
$objPHPExcel->getActiveSheet()->setCellValue('E7',"$sub_nickname_array[1]");
$objPHPExcel->getActiveSheet()->setCellValue('F7',"$sub_nickname_array[2]");
$objPHPExcel->getActiveSheet()->setCellValue('G7',"$sub_nickname_array[3]");
$objPHPExcel->getActiveSheet()->setCellValue('H7',"$sub_nickname_array[4]");
$objPHPExcel->getActiveSheet()->setCellValue('I7',"$sub_nickname_array[5]");
$objPHPExcel->getActiveSheet()->setCellValue('J7','AVERAGE');
$objPHPExcel->getActiveSheet()->setCellValue('J8','=sum(D8:I8)');
//$a='9';
//$avgfunc="=sum(D$a:H$a)/I8";
}
if($sub_nickname_count=='5'){
    $literal='I';
    $preliteral='H';
    $litcol='8';
$objPHPExcel->getActiveSheet()->setCellValue('D7',"$sub_nickname_array[0]");
$objPHPExcel->getActiveSheet()->setCellValue('E7',"$sub_nickname_array[1]");
$objPHPExcel->getActiveSheet()->setCellValue('F7',"$sub_nickname_array[2]");
$objPHPExcel->getActiveSheet()->setCellValue('G7',"$sub_nickname_array[3]");
$objPHPExcel->getActiveSheet()->setCellValue('H7',"$sub_nickname_array[4]");
$objPHPExcel->getActiveSheet()->setCellValue('I7','AVERAGE');
$objPHPExcel->getActiveSheet()->setCellValue('I8','=sum(D8:H8)');
//$a='9';
//$avgfunc="=sum(D$a:H$a)/I8";
}
if($sub_nickname_count=='4')
{
    $literal='H';
$preliteral='G';
$litcol='7';
$objPHPExcel->getActiveSheet()->setCellValue('D7',"$sub_nickname_array[0]");
$objPHPExcel->getActiveSheet()->setCellValue('E7',"$sub_nickname_array[1]");
$objPHPExcel->getActiveSheet()->setCellValue('F7',"$sub_nickname_array[2]");
$objPHPExcel->getActiveSheet()->setCellValue('G7',"$sub_nickname_array[3]");
$objPHPExcel->getActiveSheet()->setCellValue('H7','AVERAGE');
$objPHPExcel->getActiveSheet()->setCellValue('H8','=sum(D8:G8)');
$val=8;
$avgfunc="=sum(D$val:G$val)/H8";

}
if($sub_nickname_count=='3')
{
    $literal='G';
$preliteral='F';
$litcol='6';
$objPHPExcel->getActiveSheet()->setCellValue('D7',"$sub_nickname_array[0]");
$objPHPExcel->getActiveSheet()->setCellValue('E7',"$sub_nickname_array[1]");
$objPHPExcel->getActiveSheet()->setCellValue('F7',"$sub_nickname_array[2]");
$objPHPExcel->getActiveSheet()->setCellValue('G7','AVERAGE');
$objPHPExcel->getActiveSheet()->setCellValue('G8','=sum(D8:F8)');
$val=7;
$avgfunc="=sum(D" . "$val" . ":F" . "$val" . ")/G8";

}
$objPHPExcel->getActiveSheet()->setCellValue('B8','Total No. of Lectures');
$objPHPExcel->getActiveSheet()->mergeCells('B8:C8');

$objPHPExcel->getActiveSheet()->getStyle('B8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C7')->getAlignment()->setShrinkToFit(true);
$objPHPExcel->getActiveSheet()->getStyle('D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('C7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('I7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);

//$objPHPExcel->getActiveSheet()->setCellValue('D8','31');
//$objPHPExcel->getActiveSheet()->setCellValue('E8','30');
//$objPHPExcel->getActiveSheet()->setCellValue('F8','33');
//$objPHPExcel->getActiveSheet()->setCellValue('G8','32');
//$objPHPExcel->getActiveSheet()->setCellValue('H8','39');

//<------DB Connection----->
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASSWORD', '123');
  define('DB_NAME', 'jspmse');
  $link= mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME) or die("error");



$i=1;
//<---Printing the roll nos.

 $sql1="SELECT rollno FROM student_info WHERE year='$yr' and division='$div'";
 
 $studquery1=mysqli_query($link,$sql1) or die("Error");

//Defn for printing rollno
$row=9;
$col=1;

//Defn for printing serial no.
$serrow=9;
$sercol=0;



while($stud_rollno=mysqli_fetch_array($studquery1))
  {
  foreach($stud_rollno as $key=>$value)
  {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$value);
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sercol,$serrow,$i);
                
  }
$row++;
$serrow++;
$i++;
}
//---->
  
//Defn for printing student names
   $row=9;
   $col = 2;
 
  //<--Printing the list of students
 
   $sql="SELECT stud_name FROM student_info WHERE year='$yr' and division='$div'";
 
   $studquery=mysqli_query($link,$sql) or die("Error");
   $numrows=mysqli_num_rows($studquery);
   $length=$row+$numrows;
   
   while($stud_name=mysqli_fetch_array($studquery))
  {
  foreach($stud_name as $key=>$value)
  {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col,$row,$value);
                
                //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($litcol,$row,$avgfunc);
  }
$row++;
//$val=$val+1;
}


for($a=9;$a<$length;$a++){
$objPHPExcel->getActiveSheet()->setCellValue("$literal$a","=sum(D$a:$preliteral$a)*100/$literal" . "8");
}

//<--Outlining
$styleThinBlackBorderOutline = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		),
	),
);
$finalpos=8+$numrows;
//$objPHPExcel->getActiveSheet()->getStyle("A9:H$finalpos")->applyFromArray($styleThinBlackBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("A9:$literal$finalpos")->applyFromArray($styleThinBlackBorderOutline);
//$objPHPExcel->getActiveSheet()->getStyle("A7:H8")->applyFromArray($styleThinBlackBorderOutline);
$objPHPExcel->getActiveSheet()->getStyle("A7:$literal" . "9")->applyFromArray($styleThinBlackBorderOutline);
//$objPHPExcel->getActiveSheet()->getStyle("A9:H$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//$objPHPExcel->getActiveSheet()->getStyle("B9:H$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//$objPHPExcel->getActiveSheet()->getStyle("C9:H$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A9:$literal$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B9:$literal$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C9:$literal$finalpos")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
 

$styleThickBlackBorderOutline = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THICK,
			'color' => array('argb' => 'FF000000'),
		),
	),
);
$objPHPExcel->getActiveSheet()->getStyle("A7:$literal" . "8")->applyFromArray($styleThickBlackBorderOutline);
//---->


//<--Cell Protection

$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);	// Needs to be set to true in order to enable any worksheet protection!
//echo $literal;
//$objPHPExcel->getActiveSheet()->protectCells("A7:$literal" . "7", 'PHPExcel');
//
//$objPHPExcel->getActiveSheet()->getStyle("D8:$literal" . "8")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
$objPHPExcel->getActiveSheet()->getStyle("D8:$preliteral$finalpos")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
//echo $finalpos;
