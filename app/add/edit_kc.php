<html>
    <head>
        <title>KC</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="css/new_css.css" type="text/css" media="screen">
        <link href="css/btncss.css" type="text/css" rel="stylesheet"/>
        <link href="css/table_css.css" type="text/css" rel="stylesheet"/>
        <!-- OF COURSE YOU NEED TO ADAPT NEXT LINE TO YOUR tiny_mce.js PATH -->
        <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/general.js"></script>
        <script type="text/javascript" src="MathJax.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                mode: "textareas",
                elements: "elm1",
                theme: "advanced",
                plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,fmath_formula,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
                theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,fmath_formula,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
                file_browser_callback: "ajaxfilemanager",
                paste_use_dialog: false,
                theme_advanced_resizing: true,
                theme_advanced_resize_horizontal: true,
                apply_source_formatting: true,
                force_br_newlines: true,
                force_p_newlines: false,
                relative_urls: true

            });
            function ajaxfilemanager(field_name, url, type, win) {
                var ajaxfilemanagerurl = "tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
                switch (type) {
                    case "image":
                        break;
                    case "media":
                        break;
                    case "flash":
                        break;
                    case "file":
                        break;
                    default:
                        return false;
                }
                tinyMCE.activeEditor.windowManager.open({
                    url: "tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                    width: 782,
                    height: 440,
                    inline: "yes",
                    close_previous: "no"
                }, {
                    window: win,
                    input: field_name
                });
            }
        </script>
        <script language="JavaScript" src="gen_validatorv4.js"
        type="text/javascript" xml:space="preserve"></script>
    </head>
    <body onUnload="exitPopUp()">
<?php
include"config.php";
mysql_select_db('roughsheet_database_1423552512');
if (isset($_POST['up'])) {

    $text = trim(htmlspecialchars($_POST['intro'], ENT_QUOTES));
    $id = $_POST['id'];
    mysql_query("UPDATE rs_key_concepts_27062015 SET kc_text='$text' where topic_id='$id'") or die(mysql_error());
    echo"DONE";
}
?>
        <div id=wrap>
            <div id="container">

                <div id="content-container1">
                    <span style="margin-left: 40px;" ></span>
                    <div id="content-container2">
                        <div id="content">
                            </br></br></br><center>
                                <a href='update_key_concepts.php'>BACK</a>
                                <table class=table style="margin:0 0 0 40px;">
<?php
$tstr = $_GET['editid'];
$tarr = explode('_', $tstr);
$kcq = mysql_query("select * from rs_key_concepts_27062015 where topic_id='$tarr[1]'") or die(mysql_error());
$kcd = mysql_fetch_assoc($kcq);
?>
                                    <form method="POST" action=# >
                                        <input type="hidden" name="id" value="<?php echo $tarr['1'] ?>"/>
                                        <tr><td>Topic Name</td><td><input type="text" name="tname" disabled value="<?php echo $tarr['0'] ?>"/></td></tr>
                                        <tr><td>INTRODUCTION</td><td><textarea name="intro" ><?php echo $kcd['kc_text'] ?></textarea></td></tr>	
                                        <tr><td colspan=2 ><center><input type=submit class=btn name='up' value=Submit></center></td></tr>
                                    </form>

                                </table>
                            </center>
                        </div>
                    </div>
                </div>
<?php include('footer/footer.html'); ?>
            </div>
            <script language="JavaScript" type="text/javascript"
                    xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
//]]></script>
        </div>
                <?php include("footer.html"); ?>
    </body>
</html>