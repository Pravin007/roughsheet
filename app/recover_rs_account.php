<link rel="icon" href="assets/img/tab_icon.png">
<title>RoughSheet</title>
<body>
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <?php
    session_start();
    if ($_SERVER['SERVER_NAME'] == "roughsheet.com") {
        ?>
        <script>
            window.location.assign("https://www.roughsheet.com<?php echo $_SERVER['REQUEST_URI'] ?>");
        </script>
        <?php
    } else {
        if (isset($_REQUEST['hidden_flag']) && $_REQUEST['hidden_flag'] == '1_rs_recover' && isset($_SESSION['RECOVER_ACCOUNT'])) {
            $err = "";
            if ($_REQUEST['pass'] == "" || empty($_REQUEST['pass']) || !isset($_REQUEST['pass']) || $_REQUEST['pass'] == null || $_REQUEST['cpass'] == "" || empty($_REQUEST['cpass']) || !isset($_REQUEST['cpass']) || $_REQUEST['cpass'] == null) {
                echo"<div class='alert alert-info'><center>All fields are mandatory</center></div>";
                goto Re_Enter;
            } elseif ($_REQUEST['pass'] == $_REQUEST['cpass']) {
                $pas2 = trim(htmlspecialchars($_REQUEST['pass'], ENT_QUOTES));
                include"includes/header.php";
                include"includes/def_paths.php";
                $call_db = "GRANT_ACCESS_TO_DB";
                $ConnArray = getMySqlConnectionValues("user");
                $host = $ConnArray["HOST"];
                $user = $ConnArray["USER"];
                $pass = $ConnArray["PASS"];
                $db = $ConnArray["DB"];
                require ext_file_include('db_connectivity', 'config');
                mysql_query("update " . LOGIN_TABLE . " set UID_Password='$pas2' where UID_Username='$_SESSION[RECOVER_ACCOUNT]'") or die("There was a problem resetting your password");
                mysql_query("LOCK TABLES " . FORGOT_PASS . " write");
                mysql_query("delete from " . FORGOT_PASS . " WHERE email='$_SESSION[RECOVER_ACCOUNT]'") or die("Pass Update : ERR-PANIC");
                mysql_query("UNLOCK TABLES");
                session_destroy();
                echo "<body><div class='alert alert-info'><center>Your password has been successfully updated.</center></div><a class='navbar-brand' href='index.php'>
          <img  src='assets/img/rs_logo.png' style='margin:200px 200px 200px 230px' alt='roughsheet' /></a></body>";
            } else {
                echo"<div class='alert alert-info'><center>Passwords do not match.</center></div>";
                goto Re_Enter;
            }
        } else {
            if (!isset($_GET['rs_secure_token']) || !isset($_GET['ma'])) {
                echo"<body><div class='alert alert-info'><center><h3>Invalid Access</h3></center></div><a class='navbar-brand' href='index.php'>
          <img  src='assets/img/rs_logo.png' style='margin:200px 200px 200px 230px' alt='roughsheet' /></a></body>";
            } elseif (isset($_SESSION['ROUGHSHEET_SCI'])) {
                echo"<body><div class='alert alert-info'>An attempt to attack detected. Your connection request is logged for further processing.<br>
You can continue your session. <a href='https://roughsheet.com' class='btn btn-default'>Go To Homepage</a></div></body>";
            } else {
                $email = $_GET['ma'];
                if ($email == "" || empty($email) || $email == null || !isset($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo "<body><div class='alert alert-info'><center><h3>Email has inappropriate value. Attack vectors detected.<br>
        	Your connection request is logged for further processing.
        	</h3></center></div></body>";
                } else {
                    include"includes/header.php";
                    include"includes/def_paths.php";
                    $token_link = $_GET['rs_secure_token'];
                    $call_db = "GRANT_ACCESS_TO_DB";
                    $ConnArray = getMySqlConnectionValues("user");
                    $host = $ConnArray["HOST"];
                    $user = $ConnArray["USER"];
                    $pass = $ConnArray["PASS"];
                    $db = $ConnArray["DB"];
                    require ext_file_include('db_connectivity', 'config');
                    $chk_data = mysql_query("select * from " . FORGOT_PASS . " where email='$email' and link='$token_link'") or error_log(mysql_error());
                    $chk_data_count = mysql_num_rows($chk_data);
                    if ($chk_data_count == 0) {
                        echo"<body><div class='alert alert-info'><center>No such entry.</center></div>
            <a class='navbar-brand' href='index.php'>
          <img  src='assets/img/rs_logo.png' style='margin:200px 200px 200px 230px' alt='roughsheet' /></a>
            </body>";
                    } else {
                        $_SESSION['RECOVER_ACCOUNT'] = $email;
                        Re_Enter:
                        ?>
                        <style>
                            body{background:url(assets/img/f_bg.jpg);}
                            .f_box{
                                width:300px;
                                height:400px;
                                border-radius:5px;
                                margin:90px auto;
                                box-shadow: 0 0 2px #eee;
                                background:#fff;
                                padding:40px;
                                opacity:0.8;
                            }
                            footer{display:none;}
                        </style>    
                        <div class='f_box'>
                            <a class="navbar-brand" href="index.php">
                                <img  src="assets/img/rs_logo.png" style="width:220px;height:70px;margin-left:-20px;margin-top:-30px; " alt="roughsheet" />
                            </a>
                            </br></br>
                            <form method="POST" class="form-horizontal" style='margin-top:20px;' action="https://www.roughsheet.com/recover_rs_account.php">
                                <div class="form-group">
                                    <input type="hidden" name="hidden_flag" value="1_rs_recover" >
                                    <label>New Password:</label> 
                                    <input  type="password" class="form-control" name="pass">
                                </div>
                                <div class="form-group">
                                    <label>Re-Type Password: </label>
                                    <input type="password" class="form-control" name="cpass"><br>
                                    <input type="submit" class='btn btn-block' value="Reset">
                                </div>
                            </form>
                        </div>
                    <?php
                }
            }
        }
    }//hidden flag ends
}//server chk ends
mysql_close();
?>
</body>