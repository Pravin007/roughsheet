<?php

if ($confirm == 'YES_I_WANT_TO_MAIL') {

    function smtpmailer($to, $from, $from_name, $subject, $body) {
        /**
         * This example shows settings to use when sending via Google's Gmail servers.
         */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
        date_default_timezone_set('Etc/UTC');
        require SITEURL . 'libs/phpmailer/PHPMailerAutoload.php';
//Create a new PHPMailer instance
        $mail = new PHPMailer;
//Tell PHPMailer to use SMTP
//        $mail->isSMTP();
////Enable SMTP debugging
//// 0 = off (for production use)
//// 1 = client messages
//// 2 = client and server messages
////$mail->SMTPDebug = 2;
////Ask for HTML-friendly debug output
//        $mail->Debugoutput = 'html';
////Set the hostname of the mail server
////$mail->Host = 'relay-hosting.secureserver.net';
//        $mail->Host = '';
////Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
////$mail->Port = 25;
//        $mail->Port = 587;
////Set the encryption system to use - ssl (deprecated) or tls
//        $mail->SMTPSecure = 'tls';
        $mail->IsHTML(true);
//Whether to use SMTP authentication
//        $mail->SMTPAuth = true;
        $from_pass = '';
        if ($from == "ankur@roughsheet.com") {
            $from_pass = "Sh@dow45";
        } else if ($from == "hello@roughsheet.com") {
            $from_pass = "Hello@123";
        } else if ($from == "donotreply@roughsheet.com") {
            $from_pass = "Donotreply@123";
        } else if ($from == "recover@roughsheet.com") {
            $from_pass = "Recover@123";
        } else if ($from == "connect@roughsheet.com") {
            $from_pass = "Connect@123";
        } else if ($from == "contact@roughsheet.com") {
            $from_pass = "Contact@123";
        } else {
// if($from=="userquery@roughsheet.com"){
            $from_pass = "Userquery@123";
        }
//Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "$from";
//Password to use for SMTP authentication
        $mail->Password = "$from_pass";
//Set who the message is to be sent from
        $mail->setFrom("$from", "$from_name");
//Set an alternative reply-to address
        $mail->addReplyTo("$from", "$from_name");
//Set who the message is to be sent to
        $mail->addAddress("$to", 'User');
//Set the subject line
        $mail->Subject = $subject;
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
//$message="Client Name: $from_name \r\n";
//$message.="Client Email: $from \r\n";
//$message.="Enquiry: \r\n $body";
        $mail->Body = $body;
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
        if (!$mail->send()) {
            //echo "Mailer Error: " . $mail->ErrorInfo;
            //$er=$mail->ErrorInfo;
            echo"A problem occured while mailing. Please contact administrator.";
            //echo"<script>$('#reg_info').html('<center><h4>$er</h4></center>');</script>";
        } else {
            echo"<script>$('#reg_info').html('<center><h4>Mail Sent.</h4></center>');</script>";
        }
    }

//function ends here
} else {
    echo"ALERT: NOT AUTHORISED TO USE THE MAIL SCRIPT";
}
?>