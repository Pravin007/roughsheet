<?php
session_start();
//**IMPORTANT** If you intend to change the file structure , notify to ROUGHSHEET-CTO.
//include header file.
include"includes/header.php";
include"includes/html_head.php";
echo"<body >";
//HEADER FILE INCLUDED.
//check if user wants to logout
if (isset($_SERVER["QUERY_STRING"]) && htmlentities($_SERVER["QUERY_STRING"], ENT_QUOTES) == "Logout") {
    if (isset($_SESSION["ROUGHSHEET_SCI"])) {
        //destroy all the cookies
        //setcookie( "ROUGHSHEET_SCI", "", time()- 60 );
        session_destroy();
        //then jump on the login page.
        goto LOGIN;
    } else {
        //User is not logged in but still he requests a logout.
        //User may be a threat.
    }
} else {
    //It means user wants to surf different page.
    //any other functionality like tracking, cookie editing, or user blocking can be added here.
}
//END LOGOUT CHECK.
//SessionCheck - CookieCheck,Cookiesetting,CookieEncrypt,CookieDecrypt.
if (isset($_SESSION["ROUGHSHEET_SCI"])) {
    if ($_SESSION["ROUGHSHEET_SCI"] == null || empty($_SESSION["ROUGHSHEET_SCI"]) || $_SESSION["ROUGHSHEET_SCI"] == "") {
        //It means cookie is empty or null.
        //last value should be changed to '1' after HTTPS connection is active.
        //value '0' represents HTTP connection.
        //setcookie( "ROUGHSHEET_SCI", "", time()- 60 );
        session_destroy();
        goto LOGIN;
    } else {
        //cookie has a value.
        //authenticate it.
        //jump to set file param.
        $session = "ACTIVE";
        goto SET_FILE;
    }
} elseif (isset($_REQUEST) && ($_REQUEST['func_type'] == "register" || $_REQUEST['func_type'] == "forgotpass")) {
    goto SET_FILE;
} else {
    goto LOGIN;
}
//END OF SESSION-CHECK.
//THIS SECTION CAN BE USED TO RESTRICT USER ,IF USER IS NOT PERMITTED, TO ACCESS THE SPECIFIC PAGE.
//END USER RESTRICT.
//$_GET,$_POST,$_REQUEST Variables check for html-characters, possible SQLInjections, XSS Scripts, others(if any).
//Also Encryption and Decryption funcationality can be added.
//Call vriable check Function from header file.
if (count($_POST) != 0 || count($_GET) != 0) {
    SET_FILE:
    //avoid page visit after session has been started or even pages can be added if the user is blocked from viewing them.
    $avoid = array("login", "register");
    if (isset($session) && $session == "ACTIVE") {
        if (isset($_REQUEST['func_type'])) {
            $val = $_REQUEST['func_type'];
            //require ext_file_include('user_menu','php_sector');
            if (in_array($val, $avoid)) {
                //many more func_type's can be added to avoid page visit after session is active
                //even after logout
                $_REQUEST['func_type'] = 'homepage';
                //$file=$_REQUEST['func_type'];
            }
        }
    }
    if (isset($_REQUEST['func_type'])) {
        $file = $_REQUEST['func_type'];
    } else {
        $file = 'homepage'; //first page after login
    }
} else {
    LOGIN:
    $file = 'login'; // first page to be displayed
}
//END OF VARIABLE CHECK.
INC_FILE:
//Include main file.
//Every form's submit button must have a name of file to which values should be passed.
$file_name = ext_file_include($file, 'php_sector');
require_once $file_name;
//MAIN FILE INCLUDED.
//include footer file.
include"includes/footer.php";
//FOOTER FILE INCLUDED.
?> 