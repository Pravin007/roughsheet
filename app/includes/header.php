<?php

date_default_timezone_set('Asia/Kolkata');
ini_set("log_errors", true);
ini_set("display_errors", "off");
//DIRECT PAGE ACCESS BLOCK
include"block_access.php";

//END
$date = date("d_m_Y");
$mail_header = "
                <html>
<head>
    <style>
#outlook a { 
  padding:0; 
} 
body{ 
  width:100% !important; 
  min-width: 100%;
  -webkit-text-size-adjust:100%; 
  -ms-text-size-adjust:100%; 
  margin:0; 
  padding:0;
}
.ExternalClass { 
  width:100%;
} 
.ExternalClass, 
.ExternalClass p, 
.ExternalClass span, 
.ExternalClass font, 
.ExternalClass td, 
.ExternalClass div { 
  line-height: 100%; 
} 
#backgroundTable { 
  margin:0; 
  padding:0; 
  width:100% !important; 
  line-height: 100% !important; 
}
img { 
  outline:none; 
  text-decoration:none; 
  -ms-interpolation-mode: bicubic;
  width: auto;
  max-width: 100%; 
  float: left; 
  clear: both; 
  display: block;
}
center {
  width: 100%;
  min-width: 580px;
}
a img { 
  border: none;
}
p {
  margin: 0 0 0 10px;
}
table {
  border-spacing: 0;
  border-collapse: collapse;
}
td { 
  word-break: break-word;
  -webkit-hyphens: auto;
  -moz-hyphens: auto;
  hyphens: auto;
  border-collapse: collapse !important; 
}
table, tr, td {
  padding: 0;
  vertical-align: top;
  text-align: left;
}
hr {
  color: #6699CC; 
  background-color:#6699CC; 
  height: 1px; 
  border: none;
}
/* Responsive Grid */
table.body {
  height: 100%;
  width: 100%;
}
table.container {
  width: 580px;
  margin: 0 auto;
  text-align: inherit;
}
table.row { 
  padding: 0px; 
  width: 100%;
  position: relative;
}
table.container table.row {
  display: block;
}
td.wrapper {
  padding: 10px 20px 0px 0px;
  position: relative;
}
table.columns,
table.column {
  margin: 0 auto;
}
table.columns td,
table.column td {
  padding: 0px 0px 10px; 
}
table.columns td.sub-columns,
table.column td.sub-columns,
table.columns td.sub-column,
table.column td.sub-column {
  padding-right: 10px;
}
td.sub-column, td.sub-columns {
  min-width: 0px;
}
table.row td.last,
table.container td.last {
  padding-right: 0px;
}
table.one { width: 30px; }
table.two { width: 80px; }
table.three { width: 130px; }
table.four { width: 180px; }
table.five { width: 230px; }
table.six { width: 280px; }
table.seven { width: 330px; }
table.eight { width: 380px; }
table.nine { width: 430px; }
table.ten { width: 480px; }
table.eleven { width: 530px; }
table.twelve { width: 580px; }
table.one center { min-width: 30px; }
table.two center { min-width: 80px; }
table.three center { min-width: 130px; }
table.four center { min-width: 180px; }
table.five center { min-width: 230px; }
table.six center { min-width: 280px; }
table.seven center { min-width: 330px; }
table.eight center { min-width: 380px; }
table.nine center { min-width: 430px; }
table.ten center { min-width: 480px; }
table.eleven center { min-width: 530px; }
table.twelve center { min-width: 580px; }
table.one .panel center { min-width: 10px; }
table.two .panel center { min-width: 60px; }
table.three .panel center { min-width: 110px; }
table.four .panel center { min-width: 160px; }
table.five .panel center { min-width: 210px; }
table.six .panel center { min-width: 260px; }
table.seven .panel center { min-width: 310px; }
table.eight .panel center { min-width: 360px; }
table.nine .panel center { min-width: 410px; }
table.ten .panel center { min-width: 460px; }
table.eleven .panel center { min-width: 510px; }
table.twelve .panel center { min-width: 560px; }
.body .columns td.one,
.body .column td.one { width: 8.333333%; }
.body .columns td.two,
.body .column td.two { width: 16.666666%; }
.body .columns td.three,
.body .column td.three { width: 25%; }
.body .columns td.four,
.body .column td.four { width: 33.333333%; }
.body .columns td.five,
.body .column td.five { width: 41.666666%; }
.body .columns td.six,
.body .column td.six { width: 50%; }
.body .columns td.seven,
.body .column td.seven { width: 58.333333%; }
.body .columns td.eight,
.body .column td.eight { width: 66.666666%; }
.body .columns td.nine,
.body .column td.nine { width: 75%; }
.body .columns td.ten,
.body .column td.ten { width: 83.333333%; }
.body .columns td.eleven,
.body .column td.eleven { width: 91.666666%; }
.body .columns td.twelve,
.body .column td.twelve { width: 100%; }
td.offset-by-one { padding-left: 50px; }
td.offset-by-two { padding-left: 100px; }
td.offset-by-three { padding-left: 150px; }
td.offset-by-four { padding-left: 200px; }
td.offset-by-five { padding-left: 250px; }
td.offset-by-six { padding-left: 300px; }
td.offset-by-seven { padding-left: 350px; }
td.offset-by-eight { padding-left: 400px; }
td.offset-by-nine { padding-left: 450px; }
td.offset-by-ten { padding-left: 500px; }
td.offset-by-eleven { padding-left: 550px; }
td.expander {
  visibility: hidden;
  width: 0px;
  padding: 0 !important;
}
table.columns .text-pad,
table.column .text-pad {
  padding-left: 10px;
  padding-right: 10px;
}
table.columns .left-text-pad,
table.columns .text-pad-left,
table.column .left-text-pad,
table.column .text-pad-left {
  padding-left: 10px;
}
table.columns .right-text-pad,
table.columns .text-pad-right,
table.column .right-text-pad,
table.column .text-pad-right {
  padding-right: 10px;
}
/* Block Grid */
.block-grid {
  width: 100%;
  max-width: 580px;
}
.block-grid td {
  display: inline-block;
  padding:10px;
}
.two-up td {
  width:270px;
}
.three-up td {
  width:173px;
}
.four-up td {
  width:125px;
}
.five-up td {
  width:96px;
}
.six-up td {
  width:76px;
}
.seven-up td {
  width:62px;
}
.eight-up td {
  width:52px;
}
/* Alignment & Visibility Classes */
table.center, td.center {
  text-align: center;
}
h1.center,
h2.center,
h3.center,
h4.center,
h5.center,
h6.center {
  text-align: center;
}
span.center {
  display: block;
  width: 100%;
  text-align: center;
}
img.center {
  margin: 0 auto;
  float: none;
}
.show-for-small,
.hide-for-desktop {
  display: none;
}
/* Typography */
body, table.body, h1, h2, h3, h4, h5, h6, p, td { 
  color: #222222;
  font-family: 'Helvetica', 'Arial', sans-serif; 
  font-weight: normal; 
  padding:0; 
  margin: 0;
  text-align: left; 
  line-height: 1.3;
}
h1, h2, h3, h4, h5, h6 {
  word-break: normal;
}
h1 {font-size: 40px;}
h2 {font-size: 36px;}
h3 {font-size: 32px;}
h4 {font-size: 28px;}
h5 {font-size: 15px;}
h6 {font-size: 20px;}
body, table.body, p, td {font-size: 14px;line-height:19px;}
p.lead, p.lede, p.leed {
  font-size: 18px;
  line-height:21px;
}
p { 
  margin-bottom: 10px;
}
small {
  font-size: 10px;
}
a {
  color: #2ba6cb; 
  text-decoration: none;
}
a:hover { 
  color: #2795b6 !important;
}
a:active { 
  color: #2795b6 !important;
}
a:visited { 
  color: #2ba6cb !important;
}
h1 a, 
h2 a, 
h3 a, 
h4 a, 
h5 a, 
h6 a {
  color: #2ba6cb;
}
h1 a:active, 
h2 a:active,  
h3 a:active, 
h4 a:active, 
h5 a:active, 
h6 a:active { 
  color: #2ba6cb !important; 
} 
h1 a:visited, 
h2 a:visited,  
h3 a:visited, 
h4 a:visited, 
h5 a:visited, 
h6 a:visited { 
  color: #2ba6cb !important; 
} 
.panel {
  background: #f2f2f2;
  border: 1px solid #d9d9d9;
  padding: 10px !important;
}
.sub-grid table {
  width: 100%;
}
.sub-grid td.sub-columns {
  padding-bottom: 0;
}
table.button,
table.tiny-button,
table.small-button,
table.medium-button,
table.large-button {
  width: 100%;
  overflow: hidden;
}
table.button td,
table.tiny-button td,
table.small-button td,
table.medium-button td,
table.large-button td {
  display: block;
  width: auto !important;
  text-align: center;
  background: #2ba6cb;
  border: 1px solid #2284a1;
  color: #ffffff;
  padding: 8px 0;
}
table.tiny-button td {
  padding: 5px 0 4px;
}
table.small-button td {
  padding: 8px 0 7px;
}
table.medium-button td {
  padding: 12px 0 10px;
}
table.large-button td {
  padding: 21px 0 18px;
}
table.button td a,
table.tiny-button td a,
table.small-button td a,
table.medium-button td a,
table.large-button td a {
  font-weight: bold;
  text-decoration: none;
  font-family: Helvetica, Arial, sans-serif;
  color: #ffffff;
  font-size: 16px;
}
table.tiny-button td a {
  font-size: 12px;
  font-weight: normal;
}
table.small-button td a {
  font-size: 16px;
}
table.medium-button td a {
  font-size: 20px;
}
table.large-button td a {
  font-size: 24px;
}
table.button:hover td,
table.button:visited td,
table.button:active td {
  background: #2795b6 !important;
}
table.button:hover td a,
table.button:visited td a,
table.button:active td a {
  color: #fff !important;
}
table.button:hover td,
table.tiny-button:hover td,
table.small-button:hover td,
table.medium-button:hover td,
table.large-button:hover td {
  background: #2795b6 !important;
}
table.button:hover td a,
table.button:active td a,
table.button td a:visited,
table.tiny-button:hover td a,
table.tiny-button:active td a,
table.tiny-button td a:visited,
table.small-button:hover td a,
table.small-button:active td a,
table.small-button td a:visited,
table.medium-button:hover td a,
table.medium-button:active td a,
table.medium-button td a:visited,
table.large-button:hover td a,
table.large-button:active td a,
table.large-button td a:visited {
  color: #ffffff !important; 
}
table.secondary td {
  background: #e9e9e9;
  border-color: #d0d0d0;
  color: #555;
}
table.secondary td a {
  color: #555;
}
table.secondary:hover td {
  background: #d0d0d0 !important;
  color: #555;
}
table.secondary:hover td a,
table.secondary td a:visited,
table.secondary:active td a {
  color: #555 !important;
}
table.success td {
  background: #5da423;
  border-color: #457a1a;
}
table.success:hover td {
  background: #457a1a !important;
}
table.alert td {
  background: #c60f13;
  border-color: #970b0e;
}
table.alert:hover td {
  background: #970b0e !important;
}
table.radius td {
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
}
table.round td {
  -webkit-border-radius: 500px;
  -moz-border-radius: 500px;
  border-radius: 500px;
}
body.outlook p {
  display: inline !important;
}
@media only screen and (max-width: 600px) {
  table[class='body'] img {
    width: auto !important;
    height: auto !important;
  }
  table[class='body'] center {
    min-width: 0 !important;
  }
  table[class='body'] .container {
    width: 95% !important;
  }
  table[class='body'] .row {
    width: 100% !important;
    display: block !important;
  }
  table[class='body'] .wrapper {
    display: block !important;
    padding-right: 0 !important;
  }
  table[class='body'] .columns,
  table[class='body'] .column {
    table-layout: fixed !important;
    float: none !important;
    width: 100% !important;
    padding-right: 0px !important;
    padding-left: 0px !important;
    display: block !important;
  }
  table[class='body'] .wrapper.first .columns,
  table[class='body'] .wrapper.first .column {
    display: table !important;
  }
  table[class='body'] table.columns td,
  table[class='body'] table.column td {
    width: 100% !important;
  }
  table[class='body'] .columns td.one,
  table[class='body'] .column td.one { width: 8.333333% !important; }
  table[class='body'] .columns td.two,
  table[class='body'] .column td.two { width: 16.666666% !important; }
  table[class='body'] .columns td.three,
  table[class='body'] .column td.three { width: 25% !important; }
  table[class='body'] .columns td.four,
  table[class='body'] .column td.four { width: 33.333333% !important; }
  table[class='body'] .columns td.five,
  table[class='body'] .column td.five { width: 41.666666% !important; }
  table[class='body'] .columns td.six,
  table[class='body'] .column td.six { width: 50% !important; }
  table[class='body'] .columns td.seven,
  table[class='body'] .column td.seven { width: 58.333333% !important; }
  table[class='body'] .columns td.eight,
  table[class='body'] .column td.eight { width: 66.666666% !important; }
  table[class='body'] .columns td.nine,
  table[class='body'] .column td.nine { width: 75% !important; }
  table[class='body'] .columns td.ten,
  table[class='body'] .column td.ten { width: 83.333333% !important; }
  table[class='body'] .columns td.eleven,
  table[class='body'] .column td.eleven { width: 91.666666% !important; }
  table[class='body'] .columns td.twelve,
  table[class='body'] .column td.twelve { width: 100% !important; }
  table[class='body'] td.offset-by-one,
  table[class='body'] td.offset-by-two,
  table[class='body'] td.offset-by-three,
  table[class='body'] td.offset-by-four,
  table[class='body'] td.offset-by-five,
  table[class='body'] td.offset-by-six,
  table[class='body'] td.offset-by-seven,
  table[class='body'] td.offset-by-eight,
  table[class='body'] td.offset-by-nine,
  table[class='body'] td.offset-by-ten,
  table[class='body'] td.offset-by-eleven {
    padding-left: 0 !important;
  }
  table[class='body'] table.columns td.expander {
    width: 1px !important;
  }
  table[class='body'] .right-text-pad,
  table[class='body'] .text-pad-right {
    padding-left: 10px !important;
  }
  table[class='body'] .left-text-pad,
  table[class='body'] .text-pad-left {
    padding-right: 10px !important;
  }
  table[class='body'] .hide-for-small,
  table[class='body'] .show-for-desktop {
    display: none !important;
  }
  table[class='body'] .show-for-small,
  table[class='body'] .hide-for-desktop {
    display: inherit !important;
  }
}
  </style>
  <style>
    table.facebook td {
      background: #3b5998;
      border-color: #2d4473;
    }
    table.facebook:hover td {
      background: #2d4473 !important;
    }
    table.linkedin td {
      background-color: #007bb6;
      border-color: #CC0000;
    }
    table.linkedin:hover td {
      background: #CC0000 !important;
    }
    table.twitter td {
      background: #00acee;
      border-color: #0087bb;
    }
    table.twitter:hover td {
      background: #0087bb !important;
    }
    .template-label {
      color: #ffffff;
      font-weight: bold;
      font-size: 11px;
    }
    .callout .wrapper {
      padding-bottom: 20px;
    }
    .callout .panel {
      background: #ECF8FF;
      border-color: #b9e5ff;
    }
    .header {
      background: #999999;
    }
    .footer .wrapper {
      background: #ebebeb;
    }
    .footer h5 {
      padding-bottom: 10px;
    }
    table.columns .text-pad {
      padding-left: 10px;
      padding-right: 10px;
    }
    table.columns .left-text-pad {
      padding-left: 10px;
    }
    table.columns .right-text-pad {
      padding-right: 10px;
    }
    @media only screen and (max-width: 600px) {
      table[class='body'] .right-text-pad {
        padding-left: 10px !important;
      }
      table[class='body'] .left-text-pad {
        padding-right: 10px !important;
      }
    }
    </style>
</head>
<body>
    <table class='body' style='width:100%;'>
        <tr>
            <td class='center' align='center' valign='top'>
        <center>
          <table class='row header'>
            <tr>
              <td class='center' align='center'>
                <center>
                  <table class='container'>
                    <tr>
                      <td class='wrapper last'>
                        <table class='twelve columns'>
                          <tr>
                            <td class='six sub-columns'>
                              <img src='http://www.roughsheet.com/assets/images/180_30_logo_size.png' alt='Logo' width='200' height='45'>
                            </td>
                            <td class='six sub-columns last' style='text-align:right; vertical-align:middle;'>
                            </td>
                            <td class='expander'></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </center>
              </td>
            </tr>
          </table>
          <table class='container'>
            <tr>
              <td>
                <table class='row'>
                  <tr>
                    <td class='wrapper last'>
                      <table class='twelve columns'>
                        <tr>
                          <td>
";
$mail_footer = "
</td>
                          <td class='expander'></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
<table class='row footer'>
                  <tr>
                    <td class='wrapper'>
                      <table class='six columns'>
                        <tr>
                          <td class='left-text-pad'>
                            <h5>Connect With Us:</h5>
                            <table class='tiny-button facebook'>
                              <tr>
                                <td>
                                  <a href='https://www.facebook.com/roughsheet'>Facebook</a>
                                </td>
                              </tr>
                            </table>
                            <br>
                            <table class='tiny-button twitter'>
                              <tr>
                                <td>
                                  <a href='https://twitter.com/roughsheetinc'>Twitter</a>
                                </td>
                              </tr>
                            </table>
                            <br>
                            <table class='tiny-button google-plus'>
                              <tr>
                                <td>
                                  <a href='https://www.linkedin.com/company/roughsheet'>LinkedIn</a>
                                </td>
                              </tr>
                            </table>
                          </td>
                          <td class='expander'></td>
                        </tr>
                      </table>
                    </td>
                    <td class='wrapper last'>
                      <table class='six columns'>
                        <tr>
                          <td class='last right-text-pad'>
                            <h5>We value your suggestions, and hence, please do not hesitate to get in touch with us.</h5>
                            <p>Email: <a href='contact@roughsheet.com'>contact@roughsheet.com</a></p>
                          </td>
                          <td class='expander'></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <table class='row'>
                  <tr>
                    <td class='wrapper last'>
                      <table class='twelve columns'>
                        <tr>
                          <td align='center'>
                            <center>
                              <p style='text-align:center;'><a href='http://$servername/app/legal_docs/Terms_Of_Use.html'>Terms</a> | <a href='http://$servername/app/legal_docs/Privacy_Policy.html'>Privacy</a></p>
                            </center>
                          </td>
                          <td class='expander'></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </center>
            </td>
        </tr>
    </table>
</body>
</html>
";

//$error_act_path=realpath(dirname(dirname(dirname(dirname(_FILE__)))));
//ini_set("error_log",$error_act_path . "/roughsheet_secure_files/errors/error_log_$date");
// CONTACT ROUGHSHEET-CTO BEFORE MODIFYING THESE PATHS
function check_variables_for_security() {
//This function (when called) checks every value submitted through a form.
//Modification may be done so as to validate different type of key-value pair like email, date of birth,etc.
//htmlentities will encode every every special character into htmlcharacter. Here we have saved it from xss attacks.
    foreach ($_REQUEST as $key => $value) {
        $_REQUEST[$key] = htmlentities($value, ENT_QUOTES);
    }
    foreach ($_COOKIE as $key_c => $value_c) {
        $_COOKIE[$key_c] = htmlentities($value_c, ENT_QUOTES);
    }
    //print_r($_REQUEST);
}

function getMySqlConnectionValues($user) {
    $conn_array = array("HOST" => "",
        "USER" => "",
        "PASS" => "",
        "DB" => "");
    if ($_SERVER['SERVER_NAME'] != 'localhost') {
        $conn_array = array(
            "HOST" => "roughsheet-test.co8lze8dd3vj.us-west-2.rds.amazonaws.com",
            "USER" => "",
            "PASS" => "",
            "DB" => "roughsheettest"
        );
        if ($user == "login_user") {
            $conn_array["USER"] = "roughtest";
            $conn_array["PASS"] = "linosys123";
        } elseif ($user == "user") {
            $conn_array["USER"] = "roughtest";
            $conn_array["PASS"] = "linosys123";
        } elseif ($user == "admin" || $user == "developer") {
            $conn_array["USER"] = "roughtest";
            $conn_array["PASS"] = "linosys123";
        } elseif ($user == "investor") {
            $conn_array["USER"] = "roughtest";
            $conn_array["PASS"] = "linosys123";
        } else {
            return false;
        }
    } else {
        $conn_array = array(
            "HOST" => "localhost",
            "USER" => "",
            "PASS" => "",
            "DB" => "roughsheet"
        );
        if ($user == "login_user") {
            $conn_array["USER"] = "root";
            $conn_array["PASS"] = "";
        } elseif ($user == "user") {
            $conn_array["USER"] = "root";
            $conn_array["PASS"] = "";
        } elseif ($user == "admin" || $user == "developer") {
            $conn_array["USER"] = "root";
            $conn_array["PASS"] = "";
        } elseif ($user == "investor") {
            $conn_array["USER"] = "root";
            $conn_array["PASS"] = "";
        } else {
            return false;
        }
    }
//    $conn_array = array(
//        "HOST" => "localhost",
//        "USER" => "linosys_pravin",
//        "PASS" => "Pr@v!n007",
//        "DB" => "linosys_roughsheet"
//    );
    return $conn_array;
}

function encryptPass($pass) {
    $salt = "kjhg^&%jbhv^%r";
    return md5($pass . $salt);
}

function str_encrypt_n_decrypt($string, $function_type) {
//string data can be encrypted here.
//$function_type can take values {enc,dec} i.e. `enc` for encrypt and `dec` for decrypt.
//ecryption & decryption functions can be replaced here to provide maximum security.
    $return_str = null; //setting up variable with `null` value.
    if ($function_type == 'enc') {
        $return_str = base64_encode($string);
    } else if ($function_type == 'dec') {
        $return_str = base64_decode($string);
    } else {
        //Warning: Secrity functions must be handeled with care.
    }
    return $return_str;
}

function ext_file_include($file, $type) {
//type will denote in which folder to look for the file
//ex. type=config => config folder, type=php_sector => php_sector.
//If we do not want to reveal the page names then we can add other functionality over here that will map you to real name of file.
//ex. file=login => actual_login_script , but at present it maps it directly i.e. file=login => login .
    $file_name = $file . '.php';
    if ($type == 'config') {
        $chk_file = CONFIG . $file_name;
    } elseif ($type == 'php_sector') {
        $chk_file = BASEPATH . $file_name;
    } elseif ($type == "res_2_config") {
        $chk_file = RES_2_CONFIG . $file_name;
    } elseif ($type == "res_2_sector") {
        $chk_file = RES_2_SECTOR . $file_name;
    } else {
        $chk_file = 'NO_SUCH_PATH_AVAILABLE';
    }
    if (!is_file($chk_file)) {
        $file_name = 'missing.php';
    }
    return $chk_file;
}

function setShadow() {
    echo"
    <style type='text/css'>
     .shadow{
        position: fixed;
	 top: 0;
	  right: 0;	
	  bottom: 0;
	  left: 0;
	  z-index: 1030;
	  background-color: #333333;
	  opacity:0.8; // I ADDED THIS LINE 
 }
    .panel{
        position:absolute;
        min-height:90%;
        min-width:100%;
        z-index:1040;
    }
    .rank{
        z-index:0;
    }
    .u_menu{
    z-index:-10;
    }
</style>
    ";
}

function call_pic($width_img, $height_img, $class) {
    echo "<img src='app/includes/res_include.php' width='$width_img' height='$height_img' class='$class' />";
}

function getUID() {
    $uid = 0;
    if (isset($_SESSION['ROUGHSHEET_SCI']) && $_SESSION['ROUGHSHEET_SCI'] != "" && !empty($_SESSION['ROUGHSHEET_SCI'])) {
        $cookie = str_encrypt_n_decrypt($_SESSION['ROUGHSHEET_SCI'], 'dec');
        $cookie_vars = explode('_', $cookie);
        $uid = $cookie_vars[3];
    }
    return $uid;
}

function getAllUID() {
    $q = mysql_query("select UID from " . LOGIN_TABLE) or die(mysql_error());
    $uid_arr = array();
    while ($d = mysql_fetch_assoc($q))
        $uid_arr[] = $d['UID'];
    return $uid_arr;
}

function get_pic_name() {
    $cookie = str_encrypt_n_decrypt($_SESSION['ROUGHSHEET_SCI'], 'dec');
    $cookie_vars = explode('_', $cookie);
    return $cookie_vars[6];
}

function checkIntroStatus($val) {
    //val => qid
    $Question_intro_check_q = mysql_query("select Introduction from " . QUESTIONS . " where id='$val'") or die(mysql_error());
    $intro_data = mysql_fetch_assoc($Question_intro_check_q);
    if ($intro_data['Introduction'] == "" || $intro_data['Introduction'] == null || empty($intro_data['Introduction'])) {
        $ms = false;
    } else {
        $msg = true;
    }
    return $msg;
}

function getFriendsAndRequests() {
    $uid = getUID();
    $friends_q = mysql_query(" select * from " . FRIENDS_TABLE . " where UID='$uid' ") or die(mysql_error());
    $friends_data = mysql_fetch_assoc($friends_q);
    return $friends_data;
}

function getRemainingIds($qid) {
    //for intro ques
    $q_ids = mysql_query("select id from " . QUESTIONS . " where Introduction=(SELECT Introduction FROM " . QUESTIONS . " where id=$qid ) ") or error_log(mysql_error());
    $ids = array();
    $p = 0;
    while ($q_d = mysql_fetch_assoc($q_ids)) {
        $ids[$p] = $q_d['id'];
        $p++;
    }
    return $ids;
}

function updateDppFlag($c, $i, $u) {
    //update dpp flag
    //c: correct, i: incorrect, u: unattempted, a: accuracy
    //calculating accuracy
    $acc_tot = $c + $i;
    $acc_perc = $c * 100 / $acc_tot;
    //accuracy calculated
    $uid = getUID();
    $topicid = $_SESSION['topic_id'];
    $subid = getSubId($topicid);
    $ex_q = mysql_query("SELECT * FROM " . DPP_LOG . " WHERE UID='$uid' and sub_id='$subid'") or die(mysql_error());
    $ex_d_count = mysql_num_rows($ex_q);
    if (!isset($acc_perc) || $acc_perc == null || $acc_perc == "" || $acc_perc == false) {
        $acc_perc = 0;
    }
    if ($ex_d_count == 0) {
        $dpp_arr = array();
        $dpp_arr[0] = array($c, $i, $u, $acc_perc);
        $dpp_time_arr[0] = strtotime(date("H:i:s"));
        $enc_dpp_arr = json_encode($dpp_arr);
        $enc_dpp_time_arr = json_encode($dpp_time_arr);
        mysql_query("INSERT INTO " . DPP_LOG . "(UID,sub_id,data,dpp_time) VALUES('$uid','$subid','$enc_dpp_arr','$enc_dpp_time_arr')") or error_log(mysql_error());
    } else {
        $ex_d = mysql_fetch_assoc($ex_q);
        $dpp_arr = json_decode($ex_d['data']);
        $dpp_arr_count = count($dpp_arr);
        $dpp_time_arr = json_decode($ex_d['dpp_time']);
        $data_arr_formation = array($c, $i, $u, $acc_perc);
        //$enc_data_arr_formation=json_encode($data_arr_formation);
        $dpp_arr[$dpp_arr_count] = $data_arr_formation;
        $dpp_time_arr[$dpp_arr_count] = strtotime(date("H:i:s"));
        $enc_dpp_arr = json_encode($dpp_arr);
        $enc_dpp_time_arr = json_encode($dpp_time_arr);
        mysql_query("Update " . DPP_LOG . " set data='$enc_dpp_arr', dpp_time='$enc_dpp_time_arr' where UID='$uid' and sub_id='$subid'") or die(mysql_error());
    }
}

function updateExFlag() {
    //update exercise flag
    $uid = getUID();
    $ex_type = $_SESSION['ex_type'];
    $topicid = $_SESSION['topic_id'];
    $ex_q = mysql_query("SELECT * FROM " . READ_LOG . " WHERE UID='$uid'") or die(mysql_error());
    $ex_d = mysql_fetch_assoc($ex_q);
    $ex_str = "e$ex_type";
    $ex_arr = json_decode($ex_d[$ex_str]);
    if (is_array($ex_arr)) {
        $ex_count = count($ex_arr);
        if (!in_array($topicid, $ex_arr)) {
            $ex_arr[$ex_count] = $topicid;
        }
    } else {
        $ex_arr[0] = $topicid;
    }
    $enc_ex_arr = json_encode($ex_arr);
    if (mysql_query("Update " . READ_LOG . " SET `e" . $ex_type . "`='$enc_ex_arr' WHERE UID='$uid'")) {
        
    } else {
        echo"Couldnt update exercise";
    }
}

function getAllSubIds() {
    $q = mysql_query("select sub_id from " . SUBJECTS) or die(mysql_error());
    $sub_arr = array();
    while ($d = mysql_fetch_assoc($q)) {
        $sub_arr[] = $d['sub_id'];
    }
    return $sub_arr;
}

function getSubId($tid) {
    $q = mysql_query("select sub_id from " . TOPICS . " where topic_id='$tid'") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    return $d['sub_id'];
}

function validStatus($sid) {
    $uid = getUID();
    $topic_id_arr = getAllTopicIds($sid);
    $msg = "VALID";
    foreach ($topic_id_arr as $k => $tid) {
        //error_log("tia : $tid ::" . print_r($topic_id_arr,true));
        $q = mysql_query("select * from " . DPP_LOG . " where UID='$uid' and sub_id='$sid'") or die(mysql_error());
        $c = mysql_num_rows($q);
        if ($c != 0) {
            $d = mysql_fetch_assoc($q);
            $dpp_arr = json_decode($d['dpp_time']);
            $dpp_arr_c = count($dpp_arr);
            $dpp_arr_c--;
            //error_log("DPP TIME : $tid ::" . print_r($dpp_arr,true));
            $now = new DateTime(date("Y-m-d H:i:s"));
            $dpp_time = new DateTime(date("Y-m-d H:i:s", "$dpp_arr[$dpp_arr_c]"));
            $diff = $dpp_time->diff($now);
            //error_log("DIFF : > " . $diff->days);
            $tot_days = $diff->days;
            if ($tot_days <= 0) {
                $msg = "INVALID";
                goto RET_MSG;
            }
        }
    }
    /*
      $q=mysql_query("select * from " . DPP_LOG . " where UID='$uid' and topic_id='$tid'") or die(mysql_error());
      $c=mysql_num_rows($q);
      if($c==0){
      $msg="VALID";
      }
      else{
      $d=mysql_fetch_assoc($q);
      $dpp_arr=json_decode($d['dpp_time']);
      $dpp_arr_c=count($dpp_arr);
      $dpp_arr_c--;
      $latest_dpp_time=new DateTime(date("Y-m-d H:i:s","$dpp_arr[$dpp_arr_c]"));
      $now=new DateTime(date("Y-m-d H:i:s"));
      $diff=$latest_dpp_time->diff($now);
      if($diff->days >= 1){
      $msg="VALID";
      }
      else{
      //day hasnt past yet
      $msg="INVALID";
      }
      }
     */
    RET_MSG:
    return $msg;
}

function getAllTopicIds($sub_id) {
    //get all topic ids where $tid lies
    $q = mysql_query("select topic_id from " . TOPICS . " Where sub_id='$sub_id'") or die(mysql_error());
    $id_arr = array();
    $p = 0;
    while ($d = mysql_fetch_assoc($q)) {
        $id_arr[$p] = $d['topic_id'];
        $p++;
    }
    return $id_arr;
}

function getTopicName($tid) {
    $q = mysql_query("select topic_name from " . TOPICS . " WHERE topic_id='$tid'") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    return $d['topic_name'];
}

function getSubName($sub_id) {
    $q = mysql_query("select sub_name from " . SUBJECTS . " WHERE sub_id='$sub_id'") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    return $d['sub_name'];
}

function getLoginCount() {
    $sess_val = str_encrypt_n_decrypt($_SESSION['ROUGHSHEET_SCI'], 'dec');
    $sess_vars = explode('_', $sess_val);
    return $sess_vars[7];
}

function getDppData($user, $sub_id) {
    $q = mysql_query("select data,dpp_time from " . DPP_LOG . " where UID=$user and sub_id=$sub_id ") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    $dpp_data = json_decode($d['data']);
    $dpp_time = json_decode($d['dpp_time']);
    $dpp_dates = array();
    foreach ($dpp_time as $key => $val) {
        $dpp_dates[$key] = date('Y-m-d', $val);
    }
    $data['dpp_data'] = $dpp_data;
    $data['dpp_dates'] = $dpp_dates;
    return $data;
}

function chk_dpp_flag_for_subject($uid, $subid) {
    $q = mysql_query("select id from " . DPP_LOG . " where sub_id=$subid and UID=$uid ") or die(mysql_error());
    $c = mysql_num_rows($q);
    if ($c > 0) {
        return true;
    } else {
        return false;
    }
}

function getInviteCode() {
    $uid = getUID();
    $q = mysql_query("select invitation_code from " . USER_INFO . " where UID='$uid'") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    return $d['invitation_code'];
}

function curlReq($url, $to, $type, $vars) {
    // create curl resource
    $str = "";
    if ($type == 'verify') {
        $random = $vars['random'];
        $str = "&type=verify&random=$random";
    } else if ($type == 'ic') {
        $ic = $vars['ic'];
        $str = "&type=ic&ic=$ic";
    } elseif ($type == 'update') {
        $to_str = $_GET['to'];
        $str = "&type=update&to=$to";
    } elseif ($type == "welcome_acc") {
        $to_str = $_GET['to'];
        $str = "&type=welcome_acc&to=$to&request";
    }
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, $url . "?send_another_mail_with_the_script=sendanothermailwiththecurlscriptthroughserverrequest&to=$to&$str");
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string
    echo $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);
}

function getLatestDppMarksForAvg1($uid, $subid) {
    $dpp_data = getDppData($uid, $subid);
    $time_count = count($dpp_data['dpp_dates']);
    $time_count--;
    $time_count;
    $control_unit = 4;
    $control_counter = 0;
    $value = 0;
    for ($i = $time_count; $i >= 0; $i--) {
        if ($control_counter >= $control_unit) {
            break;
        } else {
            $value = $value + $dpp_data[$i][0]; //correct marks value from dpp data.
        }
        $control_counter++;
    }
    return $value / $control_counter;
}

function getUserInfo($uid) {
    $f_d_query = mysql_query("select * from " . USER_INFO . " where UID='$uid'") or die(mysql_error());
    $f_d_fetch = mysql_fetch_assoc($f_d_query);
    return $f_d_fetch;
}

function getKc($topic) {
    $q = mysql_query("select * from " . KEY_CONCEPTS . " where topic_id=$topic") or die(mysql_error());
    $d = mysql_fetch_assoc($q);
    return $d['kc_text'];
}

check_variables_for_security();
?>
