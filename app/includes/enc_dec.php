<?php
include"block_access.php";
function str_encrypt_n_decrypt($string,$function_type){
//string data can be encrypted here.
//$function_type can take values {enc,dec} i.e. `enc` for encrypt and `dec` for decrypt.
//ecryption & decryption functions can be replaced here to provide maximum security.
    $return_str=null;//setting up variable with `null` value.
    if($function_type=='enc'){
        $return_str=base64_encode($string);
    }
    else if($function_type=='dec'){
        $return_str=base64_decode($string);
    }
    else{
        //Warning: Secrity functions must be handeled with care.
    }
    return $return_str;
}
?>