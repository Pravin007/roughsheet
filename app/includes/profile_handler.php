<?php

session_start();
include"block_access.php";
include"session_manager_for_res.php";
if (isset($no_sess)) {
    header('Location: show_err.php');
} else {
    $page = $_POST['page'];
    if (isset($page)) {
        //print_r($_REQUEST);
        include(RES_2_SECTOR . "profile_data.php");
    } elseif (isset($_POST["pro_pic"])) {
        //can initialize vars here
        include(RES_2_SECTOR . "profile_data.php");
    } elseif (isset($_GET['fid'])) {
        $fid = htmlspecialchars($_GET['fid'], ENT_QUOTES);
        include(RES_2_SECTOR . "profile_data.php");
    } else {
        
    }
}
?>