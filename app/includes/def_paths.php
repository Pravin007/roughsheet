<?php

if (isset($include_def) && $include_def != 'YES') {
    die("DIRECT ACCESS");
} else {
    $servername = $_SERVER['SERVER_NAME'];
    if ($servername == 'localhost') {
        $servername = 'localhost/roughsheet';
    }
    $php_files_repository = dirname(dirname(dirname(__FILE__))) . "/app/roughsheet_secure_files"; //path from were php files are to be accessed.
    $string = realpath($php_files_repository);
    $system_path = rtrim($string, '/') . '/';

    defined("BASEPATH") ? "" : define('BASEPATH', str_replace("\\", "/", $system_path . 'php_sector/'));
    defined("CONFIG") ? "" : define('CONFIG', 'database/config/');
    defined("START_PATH") ? "" : define('START_PATH', 'http://' . $servername);
    defined("CROPPER") ? "" : define('CROPPER', 'http://' . $servername . '/libs/jcrop/crop.php');
    defined("IMAGE_PATH") ? "" : define('IMAGE_PATH', 'assets/images/');
    defined("RES_2_CONFIG") ? "" : define('RES_2_CONFIG', '../../database/config/');
    defined("RES_2_SECTOR") ? "" : define('RES_2_SECTOR', BASEPATH);
    defined("RES_2_DATA") ? "" : define('RES_2_DATA', str_replace("\\", "/", $system_path));
    defined("RES_2_IMG") ? "" : define('RES_2_IMG', '../../assets/images/');
    defined("ACC_TYPE") ? "" : define('ACC_TYPE', 'rs_account_type_table_1423552512');
    defined("LOGIN_TABLE") ? "" : define('LOGIN_TABLE', 'rs_login_table_1423552512');
    defined("USER_INFO") ? "" : define('USER_INFO', 'rs_user_info_1423552512');
    defined("EDU_INFO") ? "" : define('EDU_INFO', 'rs_edu_info_1423552512');
    defined("SUBJECTS") ? "" : define('SUBJECTS', 'rs_subjects_1423552512');
    defined("QUESTIONS") ? "" : define('QUESTIONS', 'rs_questions_db_24052015');
    defined("FRIENDS_TABLE") ? "" : define('FRIENDS_TABLE', 'rs_friends_26052015');
    defined("TOPICS") ? "" : define('TOPICS', 'rs_sub_topics_23052015');
    defined("KEY_CONCEPTS") ? "" : define('KEY_CONCEPTS', 'rs_key_concepts_27062015');
    defined("READ_LOG") ? "" : define('READ_LOG', 'rs_read_log');
    defined("OTHER_INFO") ? "" : define('OTHER_INFO', 'rs_other_info_5072015');
    defined("NEW_USER") ? "" : define('NEW_USER', 'rs_new_user_1472015');
    defined("DPP_LOG") ? "" : define('DPP_LOG', 'rs_dpp_log');
    defined("TPP_LOG") ? "" : define('TPP_LOG', 'rs_tpp_log');
    defined("SCHEDULE_DATA") ? "" : define('SCHEDULE_DATA', 'rs_schedule_data');
    defined("FORGOT_PASS") ? "" : define('FORGOT_PASS', 'rs_forgot_pass_9082015');
    defined("RSS_NAVIGATION") ? "" : define('RSS_NAVIGATION', 'rs_rss_navigation');
    defined("RSS_SOURCES") ? "" : define('RSS_SOURCES', 'rs_rss_sources');
    defined("QUOTE_LIST") ? "" : define('QUOTE_LIST', 'rs_quotes_5082015');
    defined("RSS_SOURCE_LINKS") ? "" : define('RSS_SOURCE_LINKS', 'rs_rss_source_links');
    defined("INVITATION_REQUESTS") ? "" : define('INVITATION_REQUESTS', 'rs_invitation_requests');
}
?>
