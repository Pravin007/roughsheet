<?php

session_start();
include"block_access.php";
include"session_manager_for_res.php";
if (isset($no_sess)) {
    header('Location: show_err.php');
} else {
    $page = strtoupper($_POST['page']);
    if (isset($page)) {
        if ($page == "STUDY MATERIAL") {
            include(RES_2_SECTOR . "study_mat.php");
        } elseif ($page == "DPP") {
            include(RES_2_SECTOR . "dpp.php");
        } elseif ($page == "FOCUS") {
            include(RES_2_SECTOR . "focus_area.php");
        } elseif ($page == "PERFORMANCE") {
            include(RES_2_SECTOR . "performance.php");
        } elseif ($page == "GALACTICOS") {
            include(RES_2_SECTOR . "galacticos.php");
        } elseif ($page == "DOCS") {
            include(RES_2_SECTOR . "documents.php");
        } elseif ($page == "INTERESTS") {
            //INTERESTS
            include(RES_2_SECTOR . "interests.php");
        } elseif ($page == "PROFILE") {
            //PROFILE
            include(RES_2_SECTOR . "profile.php");
        } elseif ($page == "SCHEDULER") {
            //PROFILE
            include(RES_2_SECTOR . "scheduler.php");
        }
    } else {
        include(RES_2_SECTOR . "feeds.php");
    }
}
?>