<?php
session_start();
?>
<?php
include"header.php";
include"session_manager_for_res.php";
include"def_paths.php";
$call_db = "GRANT_ACCESS_TO_DB";
$ConnArray = getMySqlConnectionValues("user");
$host = $ConnArray["HOST"];
$user = $ConnArray["USER"];
$pass = $ConnArray["PASS"];
$db = $ConnArray["DB"];
$servername = $_SERVER['SERVER_NAME'];
if ($servername == 'localhost') {
    $servername = 'localhost/roughsheet';
}
require ext_file_include('db_connectivity', 'res_2_config');
$subid = 0;
if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "SUB") {
    setShadow();
    $subid = $_POST['subid'];
    $topicid = $_POST['topicid'];
    $_SESSION['topic_id'] = $topicid;
    $ex_type = $_POST['ex_type'];
    $id_chk = mysql_query("Select * from " . SUBJECTS . " Where sub_id='$subid'") or die(mysql_error());
    $count = mysql_num_rows($id_chk);
    if ($count == 1) {
        $data = mysql_fetch_assoc($id_chk);
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$topicid'") or die(mysql_error());
        $topicid_count = mysql_num_rows($topicid_chk);
        if ($topicid_count == 1) {
            $topic_data = mysql_fetch_assoc($topicid_chk);
            echo"<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'>" . strtoupper($data['sub_name']) . " / " . strtoupper($topic_data['topic_name']) . " / EXERCISE $ex_type</div></div>";
            $exam_mod_enable = "YES";
            require ext_file_include('exam_mod', 'res_2_sector');
        } else {
            echo"NO SUCH TOPIC";
        }
    } else {
        echo "NO SUCH SUBJECT";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "galacticos") {
    $subid = $_POST['subid'];
    include("rankDomain.php");
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "rankType") {
    $subid = $_POST['subid'];
    $type = $_POST['type'];
    include("rankType.php");
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "rank") {
    $subid = $_POST['subid'];
    $domain = $_POST['domain'];
    $type = $_POST['type'];
    include"rank.php";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "contact_us") {
    $uid = getUID();
    $err = '';
    if (!isset($_POST['msg']) || empty($_POST['msg'])) {
        $err = $err . '<li>Message cannot be empty</li>';
    }
    if (!isset($_POST['sub']) || empty($_POST['sub'])) {
        $err = $err . '<li>Subject cannot be empty</li>';
    }
    if ($err == '') {
        $u_info = getUserInfo($uid);
        $_POST['name'] = $u_info['UID_FirstName'] . ' ' . $u_info['UID_LastName'];
        $_POST['email'] = $u_info['UID_Email'];
        include"../inpro_contact.php";
    } else {
        return '<ul>' . $err . '</ul>';
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "completeKc") {
    setShadow();
    $subid = $_POST['subid'];
    $topicid = $_POST['topicid'];
    $_SESSION['topic_id'] = $topicid;
    $ex_type = $_POST['ex_type'];
    $id_chk = mysql_query("Select * from " . SUBJECTS . " Where sub_id='$subid'") or die(mysql_error());
    $count = mysql_num_rows($id_chk);
    if ($count == 1) {
        $data = mysql_fetch_assoc($id_chk);
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$topicid'") or die(mysql_error());
        $topicid_count = mysql_num_rows($topicid_chk);
        if ($topicid_count == 1) {
            //update the reading complete flag for key concept.
            $uid = $_SESSION['UID'];
            $readlog_q = mysql_query("Select * from " . READ_LOG . " Where UID='$uid'") or die(mysql_error());
            $readlog_data = mysql_fetch_assoc($readlog_q);
            $valarray = json_decode($readlog_data['kc']);
            if ($valarray == "" || empty($valarray)) {
                $valarray = array("$topicid");
            } else {
                $val_count = count($valarray);
                if (!in_array($topicid, $valarray))
                    $valarray[$val_count] = $topicid;
            }
            $enc_valarray = json_encode($valarray);
            mysql_query("Update " . READ_LOG . " SET kc='$enc_valarray' WHERE UID='$uid'") or die(mysql_error());
            echo"<script>setSub($subid,$topicid,$ex_type);</script>";
        }
        else {
            echo"NO SUCH TOPIC";
        }
    } else {
        echo "NO SUCH SUBJECT";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "KC") {
    $subid = $_POST['subid'];
    $topicid = $_POST['topicid'];
    $_SESSION['topic_id'] = $topicid;
    $id_chk = mysql_query("Select * from " . SUBJECTS . " Where sub_id='$subid'") or die(mysql_error());
    $count = mysql_num_rows($id_chk);
    if ($count == 1) {
        $data = mysql_fetch_assoc($id_chk);
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$topicid'") or die(mysql_error());
        $topicid_count = mysql_num_rows($topicid_chk);
        if ($topicid_count == 1) {
            $topic_data = mysql_fetch_assoc($topicid_chk);
            echo "<style type='text/css'>
 .sm_color1{background:transparent !important;color:#fc6f4b !important;}</style>";
            echo"<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title' style='font-size:15px'>" . strtoupper($data['sub_name']) . "<span style=''> / </span>" . strtoupper($topic_data['topic_name']) . " / KEY CONCEPTS</div></div>";
            echo"<div style='float:right;margin-right:1%;'><p>Done with the key-concepts ? <a href='#' class='btn btn-default' onclick='completeKc($subid,$topicid,1)'>PROCEED</a></p></div>";
            //echo "<embed src='app/includes/pdfs/$topicid.pdf' class='panel panel-warning' width='100%' height='500' alt='pdf'>";  
            $content = htmlspecialchars_decode(getKc($topicid), ENT_QUOTES);
            $modified_str = str_replace('tinymce', 'app/add/tinymce', $content);
            $modified_str = str_replace('../assets', 'assets', $modified_str);
            echo "<span class='panel panel-warning' width='100%' height='500'>" . $modified_str . "</span>";
            //print_r($_SERVER);
        } else {
            echo"NO SUCH TOPIC";
        }
    } else {
        echo "NO SUCH SUBJECT";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getTopicsForSub") {
    $subid = $_REQUEST['subid'];
    $select_str = "";
    $q = mysql_query("select * from " . TOPICS . " where sub_id='$subid'") or error_log(mysql_error());
    while ($d = mysql_fetch_assoc($q)) {
        $select_str.="<option value='$d[topic_id]'>$d[topic_name]</option>";
    }
    echo $select_str;
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "removeSC") {
    $date = $_REQUEST['date'];
    $tid = $_REQUEST['tid'];
    $uid = getUID();
    $q = mysql_query("select * from " . SCHEDULE_DATA . " where  UID=$uid ") or error_log(mysql_error());
    $d = mysql_fetch_assoc($q);
    $data = json_decode($d['data']);
    foreach ($data as $key => $val) {
        if (in_array($tid, $val) && in_array($date, $val)) {
            array_splice($data, $key, 1);
        }
    }
    $enc_data = json_encode($data);
    mysql_query("update " . SCHEDULE_DATA . " SET data='$enc_data' WHERE '$uid' ") or error_log(mysql_error());
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "setPlanner") {
    $_SESSION['SCHEDULE_FOR_MONTH'] = $_REQUEST['month_info'];
    echo"<script>getPage('SCHEDULER')</script>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "changeMonth") {
    if (!isset($_SESSION['SCHEDULE_FOR_MONTH'])) {
        $month_numeric = date("m"); //numeric
        $month = date("F"); //alpha
        $year = date("Y");
        $_SESSION['SCHEDULE_FOR_MONTH'] = $year . '-' . $month_numeric;
    }
    if ($_REQUEST['type'] == 'NEXT') {
        $cur_month = $_SESSION['SCHEDULE_FOR_MONTH']; //=$_REQUEST['month_info'];
        echo $cur_month;
        $exploded_str = explode('-', $cur_month);
        if ($exploded_str[1] >= 12) {
            $month = 1;
            $year = $exploded_str[0] + 1;
        } elseif ($exploded_str[1] < 1) {
            //attack vectors
            $month = 1;
            $year = $exploded_str[0];
        } else {
            $month = $exploded_str[1] + 1;
            $year = $exploded_str[0];
        }
        $_SESSION['SCHEDULE_FOR_MONTH'] = $year . '-' . $month;
    } else {
        $cur_month = $_SESSION['SCHEDULE_FOR_MONTH']; //=$_REQUEST['month_info'];
        $exploded_str = explode('-', $cur_month);
        if ($exploded_str[1] <= 1) {
            $month = 12;
            $year = $exploded_str[0] - 1;
        } elseif ($exploded_str[1] > 12) {
            //attack vectors
            $month = 12;
            $year = $exploded_str[0];
        } else {
            $month = $exploded_str[1] - 1;
            $year = $exploded_str[0];
        }
        $_SESSION['SCHEDULE_FOR_MONTH'] = $year . '-' . $month;
    }
    //echo"<script>getPage('SCHEDULER')</script>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "removeMarkAsReview") {
    if (isset($_SESSION['markAsReview'])) {
        $mr_arr = $_SESSION['markAsReview'];
    }
    $_SESSION['markAsReview'] = $mr_arr;
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "markAsReview") {
    if (isset($_SESSION['markAsReview'])) {
        $mr_arr = $_SESSION['markAsReview'];
    } else {
        $mr_arr = array();
    }
    $count = count($mr_arr);
    $mr_arr[$count] = $_REQUEST['srno'];
    $_SESSION['markAsReview'] = $mr_arr;
    print_r($_SESSION);
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getQ") {
    setShadow();
    //to continue exam we set up a label here
    CONTINUE_EXAM:
    //setting session variables
    //Unattempted - U
    if ($_POST['type'] == 'next') {
        if ($_POST['qid'] == 0) {
            $limit = $_SESSION['limit'] - 1;
            $_SESSION['ans_arr'][$limit] = $_POST['op'];
        } elseif ($_POST['qid'] < 0) {
            $_POST['qid'] == 0;
            $limit = $_SESSION['limit'] - 1;
            $_SESSION['ans_arr'][$limit] = $_POST['op'];
        } elseif ($_POST['qid'] > $_SESSION['limit'] - 1) {
            $_POST['qid'] == 0;
        } else {
            $setAnsFor = $_POST['qid'] - 1;
            $_SESSION['ans_arr'][$setAnsFor] = $_POST['op'];
        }
    } elseif ($_POST['type'] == 'previous') {
        if ($_POST['qid'] == 0) {
            $_SESSION['ans_arr'][1] = $_POST['op'];
        } elseif ($_POST['qid'] < 0 || $_POST['qid'] > $_SESSION['limit'] - 1) {
            $_POST['qid'] == 0;
            //   $limit=$_SESSION['limit']-1;
            //   $_SESSION['ans_arr'][$limit]=$_POST['op'];
        } else {
            $setAnsFor = $_POST['qid'] + 1;
            $_SESSION['ans_arr'][$setAnsFor] = $_POST['op'];
        }
    }
    //printing data
    echo "<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'>";
    if (isset($_SESSION['test_type'])) {
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topic_id]'") or die(mysql_error());
        $topic_data = mysql_fetch_assoc($topicid_chk);
        echo "DPP : " . strtoupper($topic_data['topic_name']) . "</div></div>";
        $exam_mod_enable = "YES";
        require ext_file_include('dpp_mod', 'res_2_sector');
    } else {
        $sub_q = mysql_query("Select * from " . SUBJECTS . " Where sub_id='$_SESSION[subid]'") or die(mysql_error());
        $data = mysql_fetch_assoc($sub_q);
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topicid]'") or die(mysql_error());
        $topic_data = mysql_fetch_assoc($topicid_chk);
        echo strtoupper($data['sub_name']) . " / " . strtoupper($topic_data['topic_name']) . " / EXERCISE $_SESSION[ex_type]</div></div>";
        $exam_mod_enable = "YES";
        require ext_file_include('exam_mod', 'res_2_sector');
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getTQ") {
    setShadow();
    //to continue exam we set up a label here
    CONTINUE_EXAM_TPP:
    //setting session variables
    //Unattempted - U
    if ($_POST['type'] == 'next') {
        if ($_POST['qid'] == 0) {
            $limit = $_SESSION['limit'] - 1;
            $_SESSION['ans_arr'][$limit] = $_POST['op'];
        } elseif ($_POST['qid'] < 0) {
            $_POST['qid'] == 0;
            $limit = $_SESSION['limit'] - 1;
            $_SESSION['ans_arr'][$limit] = $_POST['op'];
        } elseif ($_POST['qid'] > $_SESSION['limit'] - 1) {
            $_POST['qid'] == 0;
        } else {
            $setAnsFor = $_POST['qid'] - 1;
            $_SESSION['ans_arr'][$setAnsFor] = $_POST['op'];
        }
    } elseif ($_POST['type'] == 'previous') {
        if ($_POST['qid'] == 0) {
            $_SESSION['ans_arr'][1] = $_POST['op'];
        } elseif ($_POST['qid'] < 0 || $_POST['qid'] > $_SESSION['limit'] - 1) {
            $_POST['qid'] == 0;
            //   $limit=$_SESSION['limit']-1;
            //   $_SESSION['ans_arr'][$limit]=$_POST['op'];
        } else {
            $setAnsFor = $_POST['qid'] + 1;
            $_SESSION['ans_arr'][$setAnsFor] = $_POST['op'];
        }
    }
    //printing data
    echo "<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'>";
    $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topic_id]'") or die(mysql_error());
    $topic_data = mysql_fetch_assoc($topicid_chk);
    echo "TPP : " . strtoupper($topic_data['topic_name']) . "</div></div>";
    $exam_mod_enable = "YES";
    require ext_file_include('tpp_mod', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "directQ") {
    setShadow();
    $id = $_POST['ansForQid'];
    $_SESSION['ans_arr'][$id] = $_POST['op'];
    //printing data
    echo "<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'>";
    if (isset($_SESSION['test_type'])) {
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topic_id]'") or die(mysql_error());
        $topic_data = mysql_fetch_assoc($topicid_chk);
        echo "DPP : " . strtoupper($topic_data['topic_name']) . "</div></div>";
        $exam_mod_enable = "YES";
        require ext_file_include('dpp_mod', 'res_2_sector');
    } else {
        $sub_q = mysql_query("Select * from " . SUBJECTS . " Where sub_id='$_SESSION[subid]'") or die(mysql_error());
        $data = mysql_fetch_assoc($sub_q);
        $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topicid]'") or die(mysql_error());
        $topic_data = mysql_fetch_assoc($topicid_chk);
        echo strtoupper($data['sub_name']) . " / " . strtoupper($topic_data['topic_name']) . " / EXERCISE $_SESSION[ex_type]</div></div>";
        $exam_mod_enable = "YES";
        require ext_file_include('exam_mod', 'res_2_sector');
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "directTQ") {
    setShadow();
    $id = $_POST['ansForQid'];
    $_SESSION['ans_arr'][$id] = $_POST['op'];
    //printing data
    echo "<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'>";
    $topicid_chk = mysql_query("Select * from " . TOPICS . " Where topic_id='$_SESSION[topic_id]'") or die(mysql_error());
    $topic_data = mysql_fetch_assoc($topicid_chk);
    echo "TPP : " . strtoupper($topic_data['topic_name']) . "</div></div>";
    $exam_mod_enable = "YES";
    require ext_file_include('tpp_mod', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "AskEndTest") {
    setShadow();
    ?>
    <style>
        .panel_custom{border-color:#fc6f4b;position:absolute;top:-130;}
        .panel_custom > .panel-heading {background: #fc6f4b;color:#fff;text-align: center;}
        @media (max-width: 350px){
            .panel_custom{position:absolute;top:0;}}
        @media (max-width: 414px){.panel_custom{position:absolute;top:0;}}
        @media (max-width: 767px){.panel_custom{position:absolute;top:0;}}
        @media (max-width: 900px){.panel_custom{position:absolute;top:20;}}
        @media (max-width: 1200px) {.panel_custom{position:absolute;top:40;}}
    </style>
    <?php
    echo "<div class='panel panel-default panel_custom'><div class='panel-heading'><h3>Quesions Review</h3></div>";
    $sr = 1;
    $mark_as_review_arr = $_SESSION['markAsReview'];
    $mark_as_review_str = "<i>Marked For Review</i>";
    $ask_q_count = count($_SESSION['ans_arr']) / 2;
    echo"<table class='table'>";
    $p = $_POST['qid'];
    $v = $_POST['val'];
    $_SESSION['ans_arr'][$p] = $v;
    foreach ($_SESSION['ans_arr'] as $k => $val) {
        if ($sr > $ask_q_count)
            break;
        $next_col_q = $sr + $ask_q_count;
        $next_col_ans = $k + $ask_q_count;
        $next_col_val = $_SESSION['ans_arr'][$next_col_ans];
        $stat = "<font style='color:green;' >ATTEMPTED</font>";
        $stat_next = $stat;
        if ($val == 'U' || $val == '' || $val == null) {
            $stat = "<font style='color:red;' >SKIPPED</font>";
        }
        if ($next_col_val == 'U' || $next_col_val == '' || $next_col_val == null) {
            $stat_next = "<font style='color:red;' >SKIPPED</font>";
        }
        echo"<tr><td>Q $sr - $stat ";
        if (in_array($sr, $mark_as_review_arr)) {
            echo $mark_as_review_str;
        }
        echo"</td><td>Q $next_col_q - $stat_next ";
        if (in_array($next_col_q, $mark_as_review_arr)) {
            echo $mark_as_review_str;
        }
        echo"</td></tr>";
        $sr++;
    }
    echo"</table>";
    echo"<hr><center><ul class='list-group'><li class='' style='color:#333;list-style: none;width:100%;height:34px;border-bottom:1px #eee solid;padding-left:10px;padding-top:3px;'>Do you want to submit the answers ?</li>";
    $val_no = "NO";
    $val_yes = "YES";
    echo"<li class='' style='color:#333;list-style: none;width:100%;height:40px;border-bottom:1px #eee solid;padding-left:10px;padding-top:3px;'><input type='button'  class='btn btn-success' name='end' onclick=\"endTest('$val_yes')\" value='YES' /><input type='button' name='cancel' class='btn btn-danger' onclick=\"endTest('$val_no')\" value='NO' /></li></ul></center></div>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "fp") {
    //forgot pass
    $email = $_POST['email'];
    if ($email == "" || empty($email) || $email == null || !isset($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $msg = "Email has inappropriate value.";
    } else {
        $existing_check = mysql_query("select UID from " . USER_INFO . " WHERE UID_Email='$email'") or error_log(mysql_error());
        $ec_count = mysql_num_rows($existing_check);
        if ($ec_count == 0) {
            $msg = "No such email registered.";
        } else {
            $uid_d = mysql_fetch_Assoc($existing_check);
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ><,.%?/}[]{';
            $charactersLength = strlen($characters);
            $randomString = "";
            for ($i = 1; $i <= 60; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $enc_rs = base64_encode($randomString);
            $s_q = mysql_query("SELECT id FROM " . FORGOT_PASS . " WHERE email='$email'") or error_log(mysql_error());
            $s_row_count = mysql_num_rows($s_q);
            if ($s_row_count == 0) {
                $statement = "INSERT INTO " . FORGOT_PASS . "(UID,email,link) VALUES('$uid_d[UID]','$email','$enc_rs')";
            } else {
                $statement = "UPDATE " . FORGOT_PASS . " SET link='$enc_rs' WHERE UID='$uid_d[UID]'";
            }
            mysql_query("LOCK TABLES " . FORGOT_PASS . " write");
            mysql_query($statement) or error_log(mysql_error());
            mysql_query("UNLOCK TABLES");
            $confirm = "YES_I_WANT_TO_MAIL";
            include"../forgotpass_mailer.php";
            $to = $email;
            $message = "
To recover your account, click on the link below.<br>
<br>
<a href='http://www.roughsheet.com/app/recover_rs_account.php?rs_secure_token=$enc_rs&ma=$email'><button>Reset Password</button></a><br>
<br><br>
Regards,
RoughSheet
        ";
            smtpmailer("$to", 'recover@roughsheet.com', 'RoughSheet', "RoughSheet: Recover Account", "$message");
            $msg = "Link has been mailed to your registered email address";
        }
    }
    echo $msg;
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "endTest") {
    if ($_POST['response'] == 'YES') {
        jump_cause_timer:
        //CLEAN MARK AS REVIEW
        unset($_SESSION['markAsReview']);
        //END THE TEST
        $marks = 0;
        echo "<style>.active-home{background:#fff;border:none;}.active-friends{background:#fff;border:none;}.active-profile{background:#fff;border:none;}</style>";
        echo "<div class='panel panel-default panel_custom'><div class='panel-heading'><h3 class='mob_h3'>Exam Review</h3>
        <span style='float:right;width:130px;height:60px;box-shadow:0 0 5px #666;border:1px solid #333;border-radius:5px;padding:10px;margin-top:-60px;background:#fff;color:#333;' id='review'></span></div><ul class='list-group'>";
        //echo "<li class='list-group-item' > <span id='putMarks'>$marks</span> out of $_SESSION[limit]</li>";
        //echo "<li style='color:#333;list-style: none;width:100%;border-bottom:1px #eee solid;padding-left:10px;padding-top:3px;'> Graphycal analysis<br>";
        echo"<div id='graph'></div>";
        echo"</li></ul>";
        // echo"<li class='list-group-item'><ul>";
        //   echo"</ul></li>";
        $sr = 1;
        $tot_incorrect = 0;
        foreach ($_SESSION['q_arr'] as $key => $val) {
            $ans_q = mysql_query("select * from " . QUESTIONS . " where id='$val'") or die(mysql_error());
            $ans_data = mysql_fetch_assoc($ans_q);
            $q = htmlspecialchars_decode(str_replace("tinymce", "app/add/tinymce", $ans_data['Question']));
            echo"<table class='table table-bordered'><tr><th valign=top >$sr</th><th colspan=3 >$q</th></tr><tr>";
            $user_ans_op = $_SESSION['ans_arr'][$key];
            $ans_op = $ans_data['Answer'];
            if ($user_ans_op == $ans_op) {
                $marks++;
                echo"<td> <font style='color:green;' >Correct</font></td>";
            } else if ($user_ans_op == "U" || empty($user_ans_op) || $user_ans_op == null || $user_ans_op == "") {
                echo"<td> <font style='color:red;' >Skipped</font></td>";
            } else {
                $tot_incorrect++;
                echo"<td> <font style='color:red;' >Incorrect</font></td>";
            }
            echo"<td>Your Answer : " . htmlspecialchars_decode(str_replace("tinymce", "app/add/tinymce", $ans_data["Op$user_ans_op"])) . "</td><td>Actual Answer:<font style='color:green;' >" . htmlspecialchars_decode(str_replace("tinymce", "app/add/tinymce", $ans_data["Op$ans_op"])) . "</font></td></tr>";
            echo"<tr><td colspan=4 ><b>Solution : </b>" . htmlspecialchars_decode(str_replace("tinymce", "app/add/tinymce", $ans_data['Solution'])) . "</td></tr></table><hr><hr>";
            $sr++;
        }
        if (isset($_SESSION['test_type'])) {
            $t = $tot_incorrect + $marks;
            $u = $_SESSION['limit'] - $t;
            updateDppFlag($marks, $tot_incorrect, $u);
            echo"<script>$('#t_func').html('');</script>";
        } else {
            updateExFlag();
        }
        $c_total = $marks * 3;
        $review_marks = $c_total - $tot_incorrect;
        $attempted = $marks + $tot_incorrect;
        $acc = round($marks * 100 / $attempted, 2);
        $str = "<b style='font-size:12px;'>Marks:$review_marks</b></br><b style='font-size:12px;'>Accuracy:$acc%</b>";
        ?>
        <script>
            $('#review').html("<?php echo $str; ?>");
            $('#graph').html("<iframe width='100%' height='400px' style='border:none' src='http://<?php echo "$servername/app/includes/pielegend.php?cv=$marks&iv=$tot_incorrect"; ?>'/>");
        </script>
        <?php
        echo"<br><br>";
    } else {
        setShadow();
        //CONTINUE EXAM
        $_POST['type'] = 'previous';
        $_POST['qid'] = 0;
        goto CONTINUE_EXAM;
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "dppt") {
    $st = $_SESSION['s_time'];
    $et = $_SESSION['e_time'];
    $now = strtotime(date("H:i:s"));
    if ($et <= $now) {
        echo"<script>endTest('YES')</script>";
    } else {
        $rem = $et - $now;
        $c = $rem / 60;
        $r = $rem % 60;
        echo (int) $c;
        echo ":$r";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "dpp") {
    setShadow();
    $topic_id = htmlspecialchars($_POST['data'], ENT_QUOTES);
    $_SESSION['topic_id'] = $topic_id;
    $uid = getUID();
    $t_q = mysql_query("SELECT * FROM " . TOPICS . " WHERE topic_id='$topic_id'") or die(mysql_error());
    $t_rows = mysql_num_rows($t_q);
    if ($t_rows == 0) {
        echo"No Such Topic";
    } else {
        $td = mysql_fetch_assoc($t_q);
        $s_name = getSubName(getSubId($td['topic_id']));
        echo"<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'> DPP : " . strtoupper($s_name) . "</div></div>";
        require ext_file_include('dpp_mod', 'res_2_sector');
    }
}
//tpp (code by aj4)
elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "tpp") {
    setShadow();
    $topic_id = htmlspecialchars($_POST['data'], ENT_QUOTES);
    $_SESSION['topic_id'] = $topic_id;
    $uid = getUID();
    $t_q = mysql_query("SELECT * FROM " . TOPICS . " WHERE topic_id='$topic_id'") or die(mysql_error());
    $t_rows = mysql_num_rows($t_q);
    if ($t_rows == 0) {
        echo"No Such Topic";
    } else {
        $td = mysql_fetch_assoc($t_q);
        $s_name = getTopicName(getSubId($td['topic_id']));
        echo"<div class='panel panel-default panel_custom'>
        <div class='panel-heading'><div class='panel-title'> TPP : " . strtoupper($s_name) . "</div></div>";
        require ext_file_include('tpp_mod', 'res_2_sector');
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "unfriend") {
    $uid = getUID();
    $fid = $_REQUEST['fid'];
    $q = mysql_query("select F_UID from " . FRIENDS_TABLE . " WHERE UID='$uid'") or error_log(mysql_error());
    $d = mysql_fetch_assoc($q);
    $fuid_arr = json_decode($d['F_UID']);
    $pos = array_search($fid, $fuid_arr);
    unset($fuid_arr[$pos]);
    $new_fuid_arr = json_encode(array_values($fuid_arr));
    $f_q = mysql_query("select F_UID from " . FRIENDS_TABLE . " WHERE UID='$fid'") or error_log(mysql_error());
    $f_d = mysql_fetch_assoc($f_q);
    $f_uid_arr = json_decode($f_d['F_UID']);
    $f_pos = array_search($uid, $f_uid_arr);
    unset($f_uid_arr[$f_pos]);
    $new_f_uid_arr = json_encode(array_values($f_uid_arr));
    mysql_query("LOCK TABLES " . FRIENDS_TABLE . " write");
    mysql_query("UPDATE " . FRIENDS_TABLE . " SET F_UID='$new_fuid_arr' WHERE UID='$uid'") or error_log(mysql_error()); //own
    mysql_query("UPDATE " . FRIENDS_TABLE . " SET F_UID='$new_f_uid_arr' WHERE UID='$fid'") or error_log(mysql_error()); //friends
    mysql_query("UNLOCK TABLES");
    echo"<button class='btn btn-default' onclick='addfriend($fid)'>Add Friend</button>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "friends") {
    $uid = $_SESSION['UID'];
    require ext_file_include('friends', 'res_2_sector');
    if ($f_count == 0) {
        echo"No Friends !";
    } else {
        echo "<style>
    .f_pic{border-top:solid 2px #febf10;border-bottom:2px solid #37a8df;border-left:2px solid #fc6f4b;border-right:2px solid #25af60;border-radius:50px;background: #eee ;}</style>";
        echo "<div class='panel panel-primary'>
    <div class='panel-heading'><h3 class='panel-title'>
    <center>My Friends</center></h3></div>
  <div class='panel-body'>";
        //printing friends profile
        foreach ($menu_f_arr as $val) {
            $f_d_fetch = getUserInfo($val);
            echo"<script>getHomeSidemenu();</script>
            <div class='media col-md-6' style='padding:0px;box-shadow:0 0 15px #e7e7e7;border-radius:50px 0px 0px 50px;'>
            <div class='media-left'>";
            echo"<img class='media-object f_pic wow rotateIn' src='app/includes/spic.php?$f_d_fetch[UID_Pro_Pic]' width='84' height='84'>";
            echo "</div>";
            echo"<div class='media-body wow rollIn' style='padding:3px;'>
            <a href='#' onclick='f_profile($val)'><h4 class='media-heading'>$f_d_fetch[UID_FirstName] $f_d_fetch[UID_MiddleName] $f_d_fetch[UID_LastName]</h4></a>
            <div id='button_$f_d_fetch[UID]'>";
            echo"<button class='btn btn-default' onclick='f_profile($val)'>View Profile</button><a href=# onclick='unfriend($val)'><button class='btn btn-default'>Unfriend</button></a>";
            echo "</div>";
            echo"</div></div>";
        }
        echo "</div></div>";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "people") {
    $uid = $_SESSION['UID'];
    require ext_file_include('search', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "totalDays") {
    $year = htmlspecialchars($_REQUEST['year']);
    $month = htmlspecialchars($_REQUEST['month']);
    $d = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $op = "";
    for ($i = 1; $i <= $d; $i++) {
        $op.="<option value='$i'>$i</option>";
    }
    echo "$op";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "register") {
    $data = array();
    foreach ($_REQUEST as $key => $value) {
        $data[$key] = htmlspecialchars($value, ENT_QUOTES);
    }
    $err = "";
    $gender_arr = array("Male", "Female");
    $u_length = 6; //user password length
    $err_flag = 0;
    if ($data['fb_link'] == "" || empty($data['fb_link']) || $data['fb_link'] == null || !isset($data['fb_link'])) {
        $err = $err . "$('#err_fb_link').html('Facebook profile link has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_fb_link').html('');";
    }
    if ($data['f_name'] == "" || empty($data['f_name']) || $data['f_name'] == null || !isset($data['f_name'])) {
        $err = $err . "$('#err_f_name').html('First name has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_f_name').html('');";
    }
    if ($data['l_name'] == "" || empty($data['l_name']) || $data['l_name'] == null || !isset($data['l_name'])) {
        $err = $err . "$('#err_l_name').html('Last name has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_l_name').html('');";
    }
    if ($data['gender'] == "" || empty($data['gender']) || $data['gender'] == null || !isset($data['gender']) || !in_array($data['gender'], $gender_arr)) {
        $err = $err . "$('#err_gender').html('Gender has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_gender').html('');";
    }
    if (isset($data['i_code_present']) && $data['i_code_present'] == 'yes') {
        if (!isset($data['i_code']) || $data['i_code'] == "" || empty($data['i_code']) || $data['i_code'] == null) {
            $err = $err . "$('#err_i_code').html('Invitation code has inappropriate value');";
            $err_flag = 1;
            $first_page = 1;
        } else {
            $message_post_reg = "User Registration Successful.<br>Verify yourself by clicking on the link sent to your mail box.";
        }
    } else {
        $err = $err . "$('#err_i_code').html('');";
        $message_post_reg = "We have received your request to join us.<br> Your application will be reviewed and we will get back to you within 48 hours.";
    }
    if (strlen($data['dob_m']) == '1') {
        $data['dob_m'] = '0' . $data['dob_m'];
    }
    if (strlen($data['dob_d']) == '1') {
        $data['dob_d'] = '0' . $data['dob_d'];
    }
    $data['dob'] = $data['dob_m'] . "/" . $data['dob_d'] . "/" . $data['dob_y'];
    $dob_arr = explode("/", $data['dob']);
    $today = date('m/d/Y');
    $today_time = strtotime($today);
    $dob_time = strtotime($data['dob']);
    if ($data['dob'] == "" || empty($data['dob']) || $data['dob'] == null || !isset($data['dob']) || !checkdate($dob_arr[0], $dob_arr[1], $dob_arr[2]) || $dob_time > $today_time) {
        $err = $err . "$('#err_dob').html('Date of birth has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_dob').html('');";
    }
    if ($data['email'] == "" || empty($data['email']) || $data['email'] == null || !isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
        $err = $err . "$('#err_email').html('Email has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_email').html('');";
    }
    if ($data['u_pass'] == "" || empty($data['u_pass']) || $data['u_pass'] == null || !isset($data['u_pass'])) {
        $err = $err . "$('#err_u_pass').html('Password has inappropriate value');";
        $err_flag = 1;
        $upass_err = 1;
    }
    if ($data['u_pass_confirm'] == "" || empty($data['u_pass_confirm']) || $data['u_pass_confirm'] == null || !isset($data['u_pass_confirm'])) {
        $err = $err . "$('#err_u_pass_confirm').html('Confirm-Password has inappropriate value');";
        $err_flag = 1;
        $upass_c_err = 1;
    }
    if (strlen($data['u_pass']) < $u_length || strlen($data['u_pass_confirm']) < $u_length) {
        $err = $err . "$('#err_u_pass').html('Password length must be atleast $u_length');";
        $err_flag = 1;
        $upass_err = 1;
    }
    if (!isset($upass_err) || $upass_err != 1) {
        $err = $err . "$('#err_u_pass').html('');";
    }
    if ($data['u_pass'] != $data['u_pass_confirm']) {
        $err = $err . "$('#err_u_pass_confirm').html('Passwords do not match');";
        $err_flag = 1;
        $upass_c_err = 1;
    }
    if (!isset($upass_c_err) || $upass_c_err != 1) {
        $err = $err . "$('#err_u_pass_confirm').html('');";
    }
    if ($data['captcha'] == "" || empty($data['captcha']) || $data['captcha'] == null || !isset($data['captcha']) || $data['captcha'] != $_SESSION['captcha']) {
        $err = $err . "$('#err_captcha').html('Captcha has inappropriate value');";
        $err_flag = 1;
        $first_page = 1;
    } else {
        $err = $err . "$('#err_captcha').html('');";
    }
    if ($data['location'] == "" || empty($data['location']) || $data['location'] == null || !isset($data['location'])) {
        $err = $err . "$('#err_location').html('Location has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_location').html('');";
    }
    if ($data['currently'] == "" || empty($data['currently']) || $data['currently'] == null || !isset($data['currently'])) {
        $err = $err . "$('#err_currently').html('Currently has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_currently').html('');";
    }
    if ($data['institute_name'] == "" || empty($data['institute_name']) || $data['institute_name'] == null || !isset($data['institute_name'])) {
        $err = $err . "$('#err_inst_name').html('Institute Name has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_inst_name').html('');";
    }
    if ($data['branch_name'] == "" || empty($data['branch_name']) || $data['branch_name'] == null || !isset($data['branch_name'])) {
        $err = $err . "$('#err_branch_name').html('Branch Name has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_branch_name').html('');";
    }
    if ($data['gdu_year'] == "" || empty($data['gdu_year']) || $data['gdu_year'] == null || !isset($data['gdu_year'])) {
        $err = $err . "$('#err_gdu_year').html('Graduation year has inappropriate value');";
        $err_flag = 1;
    } else {
        $err = $err . "$('#err_gdu_year').html('');";
    }

    $chkuser_q = mysql_query("select * from " . USER_INFO . " where UID_Email='$data[email]'") or die(mysql_error());
    $chkuser_count = mysql_num_rows($chkuser_q);
    if ($chkuser_count != 0) {
        $err = $err . "$('#err_email').html('Email already registered');";
        $err_flag = 1;
    }

    if (strtolower($data['i_code_present']) == 'yes') {
        $ic_q = mysql_query("select * from " . USER_INFO . " where invitation_code='$data[i_code]'") or die(mysql_error());
        $ic_count = mysql_num_rows($ic_q);
        if ($ic_count == 0) {
            $group_q = mysql_query("select * from rs_invitation_code_storage where invitation_code='$data[i_code]'") or die(mysql_error());
            $group_count = mysql_num_rows($group_q);
            //end
            if ($group_count == 0) {
                $err = $err . "$('#err_i_code').html('Invitation code has inappropriate value');";
                $err_flag = 1;
                $first_page = 1;
            } else {
                $group_data = mysql_fetch_assoc($group_q);
                $ic_uid = '-' . $group_data['id'];
            }
        } else {
            $ic_d = mysql_fetch_assoc($ic_q);
            $ic_uid = $ic_d['UID'];
        }
    } else {
        $ic_uid = 0;
    }
    //filtering done
    if ($err_flag == 0) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = "";
        for ($i = 0; $i < 17; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        mysql_query("LOCK TABLES " . USER_INFO . " WRITE," . READ_LOG . " WRITE," . OTHER_INFO . " WRITE, " . FRIENDS_TABLE . " WRITE, " . NEW_USER . " WRITE, rs_dpp_log WRITE, " . INVITATION_REQUESTS . " WRITE") or die(mysql_error());
        $element_arr = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $element_arr_count = count($element_arr);
        $element_arr_count--;
        Generate_code:
        $invitation_code = 'EU';
        for ($i = 0; $i < 6; $i++) {
            $random_val = rand(0, $element_arr_count);
            $invitation_code.=$element_arr[$random_val];
        }
        $mysql_q = mysql_query("select UID from " . USER_INFO . " where invitation_code='$invitation_code'") or die(mysql_error());
        if (mysql_num_rows($mysql_q) != 0) {
            goto Generate_code;
        }
        $data['fb_id'] = isset($_SESSION['fb_id']) ? $_SESSION['fb_id'] : "";
        mysql_query("insert into " . USER_INFO . " (`UID_FirstName`, `UID_MiddleName`, `UID_LastName`, `UID_Gender`, `UID_CurrentCity`, `UID_Hometown`, `UID_DOB`, `UID_Contact`, `UID_Email`, `UID_CurrentLevel`, `UID_CurrentRoleOrFuncArea`, `UID_CurrentInstitutionOrCompany`, `invitation_code`,`ic_used_UID`,`fb_link`,`fb_id`)
                    VALUES('$data[f_name]','','$data[l_name]','$data[gender]','','','$data[dob]','','$data[email]','$data[currently]','','','$invitation_code','$ic_uid','$data[fb_link]','$data[fb_id]')") or die(mysql_error());
        $getuid_q = mysql_query("select * from " . USER_INFO . " where UID_Email='$data[email]'") or die(mysql_error());
        $uid_data = mysql_fetch_assoc($getuid_q);
//        ["THDC Institute of Hydropower Engineering &amp; Technology","Tehri,Uttarakhand","ECE","2015","70.80","","","","","","Doon Global School","Dehradun","PCM","2010","87.60","Rajiv Gandhi Navodaya Vidyalaya","Dehradun","English","2008","91.00"]
        $academics = json_encode(array($data['institute_name'], $data['location'], $data['branch_name'], $data['gdu_year'], "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
        mysql_query("insert into " . READ_LOG . " (`UID`, `kc`, `e1`, `e2`, `e3`) VALUES('$uid_data[UID]','','','','')") or die(mysql_error());
        mysql_query("insert into " . OTHER_INFO . " (`UID`, `prof_arr`, `academics`, `achievements`, `certificates`, `social_work`, `extra`, `sports`) VALUES ('$uid_data[UID]','','$academics','','','','','')") or die(mysql_error());
        mysql_query("insert into " . FRIENDS_TABLE . " (`UID`, `F_UID`, `FR_UID`, `SFR_UID`) VALUES('$uid_data[UID]','','','')") or die(mysql_error());
        if (strtolower($data['i_code_present']) == 'yes') {
            mysql_query("insert into " . NEW_USER . " (`UID`, `UID_link`, `UID_pass`) VALUES ('$uid_data[UID]','$randomString','$data[u_pass]')") or die(mysql_error());
        } else {
            mysql_query("insert into " . INVITATION_REQUESTS . " (`UID`, `UID_link`, `UID_pass`) VALUES ('$uid_data[UID]','$randomString','$data[u_pass]')") or die(mysql_error());
        }
        $dpp_log_q = mysql_query("INSERT INTO rs_dpp_log (UID,data) VALUES('$uid_data[UID]','')") or die(mysql_error());
        
        mysql_query("UNLOCK TABLES") or die(mysql_error());
        $confirm = "YES_I_WANT_TO_MAIL";
        include SITEURL . "app/smtpmailer_register.php";
        $to = $data['email'];
        if (strtolower($data['i_code_present']) == 'yes') {
            $enc_random = str_encrypt_n_decrypt($randomString, "enc");
            $message = "
                $mail_header
                Welcome to RoughSheet!<br>
                We are humbled by your trust in us. We hope that together we achieve your target.<br>
                <br>
                Hit reply to this email and tell us which company is your dream company where you wish to get placed and why. Let us know and we will further assist you.<br>
                Allow us 48 hours to get back to you.<br>
                <br>
                Now, let’s get started.<br>
                All the best!<br>
                <br>
                Regards,<br>
                <br>
                Ankur<br>
                On behalf of Team RoughSheet
                $mail_footer";
            smtpmailer("$to", 'hello@roughsheet.com', 'RoughSheet', "Welcome! Let's talk about you.", "$message");
            $vars['random'] = $enc_random;
            $type = 'verify';
            curlReq(SITEURL . "app/includes/mt_curl.php", $to, $type, $vars);
            echo"<script>document.getElementById('reg_info').innerHTML='<center><h4>User Registration Successful.<br>Verify yourself by clicking on the link sent to your mail box.</h4></center>';</script>";
        } else {
            $message = "
            $mail_header
            Greetings!<br>
            <br>    
            We have received your application to join RoughSheet, so thank you very much for showing interest in us.<br>
            You have provided us with all the necessary details and we will perform an eligibility check.<br><br>
            Depending on the review of your application, we will get back to you within 48 hours.<br>
            Cheers!<br>
            <br>
            All the best!<br>
            Regards,<br>
            <br>
            Gnana<br>
            On behalf of Team RoughSheet
            $mail_footer";
            smtpmailer("$to", 'donotreply@roughsheet.com', 'RoughSheet', "Request received", "$message");
            echo"<script>document.getElementById('reg_info').innerHTML='<center><h4>$message_post_reg</h4></center>';</script>";
        }
    } else {
        echo "<script>";
        if (isset($first_page)) {
            echo"prevStep();";
        }
        echo $err . "document.getElementById('reg_btn').disabled=false;";
        echo"</script>";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "checkPass") {
    $uid = getUID();
    $pass = encryptPass($_REQUEST['mypass']);
    if ($pass == "" || !isset($pass) || empty($pass) || $pass == NULL) {
        echo"0"; //empty
    } else {
        $chk_q = mysql_query("Select COUNT(*) AS FLAG From " . LOGIN_TABLE . " WHERE UID='$uid' and UID_Password='$pass'") or die(mysql_error());
        $chk_d = mysql_fetch_assoc($chk_q) or die(mysql_error());
        if ($chk_d['FLAG'] == 1) {
            echo"2"; //correct
        } else {
            echo"1"; //incorrect
        }
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "changePass") {
    $uid = getUID();
    $pass = encryptPass($_REQUEST['newpass']);
    $update_q = mysql_query("update " . LOGIN_TABLE . " SET UID_Password='$pass' WHERE UID='$uid'") or die(mysql_error());
    if ($update_q) {
        echo TRUE;
    } else {
        echo FALSE;
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "addfriend") {
    $uid = $_SESSION['UID'];
    $f_query = mysql_query("select * from " . FRIENDS_TABLE . " where UID='$uid'") or die(mysql_error());
    $f_data = mysql_fetch_assoc($f_query);
    $f_arr = json_decode($f_data['SFR_UID']);
    $f_arr_count = count($f_arr);
    $f_uid = $_POST['data'];
    $ef_arr = json_decode($f_data['F_UID']); //existing friends array
    $fr_arr = json_decode($f_data['FR_UID']); //Recieved requests array
    if ($uid == $f_uid) {
        //self requesting: so don't do anything.
    } elseif (in_array($f_uid, $f_arr)) {
        echo"Friend Request Already Sent !";
    } elseif (in_array($f_uid, $ef_arr)) {
        echo"Already Friends !";
    } elseif (in_array($f_uid, $fr_arr)) {
        echo"Confirm request of the user.";
    } else {
        if ($f_arr_count == 0) {
            $f_arr = array($f_uid);
        } else {
            $f_arr[$f_arr_count] = $f_uid;
        }
        $enc_f_arr = json_encode($f_arr);
        mysql_query("Update " . FRIENDS_TABLE . " SET SFR_UID='$enc_f_arr' WHERE UID='$uid' ") or error_log(mysql_error());
        mysql_query("LOCK TABLES " . FRIENDS_TABLE . " WRITE");
        $f_q = mysql_query("select FR_UID from " . FRIENDS_TABLE . " WHERE UID='$f_uid'") or error_log(mysql_error());
        $f_d = mysql_fetch_assoc($f_q);
        $fr_arr_d = json_decode($f_d['FR_UID']);
        $fr_count = count($fr_arr_d);
        $fr_arr_d[$fr_count] = $uid;
        $enc_fr_arr = json_encode($fr_arr_d);
        mysql_query("UPDATE " . FRIENDS_TABLE . " SET FR_UID='$enc_fr_arr' WHERE UID='$f_uid'") or error_log(mysql_error());
        mysql_query("UNLOCK TABLES");
        echo"<button disabled class='btn btn-default'>Request Sent</button>";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "confirmF") {
    $fid = $_REQUEST['fid'];
    $uid = getUID();
    $f_query = mysql_query("select * from " . FRIENDS_TABLE . " where UID='$uid'") or die(mysql_error());
    $f_data = mysql_fetch_assoc($f_query);
    $fr_arr = json_decode($f_data['FR_UID']);
    if (in_array($fid, $fr_arr)) {
        $f_arr = json_decode($f_data['F_UID']);
        $f_count = count($f_arr);
        $f_arr[$f_count] = $fid;
        $enc_f_arr = json_encode($f_arr);
        $f_key = array_search($fid, $fr_arr);
        array_splice($fr_arr, $f_key, 1);
        $enc_fr_arr = json_encode($fr_arr);
        mysql_query("UPDATE " . FRIENDS_TABLE . " SET F_UID='$enc_f_arr',FR_UID='$enc_fr_arr' WHERE UID='$uid'") or error_log(mysql_error());
        mysql_query("LOCK TABLES " . FRIENDS_TABLE . " WRITE");
        $f_q = mysql_query("SELECT F_UID,SFR_UID FROM " . FRIENDS_TABLE . " WHERE UID='$fid'") or error_log(mysql_error());
        $f_d = mysql_fetch_assoc($f_q);
        $f_d_arr = json_decode($f_d['SFR_UID']);
        echo $fid_key = array_search($fid, $f_d_arr);
        array_splice($f_d_arr, $fid_key, 1);
        $enc_f_d_arr = json_encode($f_d_arr);
        $f_uid_arr = json_decode($f_d['F_UID']);
        $f_uid_count = count($f_uid_arr);
        $f_uid_arr[$f_uid_count] = $uid;
        $enc_ef_arr = json_encode($f_uid_arr);
        mysql_query("UPDATE " . FRIENDS_TABLE . " SET F_UID='$enc_ef_arr',SFR_UID='$enc_f_d_arr' WHERE UID='$fid'") or error_log(mysql_error());
        mysql_query("UNLOCK TABLES");
        echo"<button class='btn btn-default' onclick='f_profile($fid)'>View Profile</button>";
    } else {
        echo"Warning: This person haven't added you as a friend.";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "rejectU") {
    $f_uid = $_REQUEST['uid'];
    $uid = getUID();
    $f_query = mysql_query("select FR_UID from " . FRIENDS_TABLE . " where UID='$uid'") or die(mysql_error());
    $f_data = mysql_fetch_assoc($f_query);
    $fr_arr = json_decode($f_data['FR_UID']);
    $f_uid_key = array_search($f_uid, $fr_arr);
    array_splice($fr_arr, $f_uid_key, 1);
    $enc_fr_arr = json_encode($fr_arr);
    mysql_query("UPDATE " . FRIENDS_TABLE . " SET FR_UID='$enc_fr_arr' WHERE UID='$uid'") or error_log(mysql_error());
    mysql_query("LOCK TABLES " . FRIENDS_TABLE . " WRITE");
    $q = mysql_query("select SFR_UID from " . FRIENDS_TABLE . " WHERE UID='$f_uid'") or error_log(mysql_error());
    $d = mysql_fetch_assoc($q);
    $sfr_data = json_decode($d['SFR_UID']);
    $uid_key = array_search($uid, $sfr_data);
    array_splice($sfr_data, $uid_key, 1);
    $enc_sfr_data = json_encode($sfr_data);
    mysql_query("UPDATE " . FRIENDS_TABLE . " SET SFR_UID='$enc_sfr_data' WHERE UID='$f_uid'") or error_log(mysql_error());
    mysql_query("UNLOCK TABLES");
    echo"<button class='btn btn-default' onclick='addfriend($f_uid)'>Add Friend</button>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "home_sidemenu") {
    //$uid=$_SESSION['UID'];
    require ext_file_include('home_sidemenu', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "profile_sidemenu") {
    //$uid=$_SESSION['UID'];
    require ext_file_include('profile_sidemenu', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "f_profile_sidemenu") {
    //$uid=$_SESSION['UID'];
    require ext_file_include('f_profile_sidemenu', 'res_2_sector');
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "profile_update") {
    $uid = $_SESSION['UID'];
    mysql_query("Update " . USER_INFO . " SET UID_Gender='$_POST[gender]',UID_CurrentCity='$_POST[current_c]',UID_Hometown='$_POST[home_town]',UID_DOB='$_POST[birth_val]',UID_Contact='$_POST[contact]',UID_CurrentInstitutionOrCompany='$_POST[in_c]' WHERE UID='$uid' ") or die(mysql_error());
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "changePerfMonth") {
    print_r($_SESSION);
    $subid = $_REQUEST['subid'];
    $type = $_REQUEST['type'];
    $month_arr = explode("-", $_SESSION['perfMonth']);
    $month = $month_arr[0];
    $year = $month_arr[1];
    if ($type == 'NEXT') {
        if ($month < 1) {
            $month = 1;
        } elseif ($month == 12) {
            $month = 1;
            $year++;
        } else {
            $month++;
        }
    } else {
        if ($month > 12) {
            $month = 12;
        } elseif ($month == 1) {
            $month = 12;
            $year--;
        } else {
            $month--;
        }
    }
    $_SESSION['perfMonth'] = $month . '-' . $year;
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "delAca") {
    $uid = getUID();
    $key = $_REQUEST['key'];
    $get_new_data_q = mysql_query("SELECT academics FROM " . OTHER_INFO . " WHERE UID='$uid'") or die(mysql_error());
    $new_data = mysql_fetch_assoc($get_new_data_q);
    $stored_arr1 = json_decode($new_data['academics']);
    if ($key == 'degree') {
        //0-4
        $stored_arr1[0] = "";
        $stored_arr1[1] = "";
        $stored_arr1[2] = "";
        $stored_arr1[3] = "";
        $stored_arr1[4] = "";
    } else if ($key == 'diploma') {
        $stored_arr1[5] = "";
        $stored_arr1[6] = "";
        $stored_arr1[7] = "";
        $stored_arr1[8] = "";
        $stored_arr1[9] = "";
    } else if ($key == '12') {
        $stored_arr1[10] = "";
        $stored_arr1[11] = "";
        $stored_arr1[12] = "";
        $stored_arr1[13] = "";
        $stored_arr1[14] = "";
    } else {
        $stored_arr1[15] = "";
        $stored_arr1[16] = "";
        $stored_arr1[17] = "";
        $stored_arr1[18] = "";
        $stored_arr1[19] = "";
    }
    $enc_arr = json_encode($stored_arr1);
    mysql_query("update " . OTHER_INFO . " set `academics`='$enc_arr' where UID='$uid'") or die(mysql_error());
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "delprof") {
    $uid = getUID();
    $key = $_REQUEST['key'];
    $get_new_data_q = mysql_query("SELECT prof_arr FROM " . OTHER_INFO . " WHERE UID='$uid'") or die(mysql_error());
    $new_data = mysql_fetch_assoc($get_new_data_q);
    $stored_arr = json_decode($new_data['prof_arr']);
    unset($stored_arr[$key]);
    $new_stored_arr = json_encode(array_values($stored_arr));
    mysql_query("update " . OTHER_INFO . " set `prof_arr`='$new_stored_arr' where UID='$uid'") or die(mysql_error());
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getGraph") {
    $uid = getUID();
    $_SESSION['subid_graph'] = $_REQUEST['subid'];
    echo"<iframe src='app/includes/graph.php' width='100%' height='60%' style='border:none;' /><br><br><br><br><br>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getRankGraph") {
    $uid = getUID();
    $_SESSION['subid_graph'] = $_REQUEST['subid'];
    echo"<iframe src='app/includes/rankGraph.php' width='100%' height='60%' style='border:none;' /><br><br><br><br><br>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getRankGraphPC") {
    $uid = getUID();
    $_SESSION['subid_graph'] = $_REQUEST['subid'];
    echo"<iframe src='app/includes/rankGraphC.php' width='100%' height='60%' style='border:none;' /><br><br><br><br><br>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getRankGraphW") {
    $uid = getUID();
    $_SESSION['subid_graph'] = $_REQUEST['subid'];
    echo"<iframe src='app/includes/weekly_rank_graph.php' width='100%' height='60%' style='border:none;' /><br><br><br><br><br>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "getRankGraphC") {
    $uid = getUID();
    $_SESSION['subid_graph'] = $_REQUEST['subid'];
    echo"<iframe src='app/includes/cumulative_rank_graph.php' width='100%' height='60%' style='border:none;' /><br><br><br><br><br>";
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "refreshBadge") {
    $uid = getUID();
    $q = mysql_query("select FR_UID from " . FRIENDS_TABLE . " WHERE UID='$uid'") or error_log(mysql_error());
    $d = mysql_fetch_assoc($q);
    $fr_arr = json_decode($d['FR_UID']);
    $fr_count = count($fr_arr);
    if ($fr_count == 0) {
        echo"";
    } else {
        echo $fr_count;
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "schedule") {
    $uid = getUID();
    $tid = $_POST['tid'];
    $date_act = $_REQUEST['date'];
    $date = strtotime($date_act);
    $now = time();
    if ($date >= $now) {
        $s = mysql_query("select * from " . SCHEDULE_DATA . " WHERE UID='$uid'") or error_log(mysql_error());
        $s_count = mysql_num_rows($s);
        $cur_data = array("$tid", "$date");
        if ($s_count == 0) {
            $array[0] = $cur_data;
            $enc_data = json_encode($array);
            mysql_query("LOCK TABLES " . SCHEDULE_DATA . " write");
            mysql_query("insert into " . SCHEDULE_DATA . " (UID,data) VALUES('$uid','$enc_data')") or error_log(mysql_error());
            mysql_query("UNLOCK TABLES");
        } else {
            $d = mysql_fetch_assoc($s);
            $array = json_decode($d['data']);
            $d_arr_count = count($array);
            $array[$d_arr_count] = $cur_data;
            $enc_data = json_encode($array);
            mysql_query("UPDATE " . SCHEDULE_DATA . " SET data='$enc_data' WHERE UID='$uid'") or error_log(mysql_error());
        }
    } else {
        echo"You can not schedule on past dates.";
    }
} elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] == "fblogin") {
    $s = mysql_query("select fb_id from " . USER_INFO . " WHERE fb_id='$_POST[id]'") or error_log(mysql_error());
    $s_count = mysql_num_rows($s);
    if ($s_count > 0) {
        $query = mysql_query("SELECT * FROM " . LOGIN_TABLE . " WHERE `fb_id`='$_POST[id]'") or die(mysql_error());
        $count = mysql_num_rows($query);
        if ($count == 1) {
            $data = mysql_fetch_assoc($query) or die("Couldn't Get Data.");
            $uid = $data['UID'];
            $_SESSION['UID'] = $uid;
            $plid = $data['UID_PRIVILEGE_TYPE'];
            $_SESSION['UID_PRIVILEGE_TYPE'] = $plid;
            $_SESSION['UID_login_flag'] = $data['UID_login_flag'];
        } else {
            $error = "Invalid Email or Password";
            $error_type = "ERROR";
            goto throw_error;
        }
        mysql_close();
        $ConnArray = getMySqlConnectionValues($priv_array[$plid]);
        $user = $ConnArray["USER"];
        $pass = $ConnArray["PASS"];
        require ext_file_include('db_connectivity', 'config');
        mysql_query("UPDATE " . LOGIN_TABLE . " SET UID_login_flag=UID_login_flag+1 WHERE `UID`='$uid'") or error_log(mysql_error());
        $info_q = mysql_query("SELECT * FROM " . USER_INFO . " WHERE `UID`='$uid'") or die("Query Problem.");
        $info = mysql_fetch_assoc($info_q);
        $_SESSION['invitation_code'] = $info['invitation_code'];
        //die();
        //set cookie for the user
        $cookie_value = str_encrypt_n_decrypt(time() . "_RS_SCI_$uid" . "_$plid" . "_$info[UID_FirstName]" . "_$info[UID_Pro_Pic]" . "_" . $data['UID_login_flag'] . "_" . time(), "enc");
        if (isset($_REQUEST["remember"])) {
            //this is the cookie set to expire after ten years...(for `REMEMBER ME` option)
            //setcookie("ROUGHSHEET_SCI",$cookie_value, time() + 315360000 );
            $_SESSION["ROUGHSHEET_SCI"] = $cookie_value;
        } else {
            //this is the cookie that will expire after browser is closed.
//            setcookie("ROUGHSHEET_SCI",$cookie_value);
            $_SESSION["ROUGHSHEET_SCI"] = $cookie_value;
        }

        echo json_encode(array('status' => 1));
    } else {
        throw_error:
        $_SESSION['fb_id'] = $_POST['id'];
        echo json_encode(array('status' => 2));
    }
} else {
    echo $err = "Something Went Wrong";
}
?>
