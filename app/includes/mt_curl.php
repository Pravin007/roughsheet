<?php

if (!isset($_GET['send_another_mail_with_the_script']) && $_GET['send_another_mail_with_the_script'] == 'sendanothermailwiththecurlscriptthroughserverrequest') {
    die('Access Denied');
}
include"header.php";
$servername = $_SERVER['SERVER_NAME'];
if ($servername == 'localhost') {
    $servername = 'localhost/roughsheet';
}
$type = $_GET['type'];
$to = $_GET['to'];
$confirm = "YES_I_WANT_TO_MAIL";
include SITEURL."app/smtpmailer_register2.php";
if ($type == 'verify') {
    $enc_random = urlencode($_GET['random']);
    $message = "
$mail_header
Greetings!<br>
<br>
We are very happy to see you join us.<br>
Click on the link below to verify your email address:<br>
<a href='" . SITEURL . "app/registerme.php?ud=$enc_random&em=$to'>Click Here! »</a><br>
<br>
Regards,<br>
<br>
Gnana<br>
On behalf of Team RoughSheet
$mail_footer
    ";
    $subject = "Welcome to RoughSheet";
    $from = "donotreply@roughsheet.com";
} elseif ($type == 'ic') {
    $ic = $_GET['ic'];
    $message = "
         $mail_header
Greetings!<br>
<br>
We hope you're doing well. As already announced, RoughSheet is now an invite-only platform and hence we are providing you with your unique Invitation Code.<br>
<br>
Share your Invitation Code with those who you think will benefit from RoughSheet.<br>
<p>Invitation Code: $ic</p>
<br>
All the best!<br>
Regards,<br>
<br>
Ankur<br>
On behalf of Team RoughSheet
$mail_footer
        ";
    $subject = "Your Invitation Code";
    $from = "donotreply@roughsheet.com";
} else if ($type == 'update') {
    $to = json_decode($_GET['to']);
    $message = "
        $mail_header
Greetings!<br>
<br>
We hope you're doing well. It has been some time since you have joined us.<br>
Hit reply to this email and tell us which company is your dream company where you wish to get placed and why. Let us know and we will further assist you.<br>
<br>
Allow us 48 hours to get back to you. Together, we will achieve your target.<br>
<br>
An important announcement from our end:<br>
You very well know that RoughSheet is a free product which helps users to hone their aptitude skills. However, from now on, RoughSheet will no longer be an open platform. We are moving to an invitation-based system.<br>
<br>
Existing users, like you, will not be affected by this. The future users, however, cannot sign up unless they are invited by our existing users.<br>
We will provide you with your invitation code, shortly.<br>
So, if you feel that RoughSheet is useful and can help someone, do share with them your invitation code.<br>
<br>
Hope to see you again!<br>
All the best!<br>
Regards,<br>
<br>
Ankur<br>
On behalf of Team RoughSheet
$mail_footer
        ";
    $subject = "An interesting update.";
    $from = "hello@roughsheet.com";
} else if ($type == "welcome_acc") {
    $to = $_GET['to'];
    $message = "
$mail_header
Welcome to RoughSheet!<br>
We are humbled by your trust in us. We hope that together we achieve your target.<br>
<br>
Hit reply to this email and tell us which company is your dream company where you wish to get placed and why. Let us know and we will further assist you.<br>
Allow us 48 hours to get back to you.<br>
<br>
Now, let’s get started.<br>
All the best!<br>
<br>
Regards,<br>
<br>
Ankur<br>
On behalf of Team RoughSheet
$mail_footer
        ";
    $subject = "Welcome! Let's talk about you.";
    $from = "hello@roughsheet.com";
}
smtpmailer1($to, "$from", "RoughSheet", "$subject", "$message");
?>
