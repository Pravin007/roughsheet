<?php

//THIS SCRIPT BLOCKS DIRECT ACCESS TO THE PAGES. AND IT ALSO BLOCKS THE PAGE INCLUDE.
//do modify the server names before file upload.
include"domain_arr.php";
$server_name = $domain_arr;
$script_name = $_SERVER['PHP_SELF'];
$script_vars = explode("/", $script_name);
$var_count = count($script_vars);
if (in_array("$_SERVER[SERVER_NAME]", $server_name)) {
    $var = $script_vars[$var_count - 1];
    $exception = array('mic_gen.php', 'ic_tracker.php', 'res_include.php', 'invitation_code_generator.php', 'index.php', 'feeds_handler.php', 'admin_shell.php', 'mt_curl.php', 'existing_notify.php', 'set.php', 'recover_rs_account.php', 'profile_handler.php', 'pielegend.php', 'graph.php', 'cumulative_rank_graph.php', 'weekly_rank_graph.php', 'rankGraphC.php', 'rankGraph.php', 'registerme.php', 'sample1.php', 'crop.php', 'crop_handler.php', 'ranking_algo.php', 'ranking_cumulative.php', 'join_requests.php');
    if (in_array($var, $exception)) {
        //user is on the proper channel.
        // EXTERNAL PATHS DEFINED HERE
        $include_def = 'YES';
        include"def_paths.php";
        // FEW VARIABLES HAVE BEEN DEFINED FOR FASTER ACCESS AND TO REDUCE TIME COMPLEXITY.
    } else {
        goto block_direct_access;
    }
} else {
    block_direct_access:
    die("WARNING : DIRECT PAGE ACCESS IS BLOCKED.");
}
?>
