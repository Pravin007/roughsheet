<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<html> 
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
    <title></title>
    <?php
    if($_SERVER['SERVER_NAME']=='localhost'){
    	$url="http://localhost/roughsheet";
    }
    else{
	$url=$_SERVER['SERVER_NAME'];
    }
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $url; ?>/assets/css/style1.css">
    </head> 
<body> 
    <?php session_start();
    require "header.php";
    ?>
    <div id="newsWidget"> 
        <div class="intro">Below you find some <strong>random</strong> rss feeds from few sources using extended format. Every <strong>15 seconds</strong> the list is updated.</div>
        <ul id="news"></ul>
    </div>
  <?php
   function getLinks(){
   //here $catid => navigation id i.e. n_id
    $call_db="GRANT_ACCESS_TO_DB";
    $ConnArray=getMySqlConnectionValues("user");
    $host=$ConnArray["HOST"];
    $user=$ConnArray["USER"];
    $pass=$ConnArray["PASS"];
    $db=$ConnArray["DB"];
    $catid=htmlspecialchars($_REQUEST['n_id'],ENT_QUOTES);
     require ext_file_include('db_connectivity','res_2_config');
    $links_q=mysql_query("select * from " . RSS_SOURCE_LINKS . " WHERE n_id='$catid'") or die(mysql_error());
    $str='';
    $counter=1;
    while($links_d=mysql_fetch_assoc($links_q)){
            if($counter==1){
                $str="'$links_d[link]'";
            }
            else{
                $str.=", '$links_d[link]'";
            }
            $counter++;
    }
    return $str;
   }
?>
    <script type="text/javascript" src="<?php echo $url; ?>/assets/js/jquery.js"></script> 
    <script type="text/javascript" src="<?php echo $url; ?>/assets/js/jquery.newsWidget.js"></script> 
    <script type="text/javascript">
        $('ul#news').newswidget({ source: [<?php echo getLinks(); ?>],
            proxyUrl: "<?php echo $url; ?>/app/includes/get_rss_feed.php",
            limitItems: 10,
            refresh: 15000,
            random: true,
            itemDateElement: "span",
            itemLinkElement: "span",
            itemWebsiteTitleElement: 'span',
            linkTitle: true,
            format: "title,description,date,link,websiteTitle"});
    </script> 
</body> 
</html>
