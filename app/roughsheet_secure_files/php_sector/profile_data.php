<style>
    .badge{background:#dddddd;color:#000000;}
    .c_badge{background:#eeeeee;margin-right:-14;margin-top:-22px;float:right;font-size:14px;color: #494949;box-shadow: 0 0 10px #eee;}
    .company_box{background: #f9f9f9;box-shadow: 0 0 10px #d8d8d8;margin-left:40px;margin-top: 50px;border-bottom:solid 3px #37a8df;}
    .heading{font-size:16px;color:#fff;border-bottom:1px solid #eee;}
    .main{display: none;}
    .main_feed{margin-top:50px;}
    .active-profile{background:#b9e1f4;border-top:2px solid #37a8df;color:#fff !important;}
    .active-friends{background:#fff;border:none;}
</style>
<?php
if (isset($_SERVER['QUERY_STRING'])) {
    $complete_arr = $_REQUEST;
    $request_arr = $complete_arr['data'];
    require "header.php";
    $call_db = "GRANT_ACCESS_TO_DB";
    $ConnArray = getMySqlConnectionValues("user");
    $host = $ConnArray["HOST"];
    $user = $ConnArray["USER"];
    $pass = $ConnArray["PASS"];
    $db = $ConnArray["DB"];
    require ext_file_include('db_connectivity', 'res_2_config');
    include"profile_func.php";
    if (isset($_REQUEST['fid'])) {
        $func = "FID";
    } elseif (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != "" || !empty($_SERVER['QUERY_STRING'])) {
        $func = $_SERVER['QUERY_STRING'];
    } elseif (isset($_REQUEST['pro_pic'])) {
        $func = "PROPIC";
    } else {
        $func = "";
    }
    if ($func == "SAVE") {
        $page = $complete_arr['page'];
        $uid = getUID();
        if ($page == "professional_area") {
            $err = "";
            //start year
            $start_year = htmlspecialchars((int) $request_arr[0]['value'], ENT_QUOTES);
            if ($start_year < "0" || $start_year == "" || $start_year == null || !isset($start_year)) {
                $err = "Start year has inappropriate value.<br>";
            }
            //start month
            $start_month = htmlspecialchars((int) $request_arr[1]['value'], ENT_QUOTES);
            if ($start_month < "0" || $start_month == "" || $start_month == null || !isset($start_month)) {
                $err = $err . "Start Month has inappropriate value.<br>";
            }
            //to year
            $to_year = htmlspecialchars((int) $request_arr[2]['value'], ENT_QUOTES);
            if ($to_year < "0" || $to_year == "" || $to_year == null || !isset($to_year)) {
                $err = $err . "End Year has inappropriate value.<br>";
            }
            //to month
            $to_month = htmlspecialchars((int) $request_arr[3]['value'], ENT_QUOTES);
            if ($to_month < "0" || $to_month == "" || $to_month == null || !isset($to_month)) {
                $err = $err . "End Month has inappropriate value.<br>";
            }
            //job type
            $jtype = htmlspecialchars($request_arr[4]['value'], ENT_QUOTES);
            if ($jtype < "0" || $jtype == "" || $jtype == null || !isset($jtype)) {
                $err = $err . "Job type has inappropriate value.<br>";
            }
            //company name
            $cname = htmlspecialchars($request_arr[5]['value'], ENT_QUOTES);
            if ($cname == "" || $cname == null || !isset($cname)) {
                $err = $err . "Company name has inappropriate value.<br>";
            }
            //company location
            $cloc = htmlspecialchars($request_arr[6]['value'], ENT_QUOTES);
            if ($cloc == "" || $cloc == null || !isset($cloc)) {
                $err = $err . "Company location has inappropriate value.<br>";
            }
            //designation
            $desig = htmlspecialchars($request_arr[7]['value'], ENT_QUOTES);
            if ($desig == "" || $desig == null || !isset($desig)) {
                $err = $err . "Designation has inappropriate value.<br>";
            }
            //functional area
            $func_area = htmlspecialchars($request_arr[8]['value'], ENT_QUOTES);
            //print_r($request_arr);
            if ($func_area == "" || $func_area == null || !isset($func_area)) {
                $err = $err . "Functional area has inappropriate value.<br>";
            }
            if ($err == "") {
                $select_prof_q = mysql_query("SELECT prof_arr FROM " . OTHER_INFO . " WHERE UID='$uid'") or die(mysql_error());
                $prof_data = mysql_fetch_assoc($select_prof_q);
                $stored_arr = json_decode($prof_data['prof_arr']);
                if (is_array($stored_arr)) {
                    $arr_count = count($stored_arr);
                } else {
                    $arr_count = 0;
                }
                if ($arr_count >= 5) {
                    //display_prof($uid);
                    //count reached the limit.
                } else {
                    $prof_arr = array("start_year" => $start_year, "start_month" => $start_month, "to_year" => $to_year, "to_month" => $to_month, "jtype" => "$jtype", "cname" => "$cname", "cloc" => "$cloc", "desig" => "$desig", "func_area" => "$func_area");
                    $stored_arr[$arr_count] = $prof_arr;
                    $enc_stored_arr = json_encode($stored_arr);
                    mysql_query("Update " . OTHER_INFO . " SET prof_arr='$enc_stored_arr' where UID='$uid'") or die(mysql_error());
                    display_prof($uid);
                    echo"<input type='hidden' name='page' value='professional_area'><div id='professional_area_1'></div><script>toggleButtons('$page');</script>";
                }
            } else {
                echo " <div class='alert alert-warning'>" . $err . "</div>";
            }
        }
    } else if ($func == "UPDATE") {
        $page = $complete_arr['page'];
        $uid = getUID();
        if ($page == "professional_area") {
            $err = "";
            $data_arr = explode('__', $complete_arr['data']);
            //start year
            $start_year = htmlspecialchars((int) $data_arr[1], ENT_QUOTES);
            if ($start_year < "0" || $start_year == "" || $start_year == null || !isset($start_year)) {
                $err = "Start year has inappropriate value.<br>";
            }
            //start month
            $start_month = htmlspecialchars((int) $data_arr[2], ENT_QUOTES);
            if ($start_month < "0" || $start_month == "" || $start_month == null || !isset($start_month)) {
                $err = $err . "Start Month has inappropriate value.<br>";
            }
            //to year
            $to_year = htmlspecialchars((int) $data_arr[3], ENT_QUOTES);
            if ($to_year < "0" || $to_year == "" || $to_year == null || !isset($to_year)) {
                $err = $err . "End Year has inappropriate value.<br>";
            }
            //to month
            $to_month = htmlspecialchars((int) $data_arr[4], ENT_QUOTES);
            if ($to_month < "0" || $to_month == "" || $to_month == null || !isset($to_month)) {
                $err = $err . "End Month has inappropriate value.<br>";
            }
            //job type
            $jtype = htmlspecialchars($data_arr[5], ENT_QUOTES);
            if ($jtype < "0" || $jtype == "" || $jtype == null || !isset($jtype)) {
                $err = $err . "Job type has inappropriate value.<br>";
            }
            //company name
            $cname = htmlspecialchars($data_arr[6], ENT_QUOTES);
            if ($cname == "" || $cname == null || !isset($cname)) {
                $err = $err . "Company name has inappropriate value.<br>";
            }
            //company location
            $cloc = htmlspecialchars($data_arr[7], ENT_QUOTES);
            if ($cloc == "" || $cloc == null || !isset($cloc)) {
                $err = $err . "Company location has inappropriate value.<br>";
            }
            //designation
            $desig = htmlspecialchars($data_arr[8], ENT_QUOTES);
            if ($desig == "" || $desig == null || !isset($desig)) {
                $err = $err . "Designation has inappropriate value.<br>";
            }
            //functional area
            $func_area = htmlspecialchars($data_arr[9], ENT_QUOTES);
            //print_r($request_arr);
            if ($func_area == "" || $func_area == null || !isset($func_area)) {
                $err = $err . "Functional area has inappropriate value.<br>";
            }
            if ($err == "") {
                $select_prof_q = mysql_query("SELECT prof_arr FROM " . OTHER_INFO . " WHERE UID='$uid'") or die(mysql_error());
                $prof_data = mysql_fetch_assoc($select_prof_q);
                $stored_arr = json_decode($prof_data['prof_arr']);
                if (is_array($stored_arr)) {
                    $arr_count = count($stored_arr);
                } else {
                    $arr_count = 0;
                }
                $prof_arr = array("start_year" => $start_year, "start_month" => $start_month, "to_year" => $to_year, "to_month" => $to_month, "jtype" => "$jtype", "cname" => "$cname", "cloc" => "$cloc", "desig" => "$desig", "func_area" => "$func_area");
                foreach ($stored_arr as $key => $row) {
                    $syear[$key] = $row->start_year;
                    $smonth[$key] = $row->start_month;
                }
                array_multisort($syear, SORT_ASC, $smonth, SORT_ASC, $stored_arr);
                $position = $data_arr[0];
                $stored_arr[$position] = $prof_arr;
                $enc_stored_arr = json_encode($stored_arr);
                mysql_query("Update " . OTHER_INFO . " SET prof_arr='$enc_stored_arr' where UID='$uid'") or die(mysql_error());
                display_prof($uid);
                echo"<input type='hidden' name='page' value='professional_area'><div id='professional_area_1'></div><script>toggleButtons('$page');</script>";
            } else {
                echo " <div class='alert alert-warning'>" . $err . "</div>";
            }
        }
    } else if ($func == "PROPIC") {
        if (0 < $_FILES['pro_pic_file']['error']) {
            echo 'Error: ' . $_FILES['pro_pic_file']['error'] . '<br>';
        } else {
            $uid = $_SESSION['UID'];
            $valid_arr = array("jpg", "jpeg", "png", "PNG");
            $filename = $_FILES['pro_pic_file']['name'];
            $file_arr = explode(".", $filename);
            $file_arr_count = count($file_arr);
            $file_arr_count--;
            if (in_array(strtolower($file_arr[$file_arr_count]), $valid_arr)) {
                $new_name = "dpic$uid" . "." . $file_arr[$file_arr_count];
                $img_path = realpath(RES_2_IMG);
                //echo $img_path . '\\'. $new_name;
                move_uploaded_file($_FILES['pro_pic_file']['tmp_name'], $img_path . '\\' . $new_name);
                $pic_q = mysql_query("UPDATE " . USER_INFO . " SET `UID_Pro_Pic`='$new_name' WHERE UID='$uid'") or error_log(mysql_error());
                $_SESSION['img_to_crop'] = $new_name;
                header("Location: " . CROPPER);
            } else {
                echo "Invalid image format.";
            }
        }
    } elseif ($func == "FID") {
        if ($fid == "own") {
            $fid = getUID();
            echo"<a onclick=\"getPage('PROFILE')\" href='#' class='label label-info glass' style='float:right;font-size:14px; '>Edit Profile</a>";
        }
        echo"<script>
    $(document).ready(function(){
         $.ajax({
        		type: 'POST',
			url: 'app/includes/set.php?f_profile_sidemenu',
                        data: 'fid=$fid',
                        success: function(result){
                            $('#hidden').html(result);
                        }
    });
    });
    </script>";
        $user_info_q = mysql_query("select * from " . USER_INFO . " WHERE UID='$fid'") or error_log(mysql_error());
        $user_info = mysql_fetch_assoc($user_info_q);
        $name = $user_info['UID_FirstName'] . " " . $user_info['UID_LastName'];
        $gender = $user_info['UID_Gender'];
        $currCity = $user_info['UID_CurrentCity'];
        $hometown = $user_info['UID_Hometown'];
        $dob = $user_info['UID_DOB'];
        $email = $user_info['UID_Email'];
        $propic = $user_info['UID_Pro_Pic'];
        $currInst = $user_info['UID_CurrentInstitutionOrCompany'];
        //basci info
        ?>
        <div class="panel panel-default panel_profile">
            <!-- Default panel contents -->
            <div class="panel-heading"><center><h3 class='panel-title'>Profile Info</h3></center></div>
            <div class="panel-body">
                <div class="row"><!--
                <div class="col-md-3" >
               <img class="media-object img-circle wow fadeIn" data-wow-delay="0.1s" src="app/includes/res_include.php" style="width:130px;border-bottom:solid 3px #37a8df;margin:10px auto;" alt="user">
                </div>-->
                    <div class="col-md-12" >
                        <div class='panel' style='margin-bottom:30px;'>
                            <table style='font-size:12px;margin-top:35px;"' class='table table-hover'>
                                <?php
                                echo "<tr><th>Name</th><td>$name<td><th>Gender</th><td>$gender<td></tr>";
                                echo "<tr><th>Current city</th><td>$currCity<td><th>Home Town</th><td>$hometown<td></tr>";
                                //echo "<tr><th>Date of Birth</th><td>$dob<td><th>Email</th><td>$email<td></tr>";
                                //ends
                                ?>
                            </table></div>
                    </div></div>
                <!--  basic info ends  -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="background:#fff;padding: 0px;">
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="headingOne" style="border:1px solid #53cadf;">
                            <h4 class="panel-title" >
                                <a role="button"  data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
                                    <center id='show_change'>Show More</center></a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body" style="background: #eeeeee;">
                                <?php
                                $other_info_q = mysql_query("select * from " . OTHER_INFO . " where UID='$fid'") or error_log(mysql_error());
                                $other_info = mysql_fetch_assoc($other_info_q);
                                $prof_info = array();
                                $academics = array();
                                $prof_info = json_decode($other_info['prof_arr'], true);
                                $academics = json_decode($other_info['academics'], true);
                                foreach ($prof_info as $k => $v) {
                                    $sy[$k] = $v['start_year'];
                                    $sm[$k] = $v['start_month'];
                                }
                                array_multisort($sy, SORT_DESC, $sm, SORT_DESC, $prof_info);
                                error_log(print_r($prof_info, true));
                                //print_r($prof_info);
                                //echo count($prof_info);
                                //echo "<h3>Professional Info</h3>";
                                ?>
                                <div class="row">
                                    <?php
                                    for ($p = 0; $p < count($prof_info); $p++) {
                                        ?>
                                        <div class='col-sm-5 company_box wow fadeInRight'>
                                            <div class='label c_badge'><?php echo $prof_info[$p][start_year]; ?></div>
                                            <div class="row">
                                                <!--style="background: #595959"-->
                                                <div class="col-sm-12 profile_info" style="background: #8dccfa;" >
                                                    <img class="media-object img-circle img-responsive   col-md-5 wow fadeIn" data-wow-delay="0.1s"
                                                         src="assets/images/comp.png" style="border-bottom:solid 3px #37a8df;margin-left:26%;" alt="user">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class='table' style='font-size:13px;'>
                                                        <?php
                                                        //data print
                                                        echo "<tr><td>Company:</td><td>" . $prof_info[$p]['cname'] . "</td></tr>";
                                                        echo "<tr><td>Location:</td><td>" . $prof_info[$p]['cloc'] . " </td></tr>";
                                                        echo "<tr><td>Designation:</td><td>" . $prof_info[$p]['desig'] . "</td></tr> ";
                                                        echo "<tr><td>Functional area:</td><td>" . $prof_info[$p]['func_area'] . " </td></tr>";
                                                        // echo $prof_info[$p]['to_year'];
                                                        // echo $prof_info[$p]['to_month'];
                                                        ?>
                                                    </table>
                                                </div></div>
                                        </div>
                                        <?php
                                    }
                                    echo "</div>";
                                    //echo "<h3>Academics</h3>";
                                    //academics
                                    if (empty($academics[0]) && $academics[0] == "" && empty($academics[1]) && $academics[1] == "" && empty($academics[2]) && $academics[2] == "" && empty($academics[3]) && $academics[3] == "" && empty($academics[4]) && $academics[4] == "") {
                                        
                                    } else {
                                        echo "<div class='col-sm-11 company_box wow fadeInRight'>";
                                        echo "<div class='label c_badge'>$academics[3]</div>";
                                        echo "<div class='row'><div class='col-sm-12 profile_info' style='background: #8dccfa;'>
        <img class='media-object img-circle img-responsive col-md-3 wow fadeIn' data-wow-delay='0.1s'
          src='assets/images/degree.png' style='border-bottom:solid 3px #37a8df;margin-left:36%;'></div></div> <div class='row'><div class='col-sm-12'> ";
                                        echo "<h4>Degree</h4>";
                                        echo "<table style='font-size:12px;' class='table '>";
                                        ?>
                                        <tr>
                                            <?php
                                            if (!empty($academics[0])) {
                                                echo"<th>College</th><td>$academics[0]</td>";
                                            }
                                            if (!empty($academics[1])) {
                                                echo"<th>Location</th><td>$academics[1]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <?php
                                            if (!empty($academics[2])) {
                                                echo"<th>Branch</th><td>$academics[2]</td>";
                                            }
                                            if (!empty($academics[4])) {
                                                echo"<th>Grade/Percentage</th><td>$academics[4]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        echo "</table>";
                                        echo "</div></div></div>";
                                    }
                                    //diploma
                                    if (empty($academics[5]) && $academics[5] == "" && empty($academics[6]) && $academics[6] == "" && empty($academics[7]) && $academics[7] == "" && empty($academics[8]) && $academics[8] == "" && empty($academics[9]) && $academics[9] == "") {
                                        
                                    } else {
                                        echo "<div class='col-sm-11 company_box wow fadeInRight'>";
                                        echo "<div class='label c_badge  wow fadeIn'>$academics[8]</div>";
                                        echo "<div class='row'>
        <div class='col-sm-12 profile_info' style='background: #8dccfa;'>
          <img class='media-object img-circle img-responsive col-md-3 wow fadeIn' data-wow-delay='0.1s'
          src='assets/images/degree.png'
          style='border-bottom:solid 3px #37a8df;margin-left:36%;'>
        </div>
        </div>
           <div class='row'>
        <div class='col-sm-12'> ";
                                        echo "<p><b>Diploma</b></p>";
                                        echo "<table style='font-size:12px;' class='table'>";
                                        ?>
                                        <tr>
                                            <?php
                                            if (!empty($academics[5])) {
                                                echo"<th>College</th><td>$academics[5]</td>";
                                            }
                                            if (!empty($academics[6])) {
                                                echo"<th>Location</th><td>$academics[6]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <?php
                                            if (!empty($academics[7])) {
                                                echo"<th>Branch</th><td>$academics[7]</td>";
                                            }
                                            if (!empty($academics[9])) {
                                                echo"<th>Grade/Percentage</th><td>$academics[9]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        echo "</table>";
                                        echo "</div></div></div>";
                                    }
                                    //XIIth
                                    if (empty($academics[10]) && $academics[10] == "" && empty($academics[11]) && $academics[11] == "" && empty($academics[12]) && $academics[12] == "" && empty($academics[13]) && $academics[13] == "" && empty($academics[14]) && $academics[14] == "") {
                                        
                                    } else {
                                        echo "<div class='col-sm-11 company_box wow fadeInRight'>";
                                        echo "<div class='label c_badge wow fadeIn'>$academics[13]</div>";
                                        echo "<div class='row'>
        <div class='col-sm-12 profile_info' style='background: #8dccfa;'>
          <img class='media-object img-circle img-responsive col-md-3 wow fadeIn' data-wow-delay='0.1s'
          src='assets/images/school.png'
          style='border-bottom:solid 3px #37a8df;margin-left:36%;'>
        </div>
        </div>
           <div class='row'>
        <div class='col-sm-12'> ";
                                        echo "<p><b>XIIth</b></p>";
                                        echo "<table style='font-size:12px;' class='table'>";
                                        ?> 
                                        <tr>
                                            <?php
                                            if (!empty($academics[10])) {
                                                echo"<th>College</th><td>$academics[10]</td>";
                                            }
                                            if (!empty($academics[11])) {
                                                echo"<th>Location</th><td>$academics[11]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <?php
                                            if (!empty($academics[12])) {
                                                echo"<th>Board</th><td>$academics[12]</td>";
                                            }
                                            if (!empty($academics[14])) {
                                                echo"<th>Grade/Percentage</th><td>$academics[14]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        echo "</table>";
                                        echo "</div></div></div>";
                                    }
                                    //Xth
                                    if (empty($academics[15]) && $academics[15] == "" && empty($academics[16]) && $academics[16] == "" && empty($academics[17]) && $academics[17] == "" && empty($academics[18]) && $academics[18] == "" && empty($academics[19]) && $academics[19] == "") {
                                        
                                    } else {
                                        echo "<div class='col-sm-11 company_box wow fadeInRight'>";
                                        echo "<div class='label c_badge  wow fadeIn'>$academics[18]</div>";
                                        echo "<div class='row'>
        <div class='col-sm-12 profile_info' style='background: #8dccfa;'>
          <img class='media-object img-circle img-responsive col-md-3 wow fadeIn' data-wow-delay='0.1s'
          src='assets/images/school.png'
          style='border-bottom:solid 3px #37a8df;margin-left:36%;'>
        </div>
        </div>
           <div class='row'>
        <div class='col-sm-12'> ";
                                        echo "<p><b>Xth</b></p>";
                                        echo "<table style='font-size:12px;' class='table'>";
                                        ?>
                                        <tr>
                                            <?php
                                            if (!empty($academics[15])) {
                                                echo"<th>School</th><td>$academics[15]</td>";
                                            }
                                            if (!empty($academics[16])) {
                                                echo "<th>Location</th><td>$academics[16]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <?php
                                            if (!empty($academics[17])) {
                                                echo "<th>Board</th><td>$academics[17]</td>";
                                            }
                                            if (!empty($academics[19])) {
                                                echo"<th>Grade/Percentage</th><td>$academics[19]<td>";
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        echo "</table>";
                                        echo "</div></div></div>";
                                    }
                                    echo "</br></br></br></br>";
                                } else if ($func == "ACADEMIC") {
                                    $page = $complete_arr['page'];
                                    $uid = getUID();
                                    if ($request_arr == "EditAca") {
                                        $other_info_q = mysql_query("select academics from " . OTHER_INFO . " where UID='$uid'") or die(mysql_error());
                                        $other_info_data = mysql_fetch_assoc($other_info_q);
                                        $aca_arr = json_decode($other_info_data['academics']);
                                        if (is_array($aca_arr)) {
                                            //college
                                            $coll_name = $aca_arr[0];
                                            $coll_loc = $aca_arr[1];
                                            $coll_branch = $aca_arr[2];
                                            $coll_year = $aca_arr[3];
                                            $coll_grade = $aca_arr[4];
                                            //diploma
                                            $dip_name = $aca_arr[5];
                                            $dip_loc = $aca_arr[6];
                                            $dip_branch = $aca_arr[7];
                                            $dip_year = $aca_arr[8];
                                            $dip_grade = $aca_arr[9];
                                            //12th
                                            $xii_c_name = $aca_arr[10];
                                            $xii_c_loc = $aca_arr[11];
                                            $xii_c_branch = $aca_arr[12];
                                            $xii_c_year = $aca_arr[13];
                                            $xii_c_grade = $aca_arr[14];
                                            //10th
                                            $x_c_name = $aca_arr[15];
                                            $x_c_loc = $aca_arr[16];
                                            $x_c_medium = $aca_arr[17];
                                            $x_c_year = $aca_arr[18];
                                            $x_c_grade = $aca_arr[19];
                                            $current_year_data = date("Y");
                                            $past_years_till = $current_year_data - 100;
                                            $a_college_year_dd = "<select id='coll_year'>";
                                            $a_d_college_year_dd = "<select id='dip_year'>";
                                            $a_xii_year_dd = "<select id='xii_c_year'>";
                                            $a_x_year_dd = "<select id='x_c_year'>";
                                            for ($y = $current_year_data; $y >= $past_years_till; $y--) {
                                                if ($y == $coll_year) {
                                                    $a_college_year_dd.="<option value='$y' selected >$y</option>";
                                                } else {
                                                    $a_college_year_dd.="<option value='$y'>$y</option>";
                                                }
                                                if ($y == $dip_year) {
                                                    $a_d_college_year_dd.="<option value='$y' selected >$y</option>";
                                                } else {
                                                    $a_d_college_year_dd.="<option value='$y'>$y</option>";
                                                }
                                                if ($y == $xii_c_year) {
                                                    $a_xii_year_dd.="<option value='$y' selected >$y</option>";
                                                } else {
                                                    $a_xii_year_dd.="<option value='$y'>$y</option>";
                                                }
                                                if ($y == $x_c_year) {
                                                    $a_x_year_dd.="<option value='$y' selected >$y</option>";
                                                } else {
                                                    $a_x_year_dd.="<option value='$y'>$y</option>";
                                                }
                                            }
                                            $a_college_year_dd.="</select>";
                                            $a_d_college_year_dd.="</select>";
                                            $a_xii_year_dd.="</select>";
                                            $a_x_year_dd.="</select>";
                                            echo"<script>
            document.getElementById('a_college').innerHTML=\"<input type='text' class='form-control input-sm'  id='coll_name' value='$coll_name' >\";
            document.getElementById('a_college_location').innerHTML=\"<input type='text' id='coll_loc' class='form-control input-sm'  value='$coll_loc' >\";
            document.getElementById('a_branch').innerHTML=\"<input type='text' id='coll_branch' value='$coll_branch' class='form-control input-sm'  >\";
            document.getElementById('a_year').innerHTML=\"$a_college_year_dd\";
            document.getElementById('a_d_percentage').innerHTML=\"<input type='text' id='coll_grade' value='$coll_grade' class='form-control input-sm'  >\";
            document.getElementById('a_diploma_college').innerHTML=\"<input type='text' id='dip_name' value='$dip_name' class='form-control input-sm' >\";
            document.getElementById('a_diploma_location').innerHTML=\"<input type='text' id='dip_loc' value='$dip_loc' class='form-control input-sm' >\";
            document.getElementById('a_diploma_branch').innerHTML=\"<input type='text' id='dip_branch' value='$dip_branch' class='form-control input-sm' >\";
            document.getElementById('a_diploma_year').innerHTML=\"$a_d_college_year_dd\";
            document.getElementById('a_diploma_percentage').innerHTML=\"<input type='text' id='dip_grade' value='$dip_grade' class='form-control input-sm' >\";
            document.getElementById('a_xii_college').innerHTML=\"<input type='text' id='xii_c_name' value='$xii_c_name' class='form-control input-sm' >\";
            document.getElementById('a_xii_location').innerHTML=\"<input type='text' id='xii_c_loc' value='$xii_c_loc' class='form-control input-sm' >\";
            document.getElementById('a_xii_branch').innerHTML=\"<input type='text' id='xii_c_branch' value='$xii_c_branch' class='form-control input-sm' >\";
            document.getElementById('a_xii_year').innerHTML=\"$a_xii_year_dd\";
            document.getElementById('a_xii_percentage').innerHTML=\"<input type='text' id='xii_c_grade' value='$xii_c_grade' class='form-control input-sm' >\";
            document.getElementById('a_school').innerHTML=\"<input type='text' id='x_c_name' value='$x_c_name' class='form-control input-sm' >\";
            document.getElementById('a_school_location').innerHTML=\"<input type='text' id='x_c_loc' value='$x_c_loc' class='form-control input-sm' >\";
            document.getElementById('a_x_medium').innerHTML=\"<input type='text' id='x_c_medium' value='$x_c_medium' class='form-control input-sm' >\";
            document.getElementById('a_x_year').innerHTML=\"$a_x_year_dd\";
            document.getElementById('a_x_percentage').innerHTML=\"<input type='text' id='x_c_grade' value='$x_c_grade' class='form-control input-sm' >\";
            </script>";
                                        } else {
                                            echo"<script>
            document.getElementById('a_college').innerHTML=\"<input type='text' class='form-control input-sm'  id='coll_name' value='' >\";
            document.getElementById('a_college_location').innerHTML=\"<input type='text' id='coll_loc' class='form-control input-sm'  value='' >\";
            document.getElementById('a_branch').innerHTML=\"<input type='text' id='coll_branch' value='' class='form-control input-sm'  >\";
            document.getElementById('a_year').innerHTML=\"<input type='text' id='coll_year' value='' class='form-control input-sm'  >\";
            document.getElementById('a_d_percentage').innerHTML=\"<input type='text' id='coll_grade' value='' class='form-control input-sm'  >\";
            document.getElementById('a_diploma_college').innerHTML=\"<input type='text' id='dip_name' value='' class='form-control input-sm' >\";
            document.getElementById('a_diploma_location').innerHTML=\"<input type='text' id='dip_loc' value='' class='form-control input-sm' >\";
            document.getElementById('a_diploma_branch').innerHTML=\"<input type='text' id='dip_branch' value='' class='form-control input-sm' >\";
            document.getElementById('a_diploma_year').innerHTML=\"<input type='text' id='dip_year' value='' class='form-control input-sm' >\";
            document.getElementById('a_diploma_percentage').innerHTML=\"<input type='text' id='dip_grade' value='' class='form-control input-sm' >\";
            document.getElementById('a_xii_college').innerHTML=\"<input type='text' id='xii_c_name' value='' class='form-control input-sm' >\";
            document.getElementById('a_xii_location').innerHTML=\"<input type='text' id='xii_c_loc' value='' class='form-control input-sm' >\";
            document.getElementById('a_xii_branch').innerHTML=\"<input type='text' id='xii_c_branch' value='' class='form-control input-sm' >\";
            document.getElementById('a_xii_year').innerHTML=\"<input type='text' id='xii_c_year' value='' class='form-control input-sm' >\";
            document.getElementById('a_xii_percentage').innerHTML=\"<input type='text' id='xii_c_grade' value='' class='form-control input-sm' >\";
            document.getElementById('a_school').innerHTML=\"<input type='text' id='x_c_name' value='' class='form-control input-sm' >\";
            document.getElementById('a_school_location').innerHTML=\"<input type='text' id='x_c_loc' value='' class='form-control input-sm' >\";
            document.getElementById('a_x_medium').innerHTML=\"<input type='text' id='x_c_medium' value='' class='form-control input-sm' >\";
            document.getElementById('a_x_year').innerHTML=\"<input type='text' id='x_c_year' value='' class='form-control input-sm' >\";
            document.getElementById('a_x_percentage').innerHTML=\"<input type='text' id='x_c_grade' value='' class='form-control input-sm' >\";
            </script>";
                                        }
                                    } else {
                                        //update
                                        $up_data = array();
                                        foreach ($request_arr as $key => $value) {
                                            $up_data[$key] = htmlspecialchars($value, ENT_QUOTES);
                                        }
                                        $enc_arr = json_encode($up_data);
                                        mysql_query("Update " . OTHER_INFO . " SET academics='$enc_arr' where UID='$uid'") or die(mysql_error());
                                        echo"<script>
            document.getElementById('a_college').innerHTML=\"$up_data[0]\";
            document.getElementById('a_college_location').innerHTML=\"$up_data[1]\";
            document.getElementById('a_branch').innerHTML=\"$up_data[2]\";
            document.getElementById('a_year').innerHTML=\"$up_data[3]\";
            document.getElementById('a_d_percentage').innerHTML=\"$up_data[4]\";
            document.getElementById('a_diploma_college').innerHTML=\"$up_data[5]\";
            document.getElementById('a_diploma_location').innerHTML=\"$up_data[6]\";
            document.getElementById('a_diploma_branch').innerHTML=\"$up_data[7]\";
            document.getElementById('a_diploma_year').innerHTML=\"$up_data[8]\";
            document.getElementById('a_diploma_percentage').innerHTML=\"$up_data[9]\";
            document.getElementById('a_xii_college').innerHTML=\"$up_data[10]\";
            document.getElementById('a_xii_location').innerHTML=\"$up_data[11]\";
            document.getElementById('a_xii_branch').innerHTML=\"$up_data[12]\";
            document.getElementById('a_xii_year').innerHTML=\"$up_data[13]\";
            document.getElementById('a_xii_percentage').innerHTML=\"$up_data[14]\";
            document.getElementById('a_school').innerHTML=\"$up_data[15]\";
            document.getElementById('a_school_location').innerHTML=\"$up_data[16]\";
            document.getElementById('a_x_medium').innerHTML=\"$up_data[17]\";
            document.getElementById('a_x_year').innerHTML=\"$up_data[18]\";
            document.getElementById('a_x_percentage').innerHTML=\"$up_data[19]\";
            </script>";
                                    }
                                }
                            } else {
                                //empty request
                                //might be a threat
                            }
                            ?>
                        </div>
                        <!-- <div class="panel-heading" role="tab" id="headingOne" style="border:1px solid #53cadf;">
                           <h4 class="panel-title" >
                             <a role="button"  data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
                               <center id='show_change1'>Show Less</center></a>
                           </h4>
                         </div>-->
                    </div>
                </div>
            </div></div>
    </div>
    <!--  main panel end div -->    </div>
<script>
    $('#show_change').click(function () {
        if ($('#show_change').text() == 'Show Less') {
            $('#show_change').text('Show More');
        } else {
            $('#show_change').text('Show Less');
        }
    });
    $('#show_change1').click(function () {
        if ($('#show_change').text() == 'Show Less') {
            $('#show_change').text('Show More');
        } else {
            $('#show_change').text('Show More');
        }
    });
</script>