<style type="text/css">
    body{background: transparent;font-size: 12px;/*background: #25af60;*/}
    /*.active_home{background:#b9e1f4;border-top:2px solid #37a8df;}*/
    /*.active_profile{}*/
</style>
<div id=refresh ></div>
<?php
include "user_menu.php";
?>
<div class="shadow">
</div>
<ol id="tour" style="display:none;">
    <li data-target="img">Welcome to Roughsheet! Click to continue.</li>
    <li data-target=".sm_color5">Planner is the section, where you’ll plan out the way you want to study, while we’ll devise a revision 
        plan simultaneously for you</li>
    <li data-target=".sm_color1">Study Material is the section where all the study material, for the course that you’ve selected, is.</li>
    <li data-target=".sm_color2">Practice Problem Sheets is the section where you’ll get the Daily Practice Problem Sheets, on each of the 
        four subjects, on a daily basis, and you’ll also get a Topic Practice Problem Sheet, of a specific topic of a 
        specific subject, for regular revision.</li>
    <li data-target=".sm_color3">Focus Areas is the section where we indicate your weaknesses (if any), once we’ve collected substantial 
        data about your performance.</li>
    <li data-target=".sm_color4" data-angle="250">My Performance is the section where you’ll get detailed information about the way you have been 
        performing in the four subjects.</li>
    <!--<li data-target=".to_do_list" data-angle="310" data-options="distance:50">Todasy to do list</li>-->
</ol>
<div class="col-sm-12  col-md-12  main hidden-xs" style="background:#fff;border-bottom:2px solid #fc6f4b;" >
    <div class="col-md-12"  style="margin-top:40px;background:#f5f5f5;border-left:2px solid #37a8df;border-right:2px solid #37a8df;padding-bottom:10px;">
        <div class="row placeholders" style="margin-bottom:0px;">
            <div class="col-xs-6 col-sm-2 placeholder  wow fadeIn" data-wow-delay="0.2s">
                <a href="#" onclick="getPage('SCHEDULER')" class="list-group-item sm_color5" style="margin-top:20px;">
                    <img  class='glass icon_p'  src="assets/images/p2.png" >
                      <!--<img src="assets/images/p2.png" onmouseover="this.src='assets/images/p5.png';" onmouseout="this.src='assets/images/p2.png';"></img>-->
                    <h4>Planner</h4>
                               <!--<span
                               style="background: #37a8df;"
                               class="text-muted">Something else</span>-->
                </a>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder wow fadeIn" data-wow-delay="0.4s">
                <a  href="#" onclick="getPage('STUDY MATERIAL')" class="list-group-item sm_color1" style="margin-top:20px;">
                    <img  src="assets/images/s2.png"  class='glass'>
                    <h4>Study Material</h4>
                                <!--<span class="text-muted">Something else</span>-->
                </a>
                </a>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder wow fadeIn" data-wow-delay="0.6s">
                <a  href="#" onclick="getPage('DPP')" class="list-group-item sm_color2" style="margin-top:20px;">
                    <img  class='glass' src="assets/images/pp2.png">
                    <h4>Practice </br>Problem Sheets</h4>
                                <!--<span class="text-muted">Something else</span>-->
                </a>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder wow fadeIn" data-wow-delay="0.8s">
                <a href="#" onclick="getPage('FOCUS')" class="list-group-item sm_color3" style="margin-top:20px;">
                    <img  class='glass' src="assets/images/f2.png">
                    <h4>Focus Areas</h4>
                               <!--<span class="text-muted">Something else</span>-->
                </a>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder wow fadeIn" data-wow-delay="1s">
                <a href="#" onclick="getPage('PERFORMANCE')" class="list-group-item sm_color4" style="margin-top:20px;">
                    <img  class='glass' src="assets/images/m2.png">
                    <h4>My Performance</h4>
                             <!--<span class="text-muted">Something else</span>-->
                </a>
                <!--
                 <a href="#" onclick="getPage('GALACTICOS')" class="list-group-item" style="margin-top:20px;"><span class="badge">1</span>Galactitos</a>-->
            </div>
            <div class="col-xs-6 col-sm-2 placeholder wow fadeIn" data-wow-delay="1s">
                <a href="#" onclick="getPage('Galacticos')" class="list-group-item sm_color6 " style="margin-top:20px;">
                    <img  class='glass' src="assets/images/rank_4.png">
                    <h4>Ranking</h4>
                             <!--<span class="text-muted">Something else</span>-->
                </a>
                <!--
                 <a href="#" onclick="getPage('GALACTICOS')" class="list-group-item" style="margin-top:20px;"><span class="badge">1</span>Galactitos</a>-->
            </div>
        </div> 
    </div>
</div>
<div class="container-fluid main_feed" style="background:#ebebeb;bottom:0;">
    <div class="col-md-2" style="background:#fff;">
        <!--profile content-->
        <!--start-->
      <!--<img src="#" alt="Username" class="img-responsive img-circle" style="width:100%">-->
        <div id="hidden">
            <?php include"home_sidemenu.php"; ?>
        </div>
        <!--profile end-->
    </div>
    <div id="feeds_center" class="col-md-8" style="background:transparent;margin-top:10px;">
        <!--blog section-->
        <?php include"feeds.php"; ?>
    </div>
    <!--<div class="col-sm-2" style="margin:0px auto;padding:0px">-->
    <!--notification-->
    <!--<div class="panel panel-default glass wow flipInY" style="border-left: solid 2px #febf10;border-right:solid 2px #fc6f4b;border-bottom:solid 2px #febf10;border-radius: 0px;z-index:3;">-->
    <!--  <div class="panel-body">    -->
    <!--  <a href="#">Dapibus ac facilisis in</a>-->
    <!--  <a href="#">Morbi leo risus</a>-->
    <!--  <a href="#" class="list-group-item">Porta ac consectetur ac</a>-->
    <!--  <a href="#" class="list-group-item">Vestibulum at eros</a>-->
    <!--<a href="#" class="list-group-item">Dapibus ac facilisis in</a>-->
    <!--  <a href="#" class="list-group-item">Morbi leo risus</a>-->
    <!--  <a href="#" class="list-group-item">Porta ac consectetur ac</a>-->
    <!-- <a href="#" class="list-group-item">Vestibulum at eros</a>-->
    <!--
    <!--  </div>-->
    <!--<div class="list-group">-->
    <!--  <center style="border-radius:0px;">-->
    <!--  <a href="#" class="list-group-item disabled">-->
    <!--    ADDS-->
    <!--  </a></center>-->
    <!--  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>-->
    <!--  <a href="#" class="list-group-item">Morbi leo risus</a>-->
    <!--  <a href="#" class="list-group-item">Porta ac consectetur ac</a>-->
    <!--  <a href="#" class="list-group-item">Vestibulum at eros</a>-->
    <!--</div>  -->
    <!--notification end-->
    <!--add start-->
    <!--add end-->
    <!--</div>-->
    <!--</div>-->
    <!--quote-->
    <!-- quote ends-->
    <!--</div>-->
    <script type="text/javascript">
        $(function () {
            var zIndexNumber = 1000;
            // Put your target element(s) in the selector below!
            $("div").each(function () {
                $(this).css('zIndex', zIndexNumber);
                zIndexNumber -= 10;
            });
        });
    </script>
    <script src="assets/script.js"></script>