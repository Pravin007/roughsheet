<style type="text/css">
    .alert{
        margin-top:200px;
        margin:200px auto;
        width:600px;
    }
    .shadow{
        opacity: 0.9;
        background: #fff;
        min-height:100%;
        min-width:100%;
    }
</style>
<link href="assets/css/datepicker.css" rel="stylesheet">
<?php
//DIRECT PAGE ACCESS BLOCK
include"block_access.php";
//END
session_start();
//direct page access to be blocked here.
//END
$error_str="";
/** Validate captcha */
if(isset($_REQUEST["submit"])){
    //Captcha Check
    if (!empty($_REQUEST['captcha'])) {
        if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
            $error_str = "<li>Invalid captcha</li>";
            $request_captcha = htmlspecialchars($_REQUEST['captcha']);
            unset($_SESSION['captcha']);
        }
        else{
            $request_captcha = htmlspecialchars($_REQUEST['captcha']);
            unset($_SESSION['captcha']);
        }
    }
    else{
        $request_captcha = htmlspecialchars($_REQUEST['captcha']);
        unset($_SESSION['captcha']);
        $error_str="<li>Captcha is Mandatory</li>";
    }
    //this array must be modified respectively, if the registration form is modified.
    $value_pointer_desc=array(
                              "func_type" => "FUNCTION",
                              "f_name" => "First Name",
                              //"m_name" => "Middle Name",
                              "l_name" => "Last Name",
                              "gender" => "Gender",
                              "dob" => "Date Of Birth",
                              "email" => "Email",
                              //"phone" => "Phone",
                              //"acc_type" => "Account Type",
                              //"u_name" => "Username",
                              "u_pass" => "Password",
                              "u_pass_confirm" => "Confirm-Password",
                              "submit" => "Submit"
                              );
    //Empty Check
    foreach($value_pointer_desc as $key=>$value){
        if($key!="phone"){
            //as phone no is optional.
            //other optional values can be added in above if loop followed by OR value '||'.
            if(empty($_REQUEST[$key]) || $_REQUEST[$key]==null || !isset($_REQUEST[$key]))
            {
                $error_str=$error_str . "<li>$value is Mandatory</li>";
            }
        }
    }
    //password matching
    if($_REQUEST['u_pass']!=$_REQUEST['u_pass_confirm'])
    {
        $error_str=$error_str . "<li>Passwords do not match</li>";
    }
    if($error_str!=""){
        goto mandatory_error;
    }
    else{
        //insert into db.
        $conn_arr=getMySqlConnectionValues("user");
        $call_db="GRANT_ACCESS_TO_DB";
        $host=$conn_arr["HOST"];
        $user=$conn_arr["USER"];
        $pass=$conn_arr["PASS"];
        $db=$conn_arr["DB"];
        require_once ext_file_include('db_connectivity','config');
        mysql_query("SET AUTOCOMMIT=0");
        mysql_query("START TRANSACTION");
        $lock_read=mysql_query("LOCK TABLES " . USER_INFO . " WRITE," . LOGIN_TABLE . " WRITE," . FRIENDS_TABLE . "WRITE") or die(mysql_error());
        //acc type=1 : user
        $login_insert=mysql_query("INSERT INTO " . LOGIN_TABLE . "(UID_Username,UID_Password,UID_AccountType,UID_PRIVILEGE_TYPE,UID_Verified) VALUES('$_REQUEST[email]', '$_REQUEST[u_pass]', '1', '1', '0')")  or die(mysql_error());
        $user_info_insert=mysql_query("INSERT INTO " . USER_INFO . "(`UID_FirstName`, `UID_MiddleName`, `UID_LastName`, `UID_Gender`, `UID_DOB`, `UID_Contact`, `UID_Email`) VALUES ('$_REQUEST[f_name]','$_REQUEST[m_name]','$_REQUEST[l_name]','$_REQUEST[gender]','$_REQUEST[dob]','$_REQUEST[phone]','$_REQUEST[email]')") or die("USERNAME & PASSWORD ARE OK. CHECK YOUR REST OF THE INFORMATION.");
        $q=mysql_query("select UID from " . USER_INFO . " ORDER BY UID DESC LIMIT 1") or die(mysql_error());
        $qd=mysql_fetch_assoc($q);
        $friends_table_insert=mysql_query("INSERT INTO " . FRIENDS_TABLE . "(UID,F_UID,FR_UID,SFR_UID) VALUES('$qd[UID]','','','')") or die(mysql_error());
        $unlock_read=mysql_query("UNLOCK TABLES");
        if ($lock_read && $user_info_insert) {
            mysql_query("COMMIT");
            echo"REGISTRATION SUCCESSFUL<br>To Login<a href='" . START_PATH . "' >Click Here</a><br>";
        } else {        
            mysql_query("ROLLBACK");
            $error_str="<li>REGISTRATION FAILED !</li>";
            goto mandatory_error;
        }
    }
}
else{
    mandatory_error:
    if($error_str!="")
    {
        echo"<div class='alert alert-danger navbar-fixed-top'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>Close &times;</span></button>
        <center><h1><u><b>Mandatory fileds</b></u></h1></center>
        <ol style='margin:3px auto;'>$error_str</ol>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>Close &times;</span></button></div>";
    }
?>
<style type="text/css">
#result { border: 1px solid green; width: 300px; margin: 0 0 35px 0; padding: 10px 20px; font-weight: bold; }
#change-image { font-size: 0.8em; }
</style>
<style type="text/css">
   .navbar .navbar-nav .dropdown >a:hover{
    background: #fff;
    color:#27b664;  
   }
  .dropdown-menu > a:hover{
    background: #27b664;
    color:#fff;  
  }
  .dropdown-menu > li > a:hover{
    background: #27b664;
    color:#fff;  
   }
  .navbar-default .navbar-nav > .active > a {
    background: #fff;
    color: #27b664;
  }
  .navbar-default .navbar-nav > .active > a:hover {
    background: #fff;
  }
.navbar-default .navbar-nav > li > a{
  color: #fff;
}
   .navbar-default .navbar-nav > li > a:hover{
    background:#fff;
    color:#27b664;
   }
</style>
<body data-spy="scroll" data-target=".navbar-default">
    <div class="container">
<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top" style="background: #27b664;color: #fff;">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="color: #fff;background:#fff">
            <img src="assets/images/180_30_logo_size.png" style="width:170px; height:30px;  margin-top:-5px;background: transparent" alt="roughsheet" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#" style="background:#eee;">...</a></li>
            <li class="active"><a href="index.php">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>  
    </nav>
    <div class="row">
  <div class="col-md-6">
    <h2>Register User page</h2>
    <hr>
<form method="post" action="index.php" class="form-horizontal" style="background: #fff; padding: 10px; border-radius:13px; box-shadow: #ebebeb 0 0 20px;"> 
    <input type=hidden name=func_type value=register />
 <!----start---->
  <div class="form-group">
    <label for="first name" class="col-sm-4 control-label">First Name</label> 
    <div class="col-sm-6">
     <input type="text" name="f_name" class="form-control" placeholder="First Name" />
    </div>
   </div>
      <!--<div class="form-group">
    <label for="middle name" class="col-sm-4 control-label">Middle Name
    </label> 
    <div class="col-sm-6">
     <input type="text" name="m_name" class="form-control" placeholder="Middle Name"/>
    </div>
   </div>-->
      <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">
      Last Name
      </label> 
    <div class="col-sm-6">
      <input type=text name="l_name" class="form-control" placeholder="last Name"/>
    </div></div>
      <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">
      Gender</label>
    <div class="col-sm-6">  
      <input type=radio name="gender" value="Male" />
    Male
    <input type=radio name="gender" value="Female" /> Female
    </div></div>
      <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">
      DOB </label>
    <div class="col-sm-6">  
    <div id='dp1' data-date='01-01-2015' data-date-format='dd-mm-yyyy' class='date'>
        <div class="input-group">
        <input type="text" name="dob"  class='form-control' placeholder="Date of birth">
    <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-calendar" id="basic-addon2"></span></span>
    </div>
    </div></div>
      </div>
      <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">
      Email </label>
    <div class="col-sm-6">  
         <input type=email name="email" class="form-control" placeholder="Email" />
    </div></div>
    <!--  <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">  
      Phone </label>
<div class="col-sm-6">  
         <input type="text" name="phone" class="form-control" placeholder="Phone OR Mobile" />
</div></div>-->
      <!--<div class="form-group">
    <label for="gender" class="col-sm-4 control-label">  
    Account Type:</label>
<div class="col-sm-6">      
    <select name=acc_type  class="form-control">
        <option value=1 >School</option>
        <option value=2 >College</option>
        <option value=3 >Experienced</option>
        <option value=4 >Corporate</option>        
    </select>
</div></div>--><!--
    <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">  
      Username</label>
     <div class="col-sm-6">      
      <input type=text name="u_name" class="form-control" placeholder="Username"/>
     </div></div>     -->
    <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">  
      Password
      </label>
     <div class="col-sm-6">
        <input type=password name="u_pass" class="form-control" placeholder="Password" />
     </div></div>
    <div class="form-group">
    <label for="gender" class="col-sm-4 control-label">  
      Confirm-Password
      </label>
     <div class="col-sm-6"><input type=password name="u_pass_confirm"  class="form-control" placeholder="Confirm password"/>
     </div></div>
    <div class="form-group">
    <label for="gender" class="col-sm-4 control-label"></label>
     <div class="col-sm-8">
   <img src="captcha.php" id="captcha"/>
<!-- CHANGE TEXT LINK -->
<a href="#" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image">Not readable? Change text.</a><br/>
<input type="text" name="captcha" id="captcha-form" autocomplete="off" class="form-control" placeholder="Captcha"/>    
     </div></div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
<center><input type=submit name=submit value=REGISTER class="btn btn-warning" /></center>
     </div></div>
</form>
<hr></div>
    </div>
    <script src="assets/js/bootstrap-datepicker.js"></script>
    <script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'yyyy/mm/dd'
			});
			$('#dp2').datepicker({
				format: 'yyyy/mm/dd'
			});
			$('#dp3').datepicker({
				format: 'yyyy/mm/dd'
			});
			$('#dp6').datepicker({
				format: 'yyyy/mm/dd'
			});
                        $('#dp4').datepicker({
				format: 'yyyy/mm/dd'
			});
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>
<?php
}//else closed
?>