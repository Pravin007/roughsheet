<style>
    .col{color:#333;-webkit-transition:0.5s; /* Safari */transition:0.5s;transition-timing-function:ease-in-out;padding:0px; cursor: pointer;}
    .col:hover{-webkit-transition:0.5s; /* Safari */transition:0.5s;transition-timing-function:ease-in-out;box-shadow:3px 8px 10px #999;border-radius:3px;opacity:0.8;}
    table{box-shadow:0 0 10px #ddd;background: #fff;}
    tr{-webkit-transition:1s; /* Safari */transition:1s;}
    .d{text-align: center;text-align: center;background:#fc6f4b;color:#fff;}
    .s_ex{font-size:12px;border-radius:0px 0px 3px 3px;border-color:#37a8df;padding:2px;}
    .s_remove{float:right;margin-top:-17px;color:#eee;padding:3px; z-index:1030;background:#37a8df;border-radius:10px 10px 0px 0px; }
    .s_remove:hover{color:#EEE;}
    .active-home{background:#fff;border:none;}
    .active-friends{background:#fff;border:none;}
    .active-profile{background:#fff;border:none;}
</style>
<style type="text/css">.sm_color5{background-color:#fff !important;color:#37a8df!important;outline: dashed 2px #d4d4d4 !important;}</style>
<script>
    function changeCalTo(type) {
        $.ajax({
            type: "POST",
            url: "app/includes/set.php?changeCalTo",
            data: "",
            success: function (result) {
                getPage('SCHEDULER');
            }
        });
    }
    function changeMonth(type) {
        $.ajax({
            type: "POST",
            url: "app/includes/set.php?changeMonth",
            data: "type=" + type,
            success: function (result) {
                getPage('SCHEDULER');
            }
        });
    }
    function refreshTopics(sub_val) {
        $.ajax({
            type: "POST",
            url: "app/includes/set.php?getTopicsForSub",
            data: "subid=" + sub_val,
            success: function (result) {
                document.getElementById("tid").innerHTML = result;
            }
        });
    }
</script>
<?php
session_start();
require "header.php";
$call_db = "GRANT_ACCESS_TO_DB";
$ConnArray = getMySqlConnectionValues("user");
$host = $ConnArray["HOST"];
$user = $ConnArray["USER"];
$pass = $ConnArray["PASS"];
$db = $ConnArray["DB"];
require ext_file_include('db_connectivity', 'res_2_config');
$uid = getUID();
if (!isset($_SESSION['SCHEDULE_FOR_MONTH'])) {
    $month_numeric = date("m"); //numeric
    $month = date("F"); //alpha
    $year = date("Y");
} else {
    $sc_4_date = explode("-", $_SESSION['SCHEDULE_FOR_MONTH']);
    $month_numeric = $sc_4_date[1];
    $month = date("F", mktime(0, 0, 0, $sc_4_date[1], 1, $sc_4_date[0]));
    $year = $sc_4_date[0];
}
$month_info = $month . ", " . $year; // print
$number_of_days = cal_days_in_month(CAL_GREGORIAN, $month_numeric, $year);
$d = mktime(0, 0, 0, $month_numeric, 1, $year); //month,date,year
$w = date("w", $d); //week day
?>
<!--
<div class="panel panel-default  wow fadeIn" style='border-color:#37a8df;'>
<div class="panel-heading glass" style='background: #37a8df;'><center><h3 class='panel-title'>Planner</h3></center></div>
 <div class="panel-body">
    <div class="from">
<div class='form-group from-inline'>
  <div class="input-group  col-md-6">
  <div class="input-group-addon">Select Month:</div>
<input type='text' class=' form-control' id='datepickerP' placeholder='Select Month' >
</div>
</div>
</div>
 </div>
</div>
-->
<div class="panel panel-default glass" style='border-color:#37a8df;'>
    <div class="panel-heading glass" style='background: #37a8df;color:#fff;'>
        <a href="javascript: changeMonth('PREVIOUS')" class='btn btn-defalut'><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
        <a href="javascript: changeMonth('NEXT')" class='btn btn-defalut' style='float: right;'><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
        <center><h3 class='panel-title' style="margin-top:-20px;"><?php echo "$month_info"; ?></h3></center>
    </div>
    <div class="panel-body">
<!--	<center><div class="from">-->
        <!--    <div class='form-group from-inline'>-->
        <!--      <div class="input-group  col-md-6">-->
        <!--      <div class="input-group-addon">Select Month:</div>-->
        <!--    <input type='text' class=' form-control' id='datepickerP' placeholder='Select Month' >-->
        <!--    </div>-->
        <!--    </div>-->
        <!--</div></center>-->
        <a data-toggle="modal" data-target=".scheduler_model" class="btn default btn-block glass" data-toggle="tooltip" data-placement="top" title="Add to Planner" >
            Set schedule <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    </div>
    <span id='plannerArea' ></span>
    <script>
        $('#datepickerP').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });
        $('#datepickerP').on('changeDate', function (ev) {
            $(this).datepicker('hide');
            month_str = document.getElementById('datepickerP').value;
            $.ajax
                    ({
                        type: "POST",
                        url: "app/includes/set.php?setPlanner",
                        data: {
                            "month_info": month_str
                        },
                        success: function (result)
                        {
                            $("#plannerArea").html(result);
                        }
                    });
        });
        function removeSC(tid, date) {
            $.ajax
                    ({
                        type: "POST",
                        url: "app/includes/set.php?removeSC",
                        data: {
                            "date": date,
                            "tid": tid
                        },
                        success: function (result)
                        {
                            getPage('SCHEDULER');
                        }
                    });
        }
    </script>
    <?php
    echo"<table class='table table-bordered table-responsive table-hover'><tr><th class='d' style=''>Monday</th><th class='d'>Tuesday</th><th class='d'>Wednesday</th><th class='d'>Thursday</th><th class='d'>Friday</th><th class='d'>Saturday</th><th class='d'>Sunday</th></tr><tr>";
    for ($i = 1; $i <= 42; $i++) {//42 => 7 rows of 7 days
        //echo"<tr><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td><td><div id='pos$j'></div></td></tr>";
        if ($i % 7 == 0) {
            if ($i == 42) {
                echo"<td class=''><span id='date_$i' class='badge' style='width:30px;background:#37a8df;padding:5px;border-radius:1px;'></span><div id='pos$i' style='font-size:12px;'></div><div id='ex_data_$i'></div><div class='s_topic' id='dt_data_$i'></div></td>";
            } else {
                echo"<td class=''><span id='date_$i' class='badge' style='width:30px;background:#37a8df;padding:5px;border-radius:1px;'></span><div id='pos$i' style='font-size:12px;'></div><div  id='ex_data_$i'></div><div class='s_topic' id='dt_data_$i'></div></td></tr><tr>";
            }
        } else {
            echo"<td class=''><span id='date_$i' class='badge' style='width:30px;background:#37a8df;padding:5px;border-radius:1px;'></span><div id='pos$i' class='glass' style='font-size:12px;'></div><div  id='ex_data_$i'></div><div class='s_topic' id='dt_data_$i' ></div></td>";
        }
    }
    echo"</tr></table>";
    $p = $w; //place
    //mysql_query("SELECT *") or error_log(mysql_error());
    $q = mysql_query("select data from " . SCHEDULE_DATA . " WHERE UID='$uid'") or error_log(mysql_error());
    $d = mysql_fetch_assoc($q);
    $dt_flag = 0; //dpp-tpp flag
    //print_r($d['data']);
    for ($j = 1; $j <= $number_of_days; $j++) {
        $ex2_count = $p + 1;
        $ex3_count = $p + 2;
        $tpp_count = $p + 7;
        if (strlen($j) == 1) {
            $j = "0$j";
        }
        $ex2_str = "";
        $ex3_str = "";
        $tpp_str = "";
        $cur_date1 = $year . "-" . $month_numeric . "-" . $j;
        $cur_date = $j . "/" . $month_numeric . "/" . $year;
        $enc_cur_date = strtotime($cur_date);
        $sub_str = "";
        $sc_data = json_decode($d['data']);
        foreach ($sc_data as $k => $v) {
            $t_d = date("d/m/Y", $v[1]);
            $sub_id = getSubId($v[0]);
            if ($t_d == $cur_date) {
                $t_name = getTopicName($v[0]);
                $ex2_str.="<div class=\"alert alert-primary s_ex col glass\" style=\"color:#35a8e0;\" onclick=\"setSub($sub_id,$v[0],2)\" >" . getTopicName($v[0]) . "<ul style=\"font-size:10px;margin-left:-20px;\"><li>Ex2  </li></ul></div>";
                $ex3_str.="<div class=\"alert alert-primary s_ex col glass\" style=\"color:#35a8e0;\" onclick=\"setSub($sub_id,$v[0],3)\" >" . getTopicName($v[0]) . "<ul style=\"font-size:10px;margin-left:-20px;\"><li>Ex3  </li></ul></div>";
                // $tpp_str.="TPP : " . getTopicName($v[0]) . "<br>";
                $sub_str.="<a href=\'javascript: void(0)\'  class=\'s_remove\' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Remove\" onclick=\'removeSC($v[0]," . strtotime($cur_date1) . ")\'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></a><div class=\"alert alert-primary s_ex col\" style=\"color:#35a8e0;\" onclick=\"setKc($sub_id,$v[0])\">" . getTopicName($v[0]) . "<ul style=\"font-size:10px;margin-left:-20px;\"><li>Key-Concepts</li><li>Ex 1</li></ul></div>";
                if ($tpp_count <= $number_of_days) {
                    echo"<script>$('#ex_data_$tpp_count').append('<div class=\"alert alert-primary s_ex col glass\" style=\"color:#35a8e0;cursor: default;\" > <span style=\'color:333;\'> TPP </span></br><ul style=\"font-size:12px;margin-left:-20px;\"><li> " . $t_name . " </li></div>' )</script>";
                }
                $dt_flag = 1;
            }
        }
        //echo $sub_str;
        if ($ex2_count <= $number_of_days) {
            echo"<script>$('#ex_data_$ex2_count').append('$ex2_str');
       </script>";
        }
        if ($ex3_count <= $number_of_days) {
            echo"<script>$('#ex_data_$ex3_count').append('$ex3_str');
       </script>";
        }
        //    if($tpp_count<=$number_of_days){
        // echo"<script>$('#dt_data_$tpp_count').append('$tpp_str');
        //   </script>";
        //    }
        if ($dt_flag == 1 && $j != $number_of_days) {
            $dt_val = $p + 1;
            echo"<script>$('#dt_data_$dt_val').html('<div class=\"alert alert-primary s_ex col glass\" onclick=\"getPage(\'DPP\')\" style=\"background:#fff;\"><ul style=\"font-size:10px;margin-left:-40px;text-align:center;\">DPPS</div>');</script>";
        }
        echo"<script>$('#date_$p').html('$j');
        $('#pos$p').html('$sub_str');
        </script>";
        $p++;
    }
    ?> 
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker1').datepicker();
        $('.datepicker1').on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    })
</script>
<div id="sch_modal" class="modal fade scheduler_model modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Set Schedule</h4>
            </div>
            <div class="modal-body">
                <center><div class='row'>
                        <div class='col-md-12 col-sm-9 col-xs-8'> 
                            <!--scheduler form-->
                            <div class="from" style="align:center;">
                                <div class="form-group from-inline">
                                    <input type="text" id="sc_date" class="form-control col-md-3 col-sm-9 col-xs-8 datepicker1"  data-date-format="yyyy-mm-dd" placeholder="Select Date" >
                                    </br></br></br>
                                    <select id="sub_id" class="form-control" onchange="refreshTopics(this.value)">
                                        <?php
                                        $q = mysql_query("select * from " . SUBJECTS . " ") or die(mysql_error());
                                        while ($d = mysql_fetch_assoc($q)) {
                                            echo"<option value='$d[sub_id]'>$d[sub_name]</option>";
                                        }
                                        ?>
                                    </select>
                                    </br>
                                    <select id="tid" class="form-control">
                                        <script>
                                            refreshTopics(1);
                                        </script>
                                    </select>
                                    </br>
                                    <button class='btn btn-primary btn-block glass' onclick="set_schedule();">Set Schedule</button>
                                    <div id='result'></div>
                                </div>
                            </div>
                            <!--ends-->
                        </div>    </div></center></div>
        </div>
    </div>
</div>
</div>
