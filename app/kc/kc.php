<html>
    <head>
        <title>Add New Que</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="css/new_css.css" type="text/css" media="screen">
<link href="css/btncss.css" type="text/css" rel="stylesheet"/>
<link href="css/table_css.css" type="text/css" rel="stylesheet"/>
<!-- OF COURSE YOU NEED TO ADAPT NEXT LINE TO YOUR tiny_mce.js PATH -->
<script type="text/javascript" src="add/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript" src="add/tinymce/jscripts/general.js"></script>
<script type="text/javascript" src="MathJax.js"></script>
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
	elements:"elm1",
	theme : "advanced",
	plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,fmath_formula,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,fmath_formula,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
			file_browser_callback : "ajaxfilemanager",
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : true,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,	
			relative_urls : true
});
function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "add/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "add/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
}
</script>
<script language="JavaScript" src="gen_validatorv4.js"
    type="text/javascript" xml:space="preserve"></script>
    </head>
    <body onUnload="exitPopUp()">
	<div id=wrap>
       <div id="container">
	   <?php //include("header/head.php"); ?>
        <div id="content-container1">
	 <span style="margin-left: 40px;" ><?php echo"TABLE:  " . strtoupper($_SESSION['table']) . " "; ?><a href="upload.php" style="margin-left: 50px;">UPLOAD EXCEL</a><a href="add.php?logout=yes" style="margin-left: 50px;">LOGOUT</a></span>
		<div id="content-container2">
                    <div id="content">
			</br></br></br><center>
<table class=table style="margin:0 0 0 40px;">
<form method="POST" action="insert.php" name="myform" id="myform">
	<tr><td>SECTION</td><td><input type="text" name="section" /></td></tr>
	<tr><td>INTRO SERIAL NO</td><td><input type="text" name="intro_s_no" /></td></tr>	
	<tr><td>INTRODUCTION</td><td><textarea name="intro" ></textarea></td></tr>	
	<tr><td>QUESTION</td><td><textarea name="que"></textarea></td></tr>
        <tr><td>OPTION 1 </td><td><textarea name="op1"></textarea></td></tr>        
        <tr><td>OPTION 2 </td><td><textarea name="op2"></textarea></td></tr>
        <tr><td>OPTION 3 </td><td><textarea name="op3"></textarea></td></tr>
        <tr><td>OPTION 4 </td><td><textarea name="op4"></textarea></td></tr>
        <tr><td>ANSWER(option&nbspnumber)</td><td>
	<select name="ans">
	    <option value=""></option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
	</select>
	</td></tr>
	<tr><td>SOLUTION </td><td><textarea name="solution"></textarea></td></tr>
        <tr><td colspan=2><center><input type=submit class=btn name=ADD value=Submit></center></td></tr>
</form>
</table>
</center>
</div>
		</div>
	</div>
<?php include('footer/footer.html');?>
     </div>
       <script language="JavaScript" type="text/javascript"
    xml:space="preserve">//<![CDATA[
//You should create the validator only after the definition of the HTML form
  var frmvalidator  = new Validator("myform");
  frmvalidator.addValidation("unit","req","PLEASE ENTER UNIT NUMBER (1 to 6) ONLY NUMERICs");
  frmvalidator.addValidation("unit","numeric","PLEASE ENTER UNIT NO. IN NUMERICS");
  frmvalidator.addValidation("ans","req","PLEASE ENTER ANSWER NO. IN NUMERIC");
  frmvalidator.addValidation("ans","numeric","PLEASE ENTER ANSWER NO. IN NUMERIC");
//]]></script>
       </div>
        <?php include("footer.html"); ?>
</body>
</html>
