<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
  session_start();
  require(dirname(dirname(dirname(__FILE__))) . "/app/includes/header.php");
 
	$targ_w = $targ_h = 150;
	$jpeg_quality = 90;

	$src = RES_2_IMG;
	$img_name=$_SESSION['img_to_crop'];
	//echo $servername . '/assets/images/' . $img_name ;
	
	$img_r = imagecreatefromjpeg($servername . '/assets/images/' . $img_name);
	$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

	imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
	$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	//header('Content-type: image/jpeg');
	imagejpeg($dst_r,$src . $img_name,$jpeg_quality);

        $path="";
        if($_SERVER['SERVER_NAME']=='localhost'){
            $path="roughsheet/";
	 //   require(dirname(dirname(dirname(__FILE__))) . "/app/includes/resize-class.php");
        }
	
       require(str_replace("\\", "/",dirname(dirname(dirname(__FILE__))) . "/app/includes/resize-class.php"));
        // *** 1) Initialise / load image
	//echo realpath($src . $img_name);
         $resizeObj = new resize(realpath($src . $img_name));
         $resizeObj_main = new resize(realpath($src . $img_name));
        // *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
       
         $resizeObj -> resizeImage(84, 84, 'auto');
         $resizeObj_main -> resizeImage(180, 180, 'auto');
        // *** 3) Save image

         $resizeObj -> saveImage(str_replace("\\", "/",realpath($src . "s" . $img_name)), 100);
         $resizeObj_main -> saveImage(str_replace("\\", "/",realpath($src . $img_name)), 100);
      
         $cookie=str_encrypt_n_decrypt($_SESSION['ROUGHSHEET_SCI'],'dec');
         $cookie_vars=explode('_',$cookie);
         $cookie_vars[6]=$img_name;
         $new_sess_str=implode("_",$cookie_vars);
         $_SESSION['ROUGHSHEET_SCI']=str_encrypt_n_decrypt($new_sess_str,'enc');
         unset($_SESSION['img_to_crop']);   

	 header("Location: http://$servername/index.php");
         

  }

// If not a POST request, display page below:

?><!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.Jcrop.js"></script>
  <!--<link rel="stylesheet" href="demo_files/main.css" type="text/css" />-->
  <!--<link rel="stylesheet" href="demo_files/demos.css" type="text/css" />-->
  <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
      <link href="http://<?php echo $servername;?>/assets/css/bootstrap.css" rel="stylesheet">
	  <link href="http://<?php echo $servername;?>/assets/css/sticky-footer-navbar.css" rel="stylesheet">

<script type="text/javascript">

  $(function(){

    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };

  function checkCoords()
  {
    if (parseInt($('#w').val())) return true;
    alert('Please select a crop region then press submit.');
    return false;
  };

</script>
<style type="text/css">
  #target {
    background-color: #ccc;
    width: 500px;
    height: 330px;
    font-size: 24px;
    display: block;
  }
</style>

</head>
<body  style='background: #f8f8f8;'>
<div class="container">
		<center>
		  
		    <div class="modal-content">
      <div class="modal-header  glass">
        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
        <h4 class="modal-title" id="myModalLabel">Update Profile picture</h4>
      </div>
		<div class="modal-body" style="background: #eee;">  

		<form action="crop.php" method="post" onsubmit="return checkCoords();">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />

			<input type="submit" value="Crop & Save" class="btn btn-lg btn-success btn-block" />
		</form>
		</br>
    <img src="crop_helper.php" id="cropbox" class="img-responsive img-thumbnail"/>

		</div>
	</div>
</div>

		</center>
	</body>

</html>
